//
//  ConnexionViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 04/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>
#import "BSKeyboardControls.h"
#import "CustomAlert.h"
#import "AppDelegate.h"
#import "resultat.h"
#import "XMLStringParser.h"
#import "XMLToObjectParser.h"
#import "utilisateur.h"
#import "ReglagesView.h"
#import "ApiUtils.h"
#import "StatistiquesRequest.h"

@protocol ConnexionViewControllerDelegate;
@interface ConnexionViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate, BSKeyboardControlsDelegate, UIAlertViewDelegate, UIPopoverControllerDelegate, UIGestureRecognizerDelegate> {
    id<ConnexionViewControllerDelegate>__unsafe_unretained delegate;
    IBOutlet UILabel *pseudoLabel;
    IBOutlet UILabel *emailLabel;
    IBOutlet UILabel *emailDetailLabel;
    IBOutlet UILabel *motDePasseLabel;
    IBOutlet UIImageView *imageMotDePasse;
    IBOutlet UIImageView *imageProfile;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *fbButton;
    IBOutlet UIButton *mailButton;
    IBOutlet UIButton *validateButton;
    IBOutlet UITextField *pseudoField;
    IBOutlet UITextField *emailField;
    IBOutlet UITextField *motDePasseField;
    IBOutlet UIImageView *fbSeparateur;
    IBOutlet UIImageView *mailSeparateur;
    BOOL isFacebook;
    BOOL isMail;
    CustomAlert *alert;
    UIPopoverController *popoverController;
    IBOutlet UIView *frontView;
    IBOutlet UIView *backView;
    IBOutlet UILabel *littleFBLabel;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    ApiUtils *utils;
    IBOutlet UIActivityIndicatorView *chargementIndicator;
    IBOutlet UILabel *infoFBTextLabel;
    IBOutlet UILabel *deconnexionReglagesLabel;
    NSString *facebookName;
    
    
    BOOL facebookConnected;
    int secondes;
    int secondesLoad;
    int appels;
    NSTimer *timer;
    NSTimer *timerLoad;

}

// Clavier
@property (nonatomic, unsafe_unretained) id<ConnexionViewControllerDelegate>delegate;
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic, retain) NSString *urlImage;

- (IBAction)flipFbPressed:(id)sender;
- (IBAction)flipMailPressed:(id)sender;
- (IBAction)resetPassword:(id)sender;

- (IBAction)fbPressed:(id)sender;
- (IBAction)mailPressed:(id)sender;
- (IBAction)validatePressed:(id)sender;
- (IBAction)selectImage:(id)sender;
- (IBAction)retourPressed:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;

@end


@protocol ConnexionViewControllerDelegate
-(void)validatePressedDetail:(BOOL)nouveau;
-(void)disconnectConnexionReglagesPressed;
-(void)retourConnexionPressed;
@required
@end