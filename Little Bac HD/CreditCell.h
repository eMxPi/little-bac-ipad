//
//  CreditCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 02/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *titre;
@property (strong, nonatomic) IBOutlet UITextView *detail;
@property (strong, nonatomic) IBOutlet UIImageView *image;

@end
