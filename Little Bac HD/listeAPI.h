//
//  listeAPI.h
//  Captain Price
//
//  Created by Maxime Pontoire on 17/10/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#define apiHost "85.236.153.33"
#define apiUrl @"http://85.236.153.33:8080/api-little"
#define apiUtilisateurUrl @"http://85.236.153.33:8080/api-little/rest/utilisateurs"
#define apiAppUrl @"http://85.236.153.33:8080/api-little/rest/appList"
#define apiCreditUrl @"http://85.236.153.33:8080/api-little/rest/credits"
#define apiLittlesUrl @"http://85.236.153.33:8080/api-little/rest/littles"
#define apiValidationUrl @"http://85.236.153.33:8080/api-little/rest/validateMot"
#define apiCorrectionUrl @"http://85.236.153.33:8080/api-little/rest/correction"
#define apiTempsUrl @"http://85.236.153.33:8080/api-little/rest/temps"
#define apiAntiSecheUrl @"http://85.236.153.33:8080/api-little/rest/antiseche"
#define apiDuelUrl @"http://85.236.153.33:8080/api-little/rest/duel"
#define apiCategorieUrl @"http://85.236.153.33:8080/api-little/rest/categories"
#define apiProgressionUrl @"http://85.236.153.33:8080/api-little/rest/progression"
#define apiReponsesUrl @"http://85.236.153.33:8080/api-little/rest/reponse"
#define apiSoumissionUrl @"http://85.236.153.33:8080/api-little/rest/soumission"


#define apiVideoUrl @"http://85.236.153.33:8080/api-little/rest/video"
#define apiStatistiquesUrl @"http://85.236.153.33:8080/api-little/rest/statistiques"

#define urlCredit @"http://www.youtube.com/watch?v=m4-89PqmsOU"

#define urlReglement @"http://quetzalgames.com/documents/20121115_ReglementCP_ValidéHuissier.pdf"

