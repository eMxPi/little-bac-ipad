//
//  ViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 03/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionsView.h"
#import "ReglagesView.h"
#import "ConnexionViewController.h"
#import "ProfilViewController.h"
#import "SoloViewController.h"
#import "TutoSoloViewController.h"
#import "TrousseViewController.h"
#import "AudioController.h"
#import "ApiUtils.h"
#import "SelectionDuelViewController.h"
#import "ApplicationCell.h"
#import "ContactCell.h"
#import "app.h"
#import "CreditCell.h"
#import "CreditImpairCell.h"
#import "credit.h"
#import "StatistiquesRequest.h"
#import "EmptyCellAccueil.h"

@interface ViewController : UIViewController <UIGestureRecognizerDelegate, ConnexionViewControllerDelegate, ProfilViewControllerDelegate, SoloViewControllerDelegate, TutoSoloViewControllerDelegate, TrousseViewControllerDelegate, SelectionDuelViewControllerDelegate, UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UIView *accueilView;
    IBOutlet UIImageView *accueilLogo;
    IBOutlet OptionsView *optionsView;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageOptions;
    IBOutlet UIImageView *imageOptionsDeplie;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    BOOL isNotifications;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    IBOutlet UIButton *notifButton;
    IBOutlet UILabel *notifLabel;
    IBOutlet UILabel *creditLabel;
    IBOutlet UILabel *howPlayLabel;
    IBOutlet UILabel *otherAppLabel;
    IBOutlet UIButton *profilButton;
    IBOutlet UIButton *soloButton;
    IBOutlet UIButton *storeButton;
    IBOutlet UIButton *duelButton;
    IBOutlet UILabel *notifDuelLabel;
    ConnexionViewController *connexionView;
    ProfilViewController *profileView;
    SoloViewController *soloView;
    TutoSoloViewController *tutoSoloView;
    TrousseViewController *trousseView;
    SelectionDuelViewController *selectionView;
    AudioController *audioController;
    ApiUtils *utils;
    IBOutlet UILabel *deconnexionReglagesLabel;
    IBOutlet UIView *creditView;
    IBOutlet UITextView *creditText;
    NSString *lettreSelected;
    IBOutlet UITableView *appTableView;
    NSMutableArray *appDataSource;
    IBOutlet UITableView *creditTableView;
    NSMutableArray *creditDataSource;
}

- (IBAction)profilPressed:(id)sender;
- (IBAction)soloPressed:(id)sender;
- (IBAction)storePressed:(id)sender;
- (IBAction)duelPressed:(id)sender;

-(void)didReceiveDuel;

// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)activeNotif:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

// Options
- (IBAction)creditsPressed:(id)sender;
- (IBAction)howToPlayPressed:(id)sender;
- (IBAction)otherAppPressed:(id)sender;
- (IBAction)hideCredit:(id)sender;

@end
