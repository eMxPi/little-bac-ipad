//
//  AudioController.h
//  Captain Price
//
//  Created by Maxime Pontoire on 18/09/12.
//  Copyright (c) 2012 Maxime Pontoire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface AudioController : NSObject 

-(void)playSound:(NSString *)filename extension:(NSString *)extension;

@end
