//
//  SoloViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 05/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLToObjectParser.h"
#import "CategorieCell.h"
#import "categorie.h"
#import "CustomAlert.h"
#import "Reachability.h"
#import "listeAPI.h"
#import "ReglagesView.h"
#import "UIImage+ProportionalFill.h"
#import "ApiUtils.h"
#import "AudioController.h"
#import <AVFoundation/AVFoundation.h>
#import "CorrectionViewController.h"
#import "AFPickerView.h"
#import "ApiUtils.h"
#import "StatistiquesRequest.h"

@protocol SoloViewControllerDelegate;
@interface SoloViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, CorrectionViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, AFPickerViewDataSource, AFPickerViewDelegate> {
    id<SoloViewControllerDelegate>__unsafe_unretained delegate;
    IBOutlet UITextField *reponseField;
    IBOutlet UITableView *categorieTableView;
    CustomAlert *alert;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    IBOutlet UIImageView *tempsImage;
    IBOutlet UILabel *tempsLabel;
    IBOutlet UIImageView *plusUnImage;
    IBOutlet UILabel *plusUnLabel;
    IBOutlet UIImageView *suivantImage;
    IBOutlet UILabel *suivantLabel;
    IBOutlet UILabel *deconnexionReglagesLabel;
    int secondes;
    int remainingSeconds;
    BOOL tempEcoule;
    NSTimer *bonusTimer;
    IBOutlet UIImageView *timerFond;
    IBOutlet UIImageView *timerImage;
    int sizeToMove;
    ApiUtils *utils;
    AudioController *audioController;
    AVAudioPlayer *audioPlayer;
    IBOutlet UIImageView *imageProfil;
    IBOutlet UILabel *duelTitreLabel;
    IBOutlet UILabel *nomDuelLabel;
    CorrectionViewController *correctionView;
    NSString *mots;
    NSString *categories;
    IBOutlet UILabel *finiLabel;
    int reponsesTaped;
    BOOL isAction;
    IBOutlet UIPickerView *categoriePicker;
    int selIndex;
    NSMutableArray *categoriesReponse;
    NSMutableArray *motsReponse;
    
    IBOutlet UIButton *retourButton;
    IBOutlet AFPickerView *cateView;
    // TempsBonus
    
    int secondesBonus;
    int remainingSecondsBonus;
    BOOL tempEcouleBonus;
    NSTimer *bonusTimerTemps;
    IBOutlet UIView *correctionAlertView;
    IBOutlet UIView *correctionAlertSoloView;
    IBOutlet UIView *correctionAlertDefiView;
    IBOutlet UILabel *titleText;
    IBOutlet UILabel *detailText;
    IBOutlet UILabel *correctionDefiTitle;
    IBOutlet UILabel *correctionDefiAmi;
    IBOutlet UILabel *correctionDefiDetail;
    IBOutlet UILabel *correctionDefiMessage;
    IBOutlet UIImageView *correctionDefiAmiImage;
}
@property (nonatomic, unsafe_unretained) id<SoloViewControllerDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, retain) IBOutlet UIImageView *imageLettre;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isDefi;
@property (nonatomic) duel *currentDuel;

- (void)playAudio;

- (IBAction)retourPressed:(id)sender;

// Bonus Actions
- (IBAction)tempsPressed:(id)sender;
- (IBAction)plusUnPressed:(id)sender;
- (IBAction)suivantPressed:(id)sender;

// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

// Correction
- (IBAction)correctionPressed:(id)sender;
- (IBAction)correctionDefiPressed:(id)sender;

@end

@protocol SoloViewControllerDelegate
-(void)retourSoloPressed;
-(void)retourDuelPressed:(duel*)duel;
-(void)rejouerPressed:(BOOL)defiEnable;
-(void)nouvelleLettrePressed:(NSString *)lettre isDefi:(BOOL)defiEnable;
@required
@end
