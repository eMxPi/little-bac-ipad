//
//  app.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 28/02/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface app : NSObject{
    NSString *nom;
    NSString *url;
    NSString *detail;
    NSString *image;
}
@property (nonatomic, retain) NSString *nom;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *image;

@end
