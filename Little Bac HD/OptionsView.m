//
//  OptionsView.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 03/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "OptionsView.h"

@implementation OptionsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
