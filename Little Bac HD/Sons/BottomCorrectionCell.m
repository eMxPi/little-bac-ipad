//
//  BottomCorrectionCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "BottomCorrectionCell.h"

@implementation BottomCorrectionCell
@synthesize imageJoueurUn, imageJoueurDeux, nomJoueurUn, nomJoueurDeux, tamponJoueurUn, tamponJoueurDeux, avatarJoueurDeux, avatarJoueurUn, resultImage, suggestionText;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
