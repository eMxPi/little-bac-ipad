//
//  EnTeteCorrectionCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnTeteCorrectionCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imageJoueurUn;
@property (strong, nonatomic) IBOutlet UILabel *nomJoueurUn;
@property (strong, nonatomic) IBOutlet UILabel *scoreJoueurUn;
@property (strong, nonatomic) IBOutlet UIImageView *imageLettre;
@property (strong, nonatomic) IBOutlet UIImageView *imageJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *nomJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *scoreJoueurDeux;

@end
