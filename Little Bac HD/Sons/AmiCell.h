//
//  AmiCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 19/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmiCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UIImageView *amiImage;
@property (strong, nonatomic) IBOutlet UILabel *amiName;

@end
