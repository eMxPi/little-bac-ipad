//
//  EnTeteCorrectionCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "EnTeteCorrectionCell.h"

@implementation EnTeteCorrectionCell
@synthesize imageLettre, imageJoueurUn, imageJoueurDeux, nomJoueurUn, nomJoueurDeux, scoreJoueurUn, scoreJoueurDeux;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
