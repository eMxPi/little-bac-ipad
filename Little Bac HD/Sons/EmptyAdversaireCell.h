//
//  EmptyAdversaireCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 08/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyAdversaireCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end
