//
//  AmiCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 19/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "AmiCell.h"

@implementation AmiCell
@synthesize amiImage, amiName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    [amiName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
