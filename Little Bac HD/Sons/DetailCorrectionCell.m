//
//  DetailCorrectionCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "DetailCorrectionCell.h"

@implementation DetailCorrectionCell
@synthesize scoreJoueurUn, scoreJoueurDeux, categorieLabel, reponseJoueurUn, reponseJoueurDeux, scoreUnLabel, scoreDeuxLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)submitWord:(id)sender {
    if ([reponseJoueurUn.text length] > 0 && ![reponseJoueurUn.text isEqualToString:@""] && ![reponseJoueurUn.text isEqualToString:@" "]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Une suggestion ?" message:[NSString stringWithFormat:@"Tu n'es pas d'accord et souhaites suggérer le mot : %@ dans la catégorie : %@", reponseJoueurUn.text, categorieLabel.text]
                                          delegate:self
                                 cancelButtonTitle:@"Annuler"
                                 otherButtonTitles:@"Suggérer",nil];
        [alert show];
        alert = nil;
    }
}


#pragma mark UIalertFirstViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = alertView.title;
    if ([title isEqualToString:@"Une suggestion ?"] && buttonIndex == 1) {
        if (utils == nil) {
            utils  = [[ApiUtils alloc] init];
        }
        resultat *res = [utils submitWord:reponseJoueurUn.text categorie:categorieLabel.text];
        if ([res.message isEqualToString:@"OK"]) {
            alert = [[CustomAlert alloc] initWithTitle:@"C'est noté !" message:@"Ton mot a été soumis. Il sera validé avant d'être ajouté."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        } else {
            
            alert = [[CustomAlert alloc] initWithTitle:@"Erreur !" message:@"Une erreur est survenue lors de l'envoi de ton mot. Retentes plus tard."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        }
    }
}
@end
