//
//  SelectionDuelViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 18/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "SelectionDuelViewController.h"

@interface SelectionDuelViewController ()

@end

@implementation SelectionDuelViewController
@synthesize delegate, duelCorrect;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    
    [duelEnCoursLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
    [invitationLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
    
    
    [invitationTitre setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [derniersAdversaireTitre setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
    [amiListTitre setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    [duelEnCoursTableView setDataSource:self];
    [duelEnCoursTableView setDelegate:self];
    [invationTableView setDataSource:self];
    [invationTableView setDelegate:self];
    [amiListTableView setDataSource:self];
    [amiListTableView setDelegate:self];
    [dernierAdversaireTableView setDataSource:self];
    [dernierAdversaireTableView setDelegate:self];
    
    [searchFriendField setDelegate:self];
    [searchFriendField setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
    
    
    [amiLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [detailText setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [confirmationInvitationLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [confirmationAmiLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    
    
    [refuserLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:10]];
    [accepterLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:10]];
    
    
    [shuffleUser setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [shuffleTitle setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    
    
    [shuffleRefuserLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:10]];
    [shuffleAccepterLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:10]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshInvitations) name:@"refreshInvitations" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDernier) name:@"refreshDernier" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAll) name:@"refreshAll" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDuels) name:@"refreshDuels" object:nil];
    
    
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Selection Duel Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Selection Duel Affiche"];
    
    utilisateurList = nil;
}


-(void)refreshDuels {
    // Parse
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        duelEnCoursDataSource = [utils getDuelsEnCours];
        dispatch_async(dispatch_get_main_queue(), ^{
            [duelEnCoursTableView reloadData];
            [duelEnCoursLabel setText:[NSString stringWithFormat:@"%i", [duelEnCoursDataSource count]]];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [utils resetBadge];
        });
    });
    
}


-(void)refreshAll {
    [self loadDuels];
    
    // Parse
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void)refreshInvitations {
    // Parse
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        invitationDataSource = [utils getDuelsInvite];
        dispatch_async(dispatch_get_main_queue(), ^{
            [invationTableView reloadData];
            [invitationLabel setText:[NSString stringWithFormat:@"%i", [invitationDataSource count]]];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [utils resetBadge];
        });
    });
}



-(void)refreshDernier {
    // Parse
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        derniersAdversairesDataSource = [utils getDuelFinished];
        dispatch_async(dispatch_get_main_queue(), ^{
            derniersAdversairesDataSource = [utils getDuelFinished];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [utils resetBadge];
        });
    });
}

-(void)viewWillAppear:(BOOL)animated {
    isLoadingFriend = FALSE;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *profileName = [defaults stringForKey:nameProfil];
    if (profileName == nil || [profileName isEqualToString:@""]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Rejoins-moi !" message:@"Il faut te connecter pour défier tes amis !"
                                          delegate:self
                                 cancelButtonTitle:@"Plus tard"
                                 otherButtonTitles:@"Se connecter",nil];
        [alert show];
        alert = nil;
    } else {
        [self loadProfile];
        [self loadDuels];
        [self loadFriends];
        
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [utils resetBadge];
        if (duelCorrect != nil) {
            NSMutableArray *corrections =  [self parseCorrections:[utils validateDuel:duelCorrect.id]];
            // Affichage de la correction
            if (!correctionView) {
                correctionView = [[CorrectionViewController alloc]initWithNibName:@"CorrectionViewController" bundle:nil];
                [correctionView setDelegate:self];
            }
            [correctionView.view setFrame:CGRectMake(0, 0, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
            correctionView.imageName = [NSString stringWithFormat:@"%@.png",duelCorrect.lettre];
            correctionView.isDefi = TRUE;
            correctionView.currentDuel = duelCorrect;
            correctionView.lettre = duelCorrect.lettre;
            correctionView.correctionDataSource = corrections;
            [self.view addSubview:correctionView.view];
        }
    }
    loaded = FALSE;
}

-(NSMutableArray *)parseCorrections:(NSMutableArray*)input {
    NSMutableArray *output = [[NSMutableArray alloc] init];
    reponse *rep = [[reponse alloc] init];
    [rep setCategorie:@"vide"];
    [output addObject:rep];
    for (int i=0;i<[input count];i++) {
        [output addObject:[input objectAtIndex:i]];
    }
    reponse *rep2 = [[reponse alloc] init];
    [rep2 setCategorie:@"vide"];
    [output addObject:rep2];
    return output;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadFriends {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:isFacebookActive] && [defaults stringForKey:nameProfil] != nil && ![[defaults stringForKey:nameProfil] isEqualToString:@""]) {
        [self getFBFriends];
        isFacebook = TRUE;
    } else {
        amiListDataSource = nil;
        isFacebook = FALSE;
    }
}

-(void)loadDuels {
    // Parse
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        duelEnCoursDataSource = [utils getDuelsEnCours];
        dispatch_async(dispatch_get_main_queue(), ^{
            [duelEnCoursTableView reloadData];
            [duelEnCoursLabel setText:[NSString stringWithFormat:@"%i", [duelEnCoursDataSource count]]];
        });
    });
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        invitationDataSource = [utils getDuelsInvite];
        dispatch_async(dispatch_get_main_queue(), ^{
            [invationTableView reloadData];
            [invitationLabel setText:[NSString stringWithFormat:@"%i", [invitationDataSource count]]];
        });
    });
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        derniersAdversairesDataSource = [utils getDuelFinished];
        dispatch_async(dispatch_get_main_queue(), ^{
            [dernierAdversaireTableView reloadData];
            [utils resetBadge];
        });
    });
}

- (IBAction)fbInvitationPressed:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self sendFBWithText:[NSString stringWithFormat:@"Viens me défier sur Little Bac. %@ t'attend à ses côtés pour réviser avec lui : http://bit.ly/WQBorI", [defaults stringForKey:nameProfil]] andImage:[UIImage imageNamed:@"icone.png"] showSheet:YES];
}

- (IBAction)mailInvitationPressed:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self sendMailWithText:[NSString stringWithFormat:@"Viens me défier sur Little Bac. %@ t'attend à ses côtés pour réviser avec lui : http://bit.ly/WQBorI", [defaults stringForKey:nameProfil]]];
}

- (IBAction)fbListAmiPressed:(id)sender {
    [fbListPointer setHidden:FALSE];
    [amiListTitre setText:@"Défier un ami Facebook"];
    [randomListPointer setHidden:TRUE];
    amiListDataSource = fbFriendsList;
    [amiListTableView reloadData];
    if (!loaded) {
        [amiListTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        loaded = TRUE;
    }
    isFacebook = TRUE;
    [searchFriendField setText:@""];
}

- (IBAction)randomListAmiPressed:(id)sender {
    
    if (utilisateurList == nil) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            isLoadingFriend = TRUE;
            [amiListTableView reloadData];
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            utilisateurList = [utils getUtilisateursFiltered];
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                isLoadingFriend = FALSE;
                amiListDataSource = utilisateurList;
                [amiListTableView reloadData];
            });
        });
    }
    [fbListPointer setHidden:TRUE];
    [amiListTitre setText:@"Recherche par pseudo"];
    [randomListPointer setHidden:FALSE];
    isFacebook = FALSE;
    [amiListTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [searchFriendField setText:@""];
    amiListDataSource = utilisateurList;
    [amiListTableView reloadData];
}

- (IBAction)retourPressed:(id)sender {
    [delegate retourSelectionDuelPressed];
}

- (IBAction)shuffleListAmi:(id)sender {
    utilisateur *util = [utilisateurList objectAtIndex:[self getRandomNumber:0 to:[utilisateurList count]-1]];
    if (util != nil) {
        currentUtil = util;
        [self displayShuffleConfirmation];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Système d'invitation indisponible."
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

-(void)displayShuffleConfirmation {
    [shuffleUser setText:currentUtil.id];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:currentUtil.urlImage]];
        if ( data == nil ) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
            [shuffleUserImage setImage:newImage];
        });
    });
    [confirmationDuel setHidden:FALSE];
    [confirmationDetailDuel setHidden:TRUE];
    [confirmationInvitationDuel setHidden:TRUE];
    [confirmationShuffleView setHidden:FALSE];
    
}

- (IBAction)acceptShuffle:(id)sender {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    resultat *res = [utils addInvitation:currentUtil.id lettre:[self generateLettre]];
    if (res && ![res.message isEqualToString:@"KO"]) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            resultat *result = [utils sendPush:currentUtil.id];
            if ( result == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (![result.message isEqualToString:@"OK"]) {
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur " message:@"Une erreur est survenue lors de l'envoi de la notification à ton adversaire. Pas de panique, il a bien reçu le duel."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                }
            });
        });
    }
    invitationDataSource = [utils getDuelsInvite];
    [invationTableView reloadData];
    duelEnCoursDataSource = [utils getDuelsEnCours];
    [duelEnCoursLabel setText:[NSString stringWithFormat:@"%i", [duelEnCoursDataSource count]]];
    [duelEnCoursTableView reloadData];
    
    
    [confirmationDuel setHidden:TRUE];
    [confirmationDetailDuel setHidden:FALSE];
    [confirmationInvitationDuel setHidden:FALSE];
    [confirmationShuffleView setHidden:TRUE];
}

- (IBAction)refuseShuffle:(id)sender {
    [confirmationDuel setHidden:TRUE];
    [confirmationDetailDuel setHidden:FALSE];
    [confirmationInvitationDuel setHidden:FALSE];
    [confirmationShuffleView setHidden:TRUE];
}

-(int)getRandomNumber:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

#pragma mark Table view methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == duelEnCoursTableView) {
        return duelEnCoursDataSource.count > 0 ? duelEnCoursDataSource.count : 1;
    } else if (tableView == invationTableView) {
        return invitationDataSource.count > 0 ? invitationDataSource.count : 1;
    } else if (tableView == amiListTableView) {
        return amiListDataSource.count > 0 ? amiListDataSource.count : 1;
    } else if (tableView == dernierAdversaireTableView) {
        return derniersAdversairesDataSource.count > 0 ? derniersAdversairesDataSource.count : 1;
    } else {
        return 0;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == duelEnCoursTableView) {
        if (duelEnCoursDataSource != nil && [duelEnCoursDataSource count] > 0) {
            NSString *cellIdentifier = @"duelCell";
            DuelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DuelCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[DuelCell class]])
                    {
                        cell = (DuelCell *)currentObject;
                        break;
                    }
                }
            }
            duel *current = [duelEnCoursDataSource objectAtIndex:indexPath.row];
            if (current) {
                [cell.amiName setText:current.joueurDeux];
                [cell.amiAction setText:current.StatutJoueurDeux];
                dispatch_async(dispatch_get_global_queue(0,0), ^{
                    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.imageJoueurDeux]];
                    if ( data == nil ) {
                        return;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
                        [cell.amiImage setImage:newImage];
                    });
                });
            }
            [cell.amiName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            [cell.amiAction setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.amiName.adjustsFontSizeToFitWidth = YES;
            cell.amiAction.adjustsFontSizeToFitWidth = YES;
            if ([current.StatutJoueurDeux isEqualToString:@"Invitation reçue"] || [current.StatutJoueurDeux isEqualToString:@"En attente"]) {
                [cell.shadowImage setHidden:FALSE];
            }
            return cell;
        } else {
            NSString *cellIdentifier = @"emptyDuelCell";
            EmptyDuelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyDuelCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyDuelCell class]])
                    {
                        cell = (EmptyDuelCell *)currentObject;
                        break;
                    }
                }
            }
            [cell.textLabel setText:@"Aucun duel"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            return cell;
        }
    } else if (tableView == invationTableView) {
        if (invitationDataSource != nil && [invitationDataSource count] > 0) {
            NSString *cellIdentifier = @"duelInvitationCell";
            DuelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DuelCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[DuelCell class]])
                    {
                        cell = (DuelCell *)currentObject;
                        break;
                    }
                }
            }
            duel *current = [invitationDataSource objectAtIndex:indexPath.row];
            [cell.amiName setText:current.joueurDeux];
            [cell.amiAction setText:current.StatutJoueurDeux];
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.imageJoueurDeux]];
                if ( data == nil ) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
                    [cell.amiImage setImage:newImage];
                });
            });
            [cell.amiName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            [cell.amiAction setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.amiName.adjustsFontSizeToFitWidth = YES;
            cell.amiAction.adjustsFontSizeToFitWidth = YES;
            return cell;
        } else {
            NSString *cellIdentifier = @"emptyDuelCell";
            EmptyDuelCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyDuelCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyDuelCell class]])
                    {
                        cell = (EmptyDuelCell *)currentObject;
                        break;
                    }
                }
            }
            [cell.textLabel setText:@"Aucune invitation"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            return cell;
        }
    } else if (tableView == dernierAdversaireTableView) {
        NSString *cellIdentifier = @"dernierCell";
        DernierCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DernierCell" owner:nil options:nil];
            
            for(id currentObject in topLevelObjects)
            {
                if([currentObject isKindOfClass:[DernierCell class]])
                {
                    cell = (DernierCell *)currentObject;
                    break;
                }
            }
        }
        if (derniersAdversairesDataSource != nil && [derniersAdversairesDataSource count] > 0) {
            duel *current = [derniersAdversairesDataSource objectAtIndex:indexPath.row];
            [cell.amiName setText:current.joueurDeux];
            [cell.amiAction setText:@"Voir la correction"];
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.imageJoueurDeux]];
                if ( data == nil ) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
                    [cell.amiImage setImage:newImage];
                });
            });
            
            [cell.amiName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            [cell.amiAction setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.amiName.adjustsFontSizeToFitWidth = YES;
            cell.amiAction.adjustsFontSizeToFitWidth = YES;
            if ([current.vuCorrUn isEqualToString:@"true"]) {
                [cell.shadowImage setImage:[UIImage imageNamed:@"duel_choix_opacite_corr.png"]];
                [cell.shadowImage setHidden:FALSE];
            }
            return cell;
        } else {
            NSString *cellIdentifier = @"emptyAdvTextCell";
            EmptyAdversaireCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyAdversaireCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyAdversaireCell class]])
                    {
                        cell = (EmptyAdversaireCell *)currentObject;
                        break;
                    }
                }
            }
            [cell.textLabel setText:@"Aucun duel"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            return cell;
        }
    } else {
        NSString *cellIdentifier = @"amiCell";
        AmiCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AmiCell" owner:nil options:nil];
            
            for(id currentObject in topLevelObjects)
            {
                if([currentObject isKindOfClass:[AmiCell class]])
                {
                    cell = (AmiCell *)currentObject;
                    break;
                }
            }
        }
        if (amiListDataSource != nil && [amiListDataSource count] > 0) {
            utilisateur *current = [amiListDataSource objectAtIndex:indexPath.row];
            if (!isFacebook) {
                [cell.amiName setText:current.id];
            } else {
                [cell.amiName setText:[NSString stringWithFormat:@"%@ %@", current.prenom, current.nom]];
            }
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.urlImage]];
                if ( data == nil ) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
                    [cell.amiImage setImage:newImage];
                });
            });
            cell.amiName.adjustsFontSizeToFitWidth = YES;
            [cell.amiName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            return cell;
        } else {
            NSString *cellIdentifier = @"emptyTextCell";
            EmptyTextCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyTextCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyTextCell class]])
                    {
                        cell = (EmptyTextCell *)currentObject;
                        break;
                    }
                }
            }
            if (isLoadingFriend) {
                [cell.textLabel setText:@"Chargement en cours"];
            } else {
                [cell.textLabel setText:@"Aucun ami"];
            }
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            [cell.textLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            return cell;
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == duelEnCoursTableView) {
        duelSelected = [duelEnCoursDataSource objectAtIndex:indexPath.row];
        /*if (utils == nil) {
         utils = [[ApiUtils alloc] init];
         }
         [utils vuDuel:duelSelected.id];
         duelSelected.vuDuelUn = @"true";
         [duelEnCoursDataSource replaceObjectAtIndex:indexPath.row withObject:duelSelected];
         [duelEnCoursTableView reloadData];*/
        if ([duelSelected.StatutJoueurDeux isEqualToString:@"Confirmé"] || [duelSelected.StatutJoueurDeux isEqualToString:@"En attente"] || [duelSelected.StatutJoueurDeux isEqualToString:@"Terminé"] || [duelSelected.StatutJoueurDeux isEqualToString:@"A toi de jouer !"] ) {
            
            if ([duelSelected.StatutJoueurUn isEqualToString:@"Terminé"] || [duelSelected.StatutJoueurDeux isEqualToString:@"En attente"]) {
                alert = [[CustomAlert alloc] initWithTitle:@"Patience" message:@"Ton camarade est en train de jouer"
                                                  delegate:self
                                         cancelButtonTitle:@"Patienter"
                                         otherButtonTitles:@"Abandonner",nil];
                [alert show];
                alert = nil;
            } else {
                if (utils == nil) {
                    utils = [[ApiUtils alloc] init];
                }
                resultat *res = [utils demarrerDuel:duelSelected.id];
                if ([res.message isEqualToString:@"OK"]) {
                    [delegate duelSelected:duelSelected];
                } else {
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Veuillez réessayer ultérieurement."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                }
            }
        } else {
            if (duelSelected != nil && duelSelected.StatutJoueurDeux != nil) {
                alert = [[CustomAlert alloc] initWithTitle:@"Patience" message:@"Ton camarade n'a pas encore accepté l'invitation"
                                                  delegate:self
                                         cancelButtonTitle:@"Patienter"
                                         otherButtonTitles:@"Abandonner",nil];
                [alert show];
                alert = nil;
            }
        }
    } else if (tableView == amiListTableView) {
        utilisateur *util = nil;
        if (isFacebook) {
            if (fbFriendsList == nil || [fbFriendsList count] == 0) {
                alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Vous n'êtes pas connecté avec votre compte Facebook"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
                [alert show];
                alert = nil;
            } else {
                if (utils == nil) {
                    utils = [[ApiUtils alloc] init];
                }
                util = [amiListDataSource objectAtIndex:indexPath.row];
            }
        } else {
            if (utilisateurList == nil || [utilisateurList count] == 0) {
                alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Aucun ami disponible."
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
                [alert show];
                alert = nil;
            } else {
                if (utils == nil) {
                    utils = [[ApiUtils alloc] init];
                }
                util = [amiListDataSource objectAtIndex:indexPath.row];
            }
        }
        if (!isLoadingFriend) {
            if (util != nil) {
                currentUtil = util;
                if (!isFacebook) {
                    [self displayConfirmation];
                } else {
                    if (![utils checkFacebookFromUtilisateur:currentUtil.facebook]) {
                        [self sendFBMessage:currentUtil.id];
                    } else {
                        [self addLittles];
                        [self displayConfirmation];
                    }
                }
            } else {
                alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Système d'invitation indisponible."
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
                [alert show];
                alert = nil;
            }
        }
    } else if (tableView == invationTableView) {
        duelSelected = [invitationDataSource objectAtIndex:indexPath.row];
        if (duelSelected != nil) {
            [amiLabel setText:duelSelected.joueurDeux];
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:duelSelected.imageJoueurDeux]];
                if ( data == nil ) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
                    [amiImage setImage:newImage];
                });
            });
            [confirmationDuel setHidden:FALSE];
            [confirmationDetailDuel setHidden:FALSE];
            [confirmationInvitationDuel setHidden:TRUE];
            [closeButton setHidden:FALSE];
        }
    } else if (tableView == dernierAdversaireTableView) {
        duelSelected = [derniersAdversairesDataSource objectAtIndex:indexPath.row];
        if (duelSelected != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils vuCorr:duelSelected.id];
            duelSelected.vuCorrUn = @"true";
            [derniersAdversairesDataSource replaceObjectAtIndex:indexPath.row withObject:duelSelected];
            [dernierAdversaireTableView reloadData];
            NSMutableArray *corrections =  [self parseCorrections:[utils validateDuel:duelSelected.id]];
            // Affichage de la correction
            if (!correctionView) {
                correctionView = [[CorrectionViewController alloc]initWithNibName:@"CorrectionViewController" bundle:nil];
                [correctionView setDelegate:self];
            }
            [correctionView.view setFrame:CGRectMake(0, 0, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
            correctionView.imageName = [NSString stringWithFormat:@"%@.png",duelSelected.lettre];
            correctionView.isDefi = TRUE;
            correctionView.currentDuel = duelSelected;
            correctionView.lettre = duelSelected.lettre;
            correctionView.correctionDataSource = corrections;
            [self.view addSubview:correctionView.view];
        }
    }
}



#pragma mark UIalertFirstViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = alertView.title;
    if ([title isEqualToString:@"Patience"] && buttonIndex == 1) {
        if (utils == nil) {
            utils  = [[ApiUtils alloc] init];
        }
        resultat *res = [utils removeDuel:duelSelected.id];
        if ([res.message isEqualToString:@"OK"]) {
            alert = [[CustomAlert alloc] initWithTitle:@"C'est noté !" message:@"Ce duel a été supprimé."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        } else {
            alert = [[CustomAlert alloc] initWithTitle:@"Erreur !" message:@"Une erreur est survenue lors de la suppression du duel. Retentes plus tard."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        }
        [self loadDuels];
    } else if ([title isEqualToString:@"Rejoins-moi !"] && buttonIndex == 1) {
        if (!connexionView) {
            connexionView = [[ConnexionViewController alloc]initWithNibName:@"ConnexionViewController" bundle:nil];
            [connexionView setDelegate:self];
        }
        [connexionView.view setFrame:CGRectMake(0, 0, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
        [self.view addSubview:connexionView.view];
    }
}

-(void)displayConfirmation {
    resultat *res = nil;
    if (!isFacebook) {
        res = [utils addInvitation:currentUtil.id lettre:[self generateLettre]];
    } else {
        res = [utils addInvitation:currentUtil.facebook lettre:[self generateLettre]];
    }
    if (res && ![res.message isEqualToString:@"KO"]) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            resultat *result = nil;
            if (!isFacebook) {
                result = [utils sendPush:currentUtil.id];
            } else {
                result = [utils sendPush:currentUtil.facebook];
            }
            if ( result == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([result.message isEqualToString:@"KO"]) {
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur " message:@"Une erreur est survenue lors de l'envoi de la notification à ton adversaire. Pas de panique, il a bien reçu le duel."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                }
            });
        });
    }
    duelEnCoursDataSource = [utils getDuelsEnCours];
    [duelEnCoursLabel setText:[NSString stringWithFormat:@"%i", [duelEnCoursDataSource count]]];
    [duelEnCoursTableView reloadData];
    if (isFacebook) {
        [confirmationAmiLabel setText:currentUtil.facebook];
    } else {
        [confirmationAmiLabel setText:currentUtil.id];
    }
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:currentUtil.urlImage]];
        if ( data == nil ) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
            [confirmationAmiImage setImage:newImage];
        });
    });
    [confirmationDuel setHidden:FALSE];
    [confirmationDetailDuel setHidden:TRUE];
    [confirmationInvitationDuel setHidden:FALSE];
    [closeButton setHidden:TRUE];
}

-(void)getFBFriends{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!FBSession.activeSession.isOpen) {
        // The user has initiated a login, so call the openSession method
        // and show the login UX if necessary.
        if ([appDelegate openSessionWithAllowLoginUI:YES]) {
            [self loadFBFriends];
        }
    } else {
        [self loadFBFriends];
    }
}

-(void)loadFBFriends {
    [[FBRequest requestForMyFriends] startWithCompletionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSArray *data = [result objectForKey:@"data"];
             fbFriendsList = [[NSMutableArray alloc] init];
             for (FBGraphObject<FBGraphUser> *friend in data) {
                 utilisateur *util = [[utilisateur alloc] init];
                 util.id = [friend id];
                 util.facebook = [friend name];
                 util.nom = [friend last_name];
                 util.prenom = [friend first_name];
                 util.urlImage = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [friend id]];
                 [fbFriendsList addObject:util];
             }
             amiListDataSource = fbFriendsList;
             
             NSSortDescriptor *sortDescriptor;
             sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nom"
                                                          ascending:YES];
             NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
             amiListDataSource = [NSMutableArray arrayWithArray:[amiListDataSource sortedArrayUsingDescriptors:sortDescriptors]];
             [amiListTableView reloadData];
         }
     }];
}


- (IBAction)deconnexionPressedButton:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [defaults setValue:@"0" forKey:tempsBonusCurrent];
    [defaults setValue:@"0" forKey:checkBonusCurrent];
    [defaults setValue:@"0" forKey:nextBonusCurrent];
    [self loadProfile];
    [self hideReglages];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    [self retourPressed:nil];
}

- (IBAction)acceptDefi:(id)sender {
    //valideInvitation
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils valideInvitation:duelSelected.id];
    duelEnCoursDataSource = [utils getDuelsEnCours];
    [duelEnCoursLabel setText:[NSString stringWithFormat:@"%i", [duelEnCoursDataSource count]]];
    [duelEnCoursTableView reloadData];
    
    invitationDataSource = [utils getDuelsInvite];
    [invitationLabel setText:[NSString stringWithFormat:@"%i", [invitationDataSource count]]];
    [confirmationDuel setHidden:TRUE];
    [invationTableView reloadData];
    
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    resultat *res = [utils demarrerDuel:duelSelected.id];
    if ([res.message isEqualToString:@"OK"]) {
        [delegate duelSelected:duelSelected];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Veuillez réessayer ultérieurement."
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    
}

- (IBAction)refuseDefi:(id)sender {
    
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeDuel:duelSelected.id];
    invitationDataSource = [utils getDuelsInvite];
    [invitationLabel setText:[NSString stringWithFormat:@"%i", [invitationDataSource count]]];
    [confirmationDuel setHidden:TRUE];
    [confirmationDetailDuel setHidden:TRUE];
    [invationTableView reloadData];
}

- (IBAction)closeDefi:(id)sender {
    [confirmationDuel setHidden:TRUE];
    [confirmationDetailDuel setHidden:TRUE];
    [confirmationInvitationDuel setHidden:TRUE];
}

- (IBAction)sendNotification:(id)sender {
    [confirmationDuel setHidden:TRUE];
    [confirmationDetailDuel setHidden:TRUE];
    [confirmationInvitationDuel setHidden:TRUE];
}

- (IBAction)suppressionCorrection:(id)sender {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    NSString *ids = @"";
    BOOL first = TRUE;
    for (int i=0;i<[derniersAdversairesDataSource count];i++) {
        duel *current = [derniersAdversairesDataSource objectAtIndex:i];
        if ([current.vuCorrUn isEqualToString:@"true"]) {
            if (first) {
                ids = [NSString stringWithFormat:@"%@" , current.id];
            } else {
                ids = [NSString stringWithFormat:@"%@;%@" , ids, current.id];
            }
            first = FALSE;
        }
    }
    [utils hideDuels:ids];
    derniersAdversairesDataSource = [utils getDuelFinished];
    [dernierAdversaireTableView reloadData];
}

-(void)sendFBMessage:(NSString *)userName {
    NSArray* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      @"Rejoins moi",@"name",@"http://www.facebook.com/apps/application.php?id=479502955424555/",@"link" , nil], nil];
    FBSBJSON *jsonWriter = [FBSBJSON new];
    NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Je viens de te défier sur Little Bac HD",@"message",
                                   @"Je viens de te défier", @"notification_text",
                                   @"http://www.facebook.com/apps/application.php?id=479502955424555/",@"link",
                                   @"http://quetzalgames.com/LB/icone@2x.png",@"picture",
                                   userName, @"to",
                                   actionLinksStr, @"actions",
                                   
                                   nil];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.facebook dialog:@"apprequests"
                       andParams:params
                     andDelegate:self];
}

/**
 * A function for parsing URL parameters.
 */
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}

// Handle the request call back
- (void)dialogCompleteWithUrl:(NSURL *)url {
    NSDictionary *params = [self parseURLParams:[url query]];
    NSString *requestID = [params valueForKey:@"request"];
    NSLog(@"Request ID: %@", requestID);
    if (![url query]) {
        NSLog(@"User canceled dialog or there was an error");
        return;
    } else {
        [self addLittles];
        [self displayConfirmation];
    }
}

-(NSString *)generateLettre {
    int random = [self getRandomNumber:1 to:25];
    NSString *lettres = @"ABCDEFGHIJKLMOPNQRSTUVWXYZ";
    NSString *lettre = [lettres substringWithRange:NSMakeRange(random, 1)];
    return lettre;
}

// Invitations
-(void)sendFBHiddenWithText:(NSString *)text andImage:(UIImage *)image showSheet:(BOOL)sheet
{
    if (FBSession.activeSession.isOpen) {
        self.postParams =
        [[NSMutableDictionary alloc] initWithObjectsAndKeys:
         @"http://bit.ly/WQBorI", @"link",
         @"http://quetzalgames.com/LB/icone@2x.png", @"picture",
         @"Little Bac HD", @"name",
         @"Little Bac HD ", @"caption",
         text, @"description",
         nil];
        if ([FBSession.activeSession.permissions
             indexOfObject:@"publish_actions"] == NSNotFound) {
            // No permissions found in session, ask for it
            [FBSession.activeSession
             reauthorizeWithPublishPermissions:
             [NSArray arrayWithObject:@"publish_actions"]
             defaultAudience:FBSessionDefaultAudienceFriends
             completionHandler:^(FBSession *session, NSError *error) {
                 if (!error) {
                     // If permissions granted, publish the story
                     [FBRequestConnection
                      startWithGraphPath:@"apprequests"
                      parameters:self.postParams
                      HTTPMethod:@"POST"
                      completionHandler:^(FBRequestConnection *connection,
                                          id result,
                                          NSError *error) {
                          if (error) {
                              alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Erreur lors de la publication"                                                                  delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil,nil];
                              [alert show];alert = nil;
                          }
                      }];
                     
                 }
             }];
        }
        else {
            [FBRequestConnection
             startWithGraphPath:@"apprequests"
             parameters:self.postParams
             HTTPMethod:@"POST"
             completionHandler:^(FBRequestConnection *connection,
                                 id result,
                                 NSError *error) {
                 if (error) {
                     alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Erreur lors de la publication"                                                                  delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
                     [alert show];alert = nil;
                 }
             }];
        }
    } else {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate openSessionWithAllowLoginUI:YES];
        [self sendFBWithText:text andImage:image showSheet:sheet];
    }
}


-(void)sendFBWithText:(NSString *)text andImage:(UIImage *)image showSheet:(BOOL)sheet
{
    if (FBSession.activeSession.isOpen) {
        DEFacebookComposeViewControllerCompletionHandler completionHandler = ^(DEFacebookComposeViewControllerResult result) {
            switch (result) {
                case DEFacebookComposeViewControllerResultCancelled:
                    NSLog(@"Facebook Result: Cancelled");
                    break;
                case DEFacebookComposeViewControllerResultDone:
                    NSLog(@"Facebook Result: Sent");
                    [self addLittles];
                    //[self displayConfirmation];
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
        DEFacebookComposeViewController *facebookViewComposer = [[DEFacebookComposeViewController alloc] init];
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [facebookViewComposer setInitialText:text];
        [facebookViewComposer addImage:image];
        facebookViewComposer.completionHandler = completionHandler;
        [self presentViewController:facebookViewComposer animated:YES completion:^{
        }];
    } else {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        if ([appDelegate openSessionWithAllowLoginUI:YES]) {
            [self sendFBWithText:text andImage:image showSheet:YES];
        }
    }
}
-(void)sendMailWithText:(NSString *)text {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Rejoins moi sur Little Bac"];
        NSArray *toRecipients = [NSArray arrayWithObjects:nil];
        [mailer setToRecipients:toRecipients];
        NSString *emailBody = text;
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentViewController:mailer animated:YES completion:nil];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Votre téléphone ne supporte pas l'envoi de mail"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            [self addLittles];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)addLittles {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    resultat *res2 = [utils addLittles:@"10"];
    if (res2 == nil){
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Le service semble indisponible. Êtes-vous sûr d'être bien connecté à internet ?"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else if ([res2.message isEqualToString:@"KO"]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Enregistrement indisponible"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

#pragma mark UITextField Delegate

-(BOOL)textFieldShouldClear:(UITextField *)textField {
    if (isFacebook) {
        amiListDataSource = fbFriendsList;
    } else {
        amiListDataSource = utilisateurList;
    }
    [amiListTableView reloadData];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.view setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    NSString *searchText = textField.text;
    if([searchText isEqualToString:@""] || searchText==nil) {
        if (isFacebook) {
            amiListDataSource = fbFriendsList;
        } else {
            amiListDataSource = utilisateurList;
        }
        [amiListTableView reloadData];
        previousSearch = @"";
        return;
    }
    NSMutableArray *searchedTableau = [[NSMutableArray alloc] init];
    NSLog(@"Texte : %@", [searchText uppercaseString]);
    NSMutableArray *tableauComplet = [[NSMutableArray alloc] init];
    if (isFacebook) {
        tableauComplet = fbFriendsList;
    } else {
        tableauComplet = utilisateurList;
    }
    if ([searchText length] < [previousSearch length]) {
        for (int i=0;  i < [tableauComplet count]; i++) {
            utilisateur *currentStation = [tableauComplet objectAtIndex:i];
            NSString *name = [NSString stringWithFormat:@"%@ %@", currentStation.prenom, currentStation.nom];
            if (isFacebook) {
                if ([[name uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound || [[searchText uppercaseString] rangeOfString:[name uppercaseString]].location != NSNotFound ) {
                    [searchedTableau addObject:currentStation];
                }
            } else {
                if ([[currentStation.id uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound || [[searchText uppercaseString] rangeOfString:[currentStation.id uppercaseString]].location != NSNotFound ) {
                    [searchedTableau addObject:currentStation];
                }
            }
        }
    } else {
        for (int i=0;  i < [amiListDataSource count]; i++) {
            utilisateur *currentStation = [amiListDataSource objectAtIndex:i];
            NSString *name = [NSString stringWithFormat:@"%@ %@", currentStation.prenom, currentStation.nom];
            if (isFacebook) {
                if ([[name uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound || [[searchText uppercaseString] rangeOfString:[name uppercaseString]].location != NSNotFound ) {
                    [searchedTableau addObject:currentStation];
                }
            } else {
                if ([[currentStation.id uppercaseString] rangeOfString:[searchText uppercaseString]].location != NSNotFound || [[searchText uppercaseString] rangeOfString:[currentStation.id uppercaseString]].location != NSNotFound ) {
                    [searchedTableau addObject:currentStation];
                }
            }
        }
    }
    amiListDataSource = searchedTableau;
    previousSearch = searchText;
    if ([amiListDataSource count] == 0) {
        utilisateur *emptyMarker = [[utilisateur alloc] init];
        emptyMarker.id = @"Aucun ami";
        emptyMarker.prenom = @"Aucun ami";
        emptyMarker.nom = @"";
        [amiListDataSource addObject:emptyMarker];
        previousSearch = @"";
    }
    [amiListTableView reloadData];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0.0f, -170.0f, self.view.frame.size.width, self.view.frame.size.height)];
    
}




-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark CorrectionViewControllerDelegate
-(void)retourCorrectionPressed {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
    [self loadDuels];
}

-(void)rejouerPressed:(BOOL)defiEnable {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
}

-(void)nouvelleLettreCorrection:(NSString *)lettre isDefi:(BOOL)defiEnable {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
}

-(void)nouvelleLettreDuelCorrection:(duel *)duel {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
    [self sendInvitationForReplay:FALSE duel:duel];
}

-(void)rejouerDuelCorrection:(duel *)duel {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
    [self sendInvitationForReplay:TRUE duel:duel];
}

-(void)sendInvitationForReplay:(BOOL *)replay duel:(duel*)duel{
    resultat *res = nil;
    if (replay) {
        res = [utils addInvitation:duel.joueurDeux lettre:duel.lettre];
    } else {
        res = [utils addInvitation:duel.joueurDeux lettre:[self generateLettre]];
    }
    if (res && ![res.message isEqualToString:@"KO"]) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            resultat *result = nil;
            result = [utils sendPush:duel.joueurDeux];
            if ( result == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (![result.message isEqualToString:@"OK"]) {
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur " message:@"Une erreur est survenue lors de l'envoi de la notification à ton adversaire. Pas de panique, il a bien reçu le duel."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                }
            });
        });
    }
    duelEnCoursDataSource = [utils getDuelsEnCours];
    [duelEnCoursLabel setText:[NSString stringWithFormat:@"%i", [duelEnCoursDataSource count]]];
    [duelEnCoursTableView reloadData];
    [confirmationAmiLabel setText:duel.joueurDeux];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:duel.imageJoueurDeux]];
        if ( data == nil ) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(61.0f, 61.0f)];
            [confirmationAmiImage setImage:newImage];
        });
    });
    [confirmationDuel setHidden:FALSE];
    [confirmationDetailDuel setHidden:TRUE];
    [confirmationInvitationDuel setHidden:FALSE];
    [closeButton setHidden:TRUE];
}

-(void)disconnectCorrectionReglagesPressed {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [defaults setValue:@"0" forKey:tempsBonusCurrent];
    [defaults setValue:@"0" forKey:checkBonusCurrent];
    [defaults setValue:@"0" forKey:nextBonusCurrent];
    [self loadProfile];
    [self retourPressed:nil];
}


// Reglages
-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}


-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}


#pragma mark ConnexionViewControllerDelegate
-(void)validatePressedDetail:(BOOL)nouveau {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [self viewWillAppear:NO];
    [Flurry logEvent:@"Accueil Affiche Connexion validee"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche Connexion validee"];
}

-(void)retourConnexionPressed {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [self viewWillAppear:NO];
    [Flurry logEvent:@"Accueil Affiche Connexion retour"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche Connexion retour"];
}

-(void)disconnectConnexionReglagesPressed {
    [self retourConnexionPressed];
    [self disconnectCorrectionReglagesPressed];
    [Flurry logEvent:@"Accueil Affiche deconnexion "];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche deconnexion"];
}
@end
