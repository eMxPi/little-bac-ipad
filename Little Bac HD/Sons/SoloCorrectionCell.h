//
//  SoloCorrectionCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 27/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiUtils.h"
#import "resultat.h"
#import "CustomAlert.h"

@interface SoloCorrectionCell : UITableViewCell <UIAlertViewDelegate> {
    ApiUtils *utils;
    CustomAlert *alert;
}
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *reponseLabel;
@property (strong, nonatomic) IBOutlet UILabel *categorieLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageReponse;

- (IBAction)submitWordPressed:(id)sender;

@end
