//
//  BottomCorrectionCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomCorrectionCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imageJoueurUn;
@property (strong, nonatomic) IBOutlet UILabel *nomJoueurUn;
@property (strong, nonatomic) IBOutlet UIImageView *tamponJoueurUn;
@property (strong, nonatomic) IBOutlet UIImageView *avatarJoueurUn;
@property (strong, nonatomic) IBOutlet UIImageView *avatarJoueurDeux;
@property (strong, nonatomic) IBOutlet UIImageView *imageJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *nomJoueurDeux;
@property (strong, nonatomic) IBOutlet UIImageView *tamponJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *scoreJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *scoreJoueurUn;
@property (strong, nonatomic) IBOutlet UIImageView *resultImage;
@property (strong, nonatomic) IBOutlet UILabel *suggestionText;

@end
