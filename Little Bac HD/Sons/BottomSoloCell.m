//
//  BottomSoloCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 28/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "BottomSoloCell.h"

@implementation BottomSoloCell
@synthesize imageJoueur, nomJoueur, recordJoueur, scoreLabel, suggestionText;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
