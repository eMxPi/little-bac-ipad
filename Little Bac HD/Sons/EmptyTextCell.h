//
//  EmptyTextCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 03/02/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyTextCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end
