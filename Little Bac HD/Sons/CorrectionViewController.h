//
//  CorrectionViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 21/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReglagesView.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "DEFacebookComposeViewController.h"
#import "ApiUtils.h"
#import "AppDelegate.h"
#import "EnTeteCorrectionCell.h"
#import "duel.h"
#import "DetailCorrectionCell.h"
#import "reponse.h"
#import "BottomCorrectionCell.h"
#import "SoloCorrectionCell.h"
#import "EmptyCell.h"
#import "BottomSoloCell.h"
#import "StatistiquesRequest.h"

@protocol CorrectionViewControllerDelegate;
@interface CorrectionViewController : UIViewController <UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource> {
    id<CorrectionViewControllerDelegate>__unsafe_unretained delegate;
    
    //Reglages
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    IBOutlet UILabel *deconnexionReglagesLabel;
    ApiUtils *utils;
    
    IBOutlet UITableView *correctionTable;
    NSMutableArray *correctionDataSource;
    IBOutlet UIImageView *fondFeuille;
    int scoreSolo;
    int scoreDuo;
    UIImage *imageJoueurUn;
    UIImage *imageJoueurDeux;
    NSString *recordJoueurUn;
    NSString *recordJoueurDeux;
    IBOutlet UIImageView *nouvelleLettreTitre;
    IBOutlet UIButton *nouvelleLettreButton;
    IBOutlet UIImageView *rejouerTitre;
    IBOutlet UIButton *rejouerButton;
    int totalUn;
    int totalDeux;
    IBOutlet UIImageView *bonusLittlesImage;
}

@property (nonatomic, unsafe_unretained) id<CorrectionViewControllerDelegate>delegate;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic) BOOL isDefi;
@property (nonatomic) duel *currentDuel;
@property (nonatomic, retain) NSMutableArray *correctionDataSource;
@property (nonatomic, retain) NSString *lettre;

- (IBAction)nouvelleLettrePressed:(id)sender;
- (IBAction)rejouerPressed:(id)sender;

- (IBAction)fbInvitationPressed:(id)sender;
- (IBAction)mailInvitationPressed:(id)sender;

- (IBAction)retourPressed:(id)sender;

// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

@end

@protocol CorrectionViewControllerDelegate
-(void)disconnectCorrectionReglagesPressed;
-(void)retourCorrectionPressed;
-(void)rejouerPressed:(BOOL)defiEnable;
-(void)nouvelleLettreCorrection:(NSString*)lettre isDefi:(BOOL)defiEnable;
-(void)nouvelleLettreDuelCorrection:(duel *)duel;
-(void)rejouerDuelCorrection:(duel *)duel;
@required
@end