//
//  DetailCorrectionCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiUtils.h"
#import "resultat.h"
#import "CustomAlert.h"

@interface DetailCorrectionCell : UITableViewCell <UIAlertViewDelegate> {
    ApiUtils *utils;
    CustomAlert *alert;
    
}
@property (strong, nonatomic) IBOutlet UILabel *reponseJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *reponseJoueurUn;
@property (strong, nonatomic) IBOutlet UILabel *categorieLabel;
@property (strong, nonatomic) IBOutlet UIImageView *scoreJoueurUn;
@property (strong, nonatomic) IBOutlet UIImageView *scoreJoueurDeux;
@property (strong, nonatomic) IBOutlet UILabel *scoreUnLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreDeuxLabel;
- (IBAction)submitWord:(id)sender;

@end
