//
//  BottomSoloCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 28/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomSoloCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imageJoueur;
@property (strong, nonatomic) IBOutlet UILabel *nomJoueur;
@property (strong, nonatomic) IBOutlet UILabel *recordJoueur;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *suggestionText;

@end
