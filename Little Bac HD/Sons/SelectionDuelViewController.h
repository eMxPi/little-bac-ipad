//
//  SelectionDuelViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 18/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReglagesView.h"
#import "duel.h"
#import "DuelCell.h"
#import "AmiCell.h"
#import "DernierCell.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "DEFacebookComposeViewController.h"
#import "ApiUtils.h"
#import "AppDelegate.h"
#import "EmptyTextCell.h"
#import "EmptyDuelCell.h"
#import "CorrectionViewController.h"
#import "FBSBJSON.h"
#import "StatistiquesRequest.h"
#import "EmptyAdversaireCell.h"
#import "ConnexionViewController.h"

@protocol SelectionDuelViewControllerDelegate;
@interface SelectionDuelViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, UITextFieldDelegate, CorrectionViewControllerDelegate, FBDialogDelegate, UIAlertViewDelegate, ConnexionViewControllerDelegate> {
    
    id<SelectionDuelViewControllerDelegate>__unsafe_unretained delegate;
    IBOutlet UILabel *duelEnCoursLabel;
    IBOutlet UILabel *invitationLabel;
    IBOutlet UITableView *duelEnCoursTableView;
    IBOutlet UITableView *invationTableView;
    IBOutlet UILabel *amiListTitre;
    IBOutlet UIImageView *fbListPointer;
    IBOutlet UIImageView *randomListPointer;
    IBOutlet UITableView *amiListTableView;
    IBOutlet UILabel *invitationTitre;
    IBOutlet UILabel *derniersAdversaireTitre;
    IBOutlet UITableView *dernierAdversaireTableView;
    CustomAlert *alert;
    //Reglages
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    IBOutlet UILabel *deconnexionReglagesLabel;
    
    NSMutableArray *duelEnCoursDataSource;
    NSMutableArray *invitationDataSource;
    NSMutableArray *amiListDataSource;
    NSMutableArray *derniersAdversairesDataSource;
    
    ApiUtils *utils;
    NSMutableArray *utilisateurList;
    NSMutableArray *fbFriendsList;
    BOOL isFacebook;
    IBOutlet UITextField *searchFriendField;
    NSString *previousSearch;
    IBOutlet UIView *confirmationDuel;
    IBOutlet UIView *confirmationDetailDuel;
    IBOutlet UIView *confirmationInvitationDuel;
    IBOutlet UILabel *amiLabel;
    IBOutlet UILabel *detailText;
    IBOutlet UIImageView *amiImage;
    duel *duelSelected;
    IBOutlet UILabel *refuserLabel;
    IBOutlet UILabel *accepterLabel;
    IBOutlet UILabel *confirmationInvitationLabel;
    IBOutlet UILabel *confirmationAmiLabel;
    IBOutlet UIImageView *confirmationAmiImage;
    IBOutlet UIButton *closeButton;
    
    CorrectionViewController *correctionView;
    ConnexionViewController *connexionView;
    utilisateur *currentUtil;
    IBOutlet UIView *confirmationShuffleView;
    IBOutlet UILabel *shuffleTitle;
    IBOutlet UILabel *shuffleUser;
    IBOutlet UIImageView *shuffleUserImage;
    IBOutlet UILabel *shuffleRefuserLabel;
    IBOutlet UILabel *shuffleAccepterLabel;
    BOOL loaded;
    BOOL isLoadingFriend;
}

@property (nonatomic, unsafe_unretained) id<SelectionDuelViewControllerDelegate>delegate;
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property (strong, nonatomic) duel *duelCorrect;

- (IBAction)fbInvitationPressed:(id)sender;
- (IBAction)mailInvitationPressed:(id)sender;

- (IBAction)fbListAmiPressed:(id)sender;
- (IBAction)randomListAmiPressed:(id)sender;
- (IBAction)retourPressed:(id)sender;
- (IBAction)shuffleListAmi:(id)sender;

- (IBAction)acceptShuffle:(id)sender;
- (IBAction)refuseShuffle:(id)sender;
// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

//Defi
- (IBAction)acceptDefi:(id)sender;
- (IBAction)refuseDefi:(id)sender;
- (IBAction)closeDefi:(id)sender;
- (IBAction)sendNotification:(id)sender;
- (IBAction)suppressionCorrection:(id)sender;

@end

@protocol SelectionDuelViewControllerDelegate
-(void)duelSelected:(duel *)duel;
-(void)retourSelectionDuelPressed;
@required
@end