//
//  CorrectionViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 21/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "CorrectionViewController.h"

@interface CorrectionViewController ()

@end

@implementation CorrectionViewController
@synthesize delegate, imageName, isDefi, currentDuel, correctionDataSource;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    [correctionTable setDelegate:self];
    [correctionTable setDataSource:self];
    
    [Flurry logAllPageViews:self];
    NSString *message = @"Correction Solo Affiche";
    if (isDefi) {
        message = @"Correction Duel Affiche";
    }
    [Flurry logEvent:message];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:message];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated {
    scoreSolo = 0;
    scoreDuo = 0;
    totalUn = 0;
    totalDeux = 0;
    [self loadProfile];
    if (!isDefi) {
        [fondFeuille setImage:[UIImage imageNamed:@"feuille_solo.png"]];
    }
    if (isDefi) {
        [self computeScoresDuel];
    } else {
        [self computeScores];
    }
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    
    NSString *scoreSoloStr = [NSString stringWithFormat:@"%i", scoreSolo];
    if (!isDefi) {
        [utils addProgression:scoreSoloStr scoreDuo:@""];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[defaults stringForKey:picProfil]]];
        if ( data == nil ) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(50.0f, 50.0f)];
            imageJoueurUn = newImage;
        });
    });
    
    if (isDefi) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:currentDuel.imageJoueurDeux]];
            if ( data == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(50.0f, 50.0f)];
                imageJoueurDeux = newImage;
                [correctionTable reloadData];
            });
        });
    }
    [self makeRecordSolo];
}


- (IBAction)retourPressed:(id)sender {
    [delegate retourCorrectionPressed];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isDefi) {
        if (indexPath.row == 0) {
            return 185;
        } else if (indexPath.row < [correctionDataSource count] - 1){
            return 100;
        } else {
            return 350;
        }
    } else {
        if (indexPath.row < [correctionDataSource count] - 1){
            return 100;
        }  else if (indexPath.row == [correctionDataSource count] - 1){
            return 270;
        } else {
            return 10;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return correctionDataSource.count > 0 ? correctionDataSource.count : 1;
    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isDefi) {
        if (indexPath.row == 0) {
            NSString *cellIdentifier = @"enTeteCell";
            EnTeteCorrectionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EnTeteCorrectionCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EnTeteCorrectionCell class]])
                    {
                        cell = (EnTeteCorrectionCell *)currentObject;
                        break;
                    }
                }
            }
            [cell.imageJoueurUn setImage:imageJoueurUn];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [cell.nomJoueurUn setText:[NSString stringWithFormat:@"%@", [defaults stringForKey:nameProfil]]];
            [cell.imageLettre setImage:[UIImage imageNamed:imageName]];
            [cell.nomJoueurUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            [cell.scoreJoueurUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.nomJoueurUn.adjustsFontSizeToFitWidth = YES;
            cell.scoreJoueurUn.adjustsFontSizeToFitWidth = YES;
            [cell.nomJoueurDeux setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            [cell.scoreJoueurDeux setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            cell.nomJoueurDeux.adjustsFontSizeToFitWidth = YES;
            cell.scoreJoueurDeux.adjustsFontSizeToFitWidth = YES;
            [cell.nomJoueurDeux setHidden:!isDefi];
            [cell.scoreJoueurDeux setHidden:!isDefi];
            [cell.imageJoueurDeux setHidden:!isDefi];
            if (isDefi) {
                [cell.nomJoueurDeux setText:[NSString stringWithFormat:@"%@", currentDuel.joueurDeux]];
                [cell.imageJoueurDeux setImage:imageJoueurDeux];
            }
            [cell.scoreJoueurUn setHidden:TRUE];
            [cell.scoreJoueurDeux setHidden:TRUE];
            
            return  cell;
        } else if (indexPath.row < [correctionDataSource count]-1) {
            
            NSString *cellIdentifier = @"detailCell";
            DetailCorrectionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DetailCorrectionCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[DetailCorrectionCell class]])
                    {
                        cell = (DetailCorrectionCell *)currentObject;
                        break;
                    }
                }
            }
            reponse *current = [correctionDataSource objectAtIndex:indexPath.row];
            [cell.categorieLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.scoreUnLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.scoreDeuxLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.reponseJoueurUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
            [cell.reponseJoueurDeux setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
            [cell.categorieLabel setText:current.categorie];
            
            if ([current.reponseJoueurUn isEqualToString:@"xxBonusAntiSechexx"]) {
                [cell.reponseJoueurUn setText:@"Bonus utilisé"];
            } else {
                [cell.reponseJoueurUn setText:current.reponseJoueurUn];
            }
            if ([current.reponseJoueurDeux isEqualToString:@"xxBonusAntiSechexx"]) {
                [cell.reponseJoueurDeux setText:@"Bonus utilisé"];
            } else {
                [cell.reponseJoueurDeux setText:current.reponseJoueurDeux];
            }
            [cell.scoreJoueurUn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", [self parseScore:current.scoreJoueurUn]]]];
            [cell.scoreJoueurDeux setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", [self parseScore:current.scoreJoueurDeux]]]];
            [cell.scoreUnLabel setText:current.scoreJoueurUn];
            [cell.scoreDeuxLabel setText:current.scoreJoueurDeux];
            cell.reponseJoueurUn.adjustsFontSizeToFitWidth = YES;
            cell.reponseJoueurDeux.adjustsFontSizeToFitWidth = YES;
            return cell;
        } else if (indexPath.row == [correctionDataSource count]-1) {
            NSString *cellIdentifier = @"bottomCell";
            BottomCorrectionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BottomCorrectionCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[BottomCorrectionCell class]])
                    {
                        cell = (BottomCorrectionCell *)currentObject;
                        break;
                    }
                }
            }
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [cell.imageJoueurUn setImage:imageJoueurUn];
            [cell.nomJoueurUn setText:[NSString stringWithFormat:@"%@", [defaults stringForKey:nameProfil]]];
            [cell.nomJoueurDeux setHidden:!isDefi];
            [cell.avatarJoueurDeux setHidden:!isDefi];
            [cell.imageJoueurDeux setHidden:!isDefi];
            [cell.tamponJoueurDeux setHidden:!isDefi];
            if (isDefi) {
                [cell.nomJoueurDeux setText:[NSString stringWithFormat:@"%@", currentDuel.joueurDeux]];
                [cell.imageJoueurDeux setImage:imageJoueurDeux];
            }
            if (totalDeux > totalUn) {
                [cell.tamponJoueurDeux setImage:[UIImage imageNamed:@"tampon_score.png"]];
                [cell.scoreJoueurDeux setTextColor:[UIColor colorWithRed:168.0f/255.0f green:29.0f/255.0f blue:29.0f/255.0f alpha:1.0f]];
                [cell.tamponJoueurUn setImage:[UIImage imageNamed:@"tampon_defaite.png"]];
                [cell.scoreJoueurUn setTextColor:[UIColor colorWithRed:85.0f/255.0f green:85.0f/255.0f blue:85.0f/255.0f alpha:1.0f]];
                [cell.avatarJoueurDeux setHidden:FALSE];
                [cell.avatarJoueurUn setHidden:TRUE];
                [cell.resultImage setImage:[UIImage imageNamed:@"duel_correction_perdu.png"]];
            } else if (totalDeux == totalUn) {
                [cell.tamponJoueurDeux setImage:[UIImage imageNamed:@"tampon_score.png"]];
                [cell.scoreJoueurDeux setTextColor:[UIColor colorWithRed:168.0f/255.0f green:29.0f/255.0f blue:29.0f/255.0f alpha:1.0f]];
                [cell.avatarJoueurDeux setHidden:FALSE];
                [cell.resultImage setImage:[UIImage imageNamed:@"duel_correction_nul.png"]];
            }
            [cell.scoreJoueurUn setText:[NSString stringWithFormat:@"%i", totalUn]];
            [cell.scoreJoueurDeux setText:[NSString stringWithFormat:@"%i", totalDeux]];
            [cell.scoreJoueurUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.scoreJoueurDeux setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.nomJoueurUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.nomJoueurDeux setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.suggestionText setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
            return cell;
        } else {
            NSString *cellIdentifier = @"emptyCell";
            EmptyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyCell class]])
                    {
                        cell = (EmptyCell *)currentObject;
                        break;
                    }
                }
            }
            return cell;
        }
    } else {
        if (indexPath.row < [correctionDataSource count]-1) {
            NSString *cellIdentifier = @"soloCell";
            SoloCorrectionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SoloCorrectionCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[SoloCorrectionCell class]])
                    {
                        cell = (SoloCorrectionCell *)currentObject;
                        break;
                    }
                }
            }
            reponse *current = [correctionDataSource objectAtIndex:indexPath.row];
            [cell.categorieLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.scoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.reponseLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.categorieLabel setText:current.categorie];
            if ([current.reponseJoueurUn isEqualToString:@"xxBonusAntiSechexx"]) {
                [cell.reponseLabel setText:@"Bonus utilisé"];
            } else {
                [cell.reponseLabel setText:current.reponseJoueurUn];
            }
            [cell.scoreLabel setText:current.scoreJoueurUn];
            [cell.imageReponse setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@", [self parseScore:current.scoreJoueurUn]]]];
            cell.reponseLabel.adjustsFontSizeToFitWidth = YES;
            //cell.categorieLabel.adjustsFontSizeToFitWidth = YES;
            return cell;
            
        } else if (indexPath.row == [correctionDataSource count]-1) {
            NSString *cellIdentifier = @"bottomSoloCell";
            BottomSoloCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BottomSoloCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[BottomSoloCell class]])
                    {
                        cell = (BottomSoloCell *)currentObject;
                        break;
                    }
                }
            }
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [cell.imageJoueur setImage:imageJoueurUn];
            NSString *profile = [defaults stringForKey:nameProfil];
            if (profile == nil || [profile isEqualToString:@""] || [profile isEqualToString:@"(null)"] || [profile isEqualToString:@"null"]) {
                profile = @"Invité";
            }
            [cell.nomJoueur setText:[NSString stringWithFormat:@"%@", profile]];
            [cell.scoreLabel setText:[NSString stringWithFormat:@"%i", scoreSolo]];
            [cell.recordJoueur setText:[NSString stringWithFormat:@"Record : %i", scoreSolo]];
            [cell.nomJoueur setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.scoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
            [cell.recordJoueur setFont:[UIFont fontWithName:@"Kameron-Bold" size:22]];
            [cell.suggestionText setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
            cell.recordJoueur.adjustsFontSizeToFitWidth = YES;
            cell.nomJoueur.adjustsFontSizeToFitWidth = YES;
            return cell;
        } else {
            NSString *cellIdentifier = @"emptyCell";
            EmptyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyCell" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyCell class]])
                    {
                        cell = (EmptyCell *)currentObject;
                        break;
                    }
                }
            }
            return cell;
        }
    }
}

-(NSString *)parseScore:(NSString *)score {
    NSString *imageOut = @"score_nul.png";
    if ([score isEqualToString:@"+1"] || [score isEqualToString:@"+2"]) {
        imageOut = @"score_gain.png";
    } else if ([score isEqualToString:@"-1"]) {
        imageOut = @"score_defaite.png";
    }
    return imageOut;
}

-(void)computeScores {
    for (int i=0;i < [correctionDataSource count]-1;i++) {
        reponse *current = [correctionDataSource objectAtIndex:i];
        if (current && current.scoreJoueurUn) {
            scoreSolo += [current.scoreJoueurUn intValue];
        }
        if (current && current.scoreJoueurDeux) {
            scoreDuo += [current.scoreJoueurDeux intValue];
        }
    }
}

-(void)computeScoresDuel {
    for (int i=0;i < [correctionDataSource count]-1;i++) {
        reponse *current = [correctionDataSource objectAtIndex:i];
        if (current && current.scoreJoueurDeux) {
            totalUn += [current.scoreJoueurUn intValue];
            totalDeux += [current.scoreJoueurDeux intValue];
        }
    }
    if (isDefi) {
        if (totalUn > totalDeux) {
            [bonusLittlesImage setImage:[UIImage imageNamed:@"15_littles_corr.png"]];
        } else if (totalUn == totalDeux) {
            [bonusLittlesImage setImage:[UIImage imageNamed:@"10_littles_corr.png"]];
        } else {
            [bonusLittlesImage setImage:[UIImage imageNamed:@"5_littles.png"]];
        }
    }
}


- (IBAction)deconnexionPressedButton:(id)sender {
    [self hideReglages];
    [delegate disconnectCorrectionReglagesPressed];
}

// Invitations
- (IBAction)nouvelleLettrePressed:(id)sender {
    if (!isDefi) {
        NSString *lettre = [self generateImage:isDefi];
        [delegate nouvelleLettreCorrection:lettre isDefi:isDefi];
    } else {
        [delegate nouvelleLettreDuelCorrection:currentDuel];
    }
}

- (IBAction)rejouerPressed:(id)sender {
    if (!isDefi) {
        [delegate rejouerPressed:isDefi];
    } else {
        [delegate rejouerDuelCorrection:currentDuel];
    }
}

-(NSString *)generateImage:(BOOL)Defi {
    int random = [self getRandomNumber:1 to:25];
    NSString *lettres = @"ABCDEFGHIJKLMOPNQRSTUVWXYZ";
    int prev = 0;
    if (isDefi) {
        prev = [lettres rangeOfString:currentDuel.lettre].location;
    } else {
        prev= [lettres rangeOfString:_lettre].location;
    }
    if (prev == random) {
        random ++;
    }
    NSString *lettre = [lettres substringWithRange:NSMakeRange(random, 1)];
    return lettre;
}

-(int)getRandomNumber:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}


- (IBAction)fbInvitationPressed:(id)sender {
    NSString *message = @"";
    if (isDefi) {
        if (totalUn > totalDeux) {
            message = [NSString stringWithFormat:@"Je viens de reporter un duel face à %@ dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", currentDuel.joueurDeux];
        } else if (totalUn == totalDeux) {
            message = [NSString stringWithFormat:@"Je viens de faire un match nul face à %@ dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", currentDuel.joueurDeux];
        } else {
            message = [NSString stringWithFormat:@"Je viens de perdre un duel face à %@ dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", currentDuel.joueurDeux];
        }
    } else {
        message = [NSString stringWithFormat:@"Je viens de réaliser un score de %i dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", scoreSolo];
    }
    [self sendFBWithText:message andImage:[UIImage imageNamed:@"icone.png"] showSheet:YES];
}

- (IBAction)mailInvitationPressed:(id)sender {
    NSString *message = @"";
    if (isDefi) {
        if (totalUn > totalDeux) {
            message = [NSString stringWithFormat:@"Je viens de reporter un duel face à %@ dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", currentDuel.joueurDeux];
        } else if (totalUn == totalDeux) {
            message = [NSString stringWithFormat:@"Je viens de faire un match nul face à %@ dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", currentDuel.joueurDeux];
        } else {
            message = [NSString stringWithFormat:@"Je viens de perdre un duel face à %@ dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", currentDuel.joueurDeux];
        }
    } else {
        message = [NSString stringWithFormat:@"Je viens de réaliser un score de %i dans Little Bac. Tu penses pouvoir me battre : http://bit.ly/WQBorI", scoreSolo];
    }
    [self sendMailWithText:message];
}

-(void)sendFBWithText:(NSString *)text andImage:(UIImage *)image showSheet:(BOOL)sheet
{
    if (FBSession.activeSession.isOpen) {
        DEFacebookComposeViewControllerCompletionHandler completionHandler = ^(DEFacebookComposeViewControllerResult result) {
            switch (result) {
                case DEFacebookComposeViewControllerResultCancelled:
                    NSLog(@"Facebook Result: Cancelled");
                    break;
                case DEFacebookComposeViewControllerResultDone:
                    NSLog(@"Facebook Result: Sent");
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
        DEFacebookComposeViewController *facebookViewComposer = [[DEFacebookComposeViewController alloc] init];
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [facebookViewComposer setInitialText:text];
        [facebookViewComposer addImage:image];
        facebookViewComposer.completionHandler = completionHandler;
        [self presentViewController:facebookViewComposer animated:YES completion:^{
            [self addLittles];
        }];
    } else {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        if ([appDelegate openSessionWithAllowLoginUI:YES]) {
            [self sendFBWithText:text andImage:image showSheet:YES];
        }
    }
}
-(void)sendMailWithText:(NSString *)text {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Rejoins moi sur Little Bac"];
        NSArray *toRecipients = [NSArray arrayWithObjects:nil];
        [mailer setToRecipients:toRecipients];
        NSString *emailBody = text;
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentViewController:mailer animated:YES completion:nil];
    } else {
        CustomAlert *alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Votre téléphone ne supporte pas l'envoi de mail"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            [self addLittles];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)makeRecordSolo {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    progression *prog = [utils getProgression];
    recordJoueurUn = prog.meilleurScoreSolo;
}

-(void)makeRecordDuo:(NSString *)joueurDeux {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    progression *prog = [utils getProgressionForUser:joueurDeux];
    recordJoueurDeux = prog.meilleurScoreSolo;
}


-(void)addLittles {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    resultat *res2 = [utils addLittles:@"10"];
    if (res2 == nil){
        CustomAlert *alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Le service semble indisponible. Êtes-vous sûr d'être bien connecté à internet ?"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else if ([res2.message isEqualToString:@"KO"]) {
        CustomAlert *alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Enregistrement indisponible"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

// Reglages
-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}


-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}

@end
