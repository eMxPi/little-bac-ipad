//
//  DuelCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 19/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "DuelCell.h"

@implementation DuelCell
@synthesize amiImage, amiName, amiAction;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
