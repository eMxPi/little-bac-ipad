//
//  UIView+JTRemoveAnimated.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 05/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "UIView+JTRemoveAnimated.h"

@implementation UIView (JTRemoveAnimated)

// remove static analyser warnings
#ifndef __clang_analyzer__

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if ([animationID isEqualToString:@"fadeout"]) {
        // Restore the opacity
        CGFloat originalOpacity = [(__bridge_transfer NSNumber *)context floatValue];
        self.layer.opacity = originalOpacity;
        [self removeFromSuperview];
    }
}

- (void)removeFromSuperviewAnimated {
    [UIView beginAnimations:@"fadeout" context:(__bridge_retained void *)[NSNumber numberWithFloat:self.layer.opacity]];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationDelegate:self];
    self.layer.opacity = 0;
    [UIView commitAnimations];
}

#endif

@end
