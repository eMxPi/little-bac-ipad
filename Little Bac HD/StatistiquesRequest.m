//
//  StatistiquesRequest.m
//  Captain Price
//
//  Created by Maxime Pontoire on 15/12/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import "StatistiquesRequest.h"
#include <arpa/inet.h>

@implementation StatistiquesRequest


-(BOOL)writeMessage:(NSString *)fichier message:(NSString *)message {
    NSString *xml = [self createStatsRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiStatistiquesUrl]] fichier:fichier message:message];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    BOOL output = FALSE;
    if ([self isNetworkEnabled]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        for(int i = 0; i < [[apiParser items] count]; i++) {
            resultat *currentMarker = [[apiParser items] objectAtIndex:i];
            if ([currentMarker.message isEqualToString:@"OK"]) {
                output = TRUE;
            }
        }
    }
    return output;
}

-(NSString *)createJeuRequest:(NSURL *)url message:(NSString *)message {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    NSString *MyString;
	NSDate *now = [NSDate date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
	MyString = [dateFormatter stringFromDate:now];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&message=%@;%@", chaine, MyString, message] dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"requete = %@",[NSString stringWithFormat:@"utilisateur=%@&jeu=%@", [defaults stringForKey:nameProfil], jeu] );
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createStatsRequest:(NSURL *)url fichier:(NSString *)fichier message:(NSString *)message {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    NSString *MyString;
	NSDate *now = [NSDate date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
	MyString = [dateFormatter stringFromDate:now];
    [requete setHTTPBody:[[NSString stringWithFormat:@"fichier=%@&message=%@;%@;%@", fichier, MyString, chaine, message] dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"requete = %@",[NSString stringWithFormat:@"utilisateur=%@&jeu=%@", [defaults stringForKey:nameProfil], jeu] );
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}


- (BOOL)isNetworkEnabled
{
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(8080);
    address.sin_addr.s_addr = inet_addr(apiHost);
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    Reachability *reach2 = [Reachability reachabilityWithAddress:&address];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    NetworkStatus net2 = [reach2 currentReachabilityStatus];
    return (networkStatus != NotReachable && net2 != NotReachable);
}


@end
