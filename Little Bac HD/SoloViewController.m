//
//  SoloViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 05/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "SoloViewController.h"

@interface SoloViewController ()

@end

@implementation SoloViewController
@synthesize delegate, dataSource = _dataSource, imageLettre, audioPlayer, isDefi, currentDuel, imageName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [categorieTableView setDelegate:self];
    [categorieTableView setDataSource:self];
    [categorieTableView reloadData];
    
    [titleText setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [detailText setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    
    
    [correctionDefiTitle setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [correctionDefiAmi setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [correctionDefiAmi setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [correctionDefiDetail setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [correctionDefiMessage setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    
    [finiLabel setFont:[UIFont fontWithName:@"Sansita One" size:15]];
    [reponseField setFont:[UIFont fontWithName:@"Sansita One" size:45]];
    [tempsLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [plusUnLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [suivantLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [nomDuelLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [duelTitreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    nomDuelLabel.adjustsFontSizeToFitWidth = YES;
    tempsLabel.adjustsFontSizeToFitWidth = YES;
    plusUnLabel.adjustsFontSizeToFitWidth = YES;
    suivantLabel.adjustsFontSizeToFitWidth = YES;
    
    //Audio
    
    NSURL *audioFileLocationURL = [[NSBundle mainBundle] URLForResource:@"JeuSon" withExtension:@"mp3"];
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileLocationURL error:&error];
    [audioPlayer setNumberOfLoops:-1];
    if (!error) {
        //Make sure the system follows our playback status
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        [[AVAudioSession sharedInstance] setActive: YES error: nil];
        //Load the audio into memory
        [audioPlayer prepareToPlay];
    }
    [reponseField setDelegate:self];
    
    
    [categoriePicker setDelegate:self];
    [categoriePicker setDataSource:self];
    
    /*[[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(keyboardWillHideHandler:)
     name:UIKeyboardWillHideNotification
     object:nil];*/
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nom"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _dataSource = [NSMutableArray arrayWithArray:[_dataSource sortedArrayUsingDescriptors:sortDescriptors]];
    
    [self populateTabs];
    
    
    cateView = [[AFPickerView alloc] initWithFrame:CGRectMake(717.0, 173.0, 276.0, 201.0)];
    cateView.dataSource = self;
    cateView.delegate = self;
    [cateView reloadData];
    [self.view addSubview:cateView];
    
    audioController = [[AudioController alloc] init];
    
    [Flurry logAllPageViews:self];
    NSString *message = @"Jeu Solo Affiche";
    if (isDefi) {
        message = @"Jeu Duel Affiche";
    }
    [Flurry logEvent:message];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:message];
}


#pragma mark - AFPickerViewDataSource

- (NSInteger)numberOfRowsInPickerView:(AFPickerView *)pickerView
{
    return [_dataSource count];
}




- (NSString *)pickerView:(AFPickerView *)pickerView titleForRow:(NSInteger)row
{
    categorie *cat = [_dataSource objectAtIndex:row];
    return cat.nom;
}




#pragma mark - AFPickerViewDelegate

- (void)pickerView:(AFPickerView *)pickerView didSelectRow:(NSInteger)row
{
    categorie *cat = [_dataSource objectAtIndex:row];
    if (row != selIndex) {
        selIndex = row;
        [reponseField setPlaceholder:[NSString stringWithFormat:@"%@",cat.nom]];
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_dataSource count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    categorie *cat = [_dataSource objectAtIndex:row];
    return cat.nom;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    categorie *cat = [_dataSource objectAtIndex:row];
    if (row != selIndex) {
        selIndex = row;
        [reponseField setPlaceholder:[NSString stringWithFormat:@"%@",cat.nom]];
        NSString *lettre = [NSString stringWithFormat:@"%c",[imageName characterAtIndex:0]];
        [reponseField setText:lettre];
    }
    
}

/*- (void) keyboardWillHideHandler:(NSNotification *)notification {
 [reponseField endEditing:NO];
 [reponseField becomeFirstResponder];
 }*/

-(void)viewWillAppear:(BOOL)animated {
    [self loadProfile];
    
    // Récupération des données variables
    if (isDefi) {
        [retourButton setHidden:TRUE];
        [self loadDataFromApiDefi:currentDuel.categories];
    } else {
        [retourButton setHidden:FALSE];
        [self loadDataFromApi];
    }
    [cateView reloadData];
    [self populateTabs];
    
    // Ajout des données fixes
    // TODO
    
    [reponseField becomeFirstResponder];
    secondes = 102;
    sizeToMove = timerFond.frame.size.width / secondes;
    [self startTimer];
    [self computeBonusRestant];
    [self loadDuel];
    reponsesTaped = 0;
    mots = @"";
    isAction = FALSE;
    categorie *cat = [_dataSource objectAtIndex:0];
    [reponseField setPlaceholder:[NSString stringWithFormat:@"%@",cat.nom]];
    // Temps Bonus
    secondesBonus = 10;
}


-(int)checkValue:(NSString *)value {
    if (value && ![value isEqualToString:@""] && ![value isEqualToString:@"(null)"]) {
        return [value intValue];
    } else {
        return 0;
    }
}

-(void)loadDuel {
    if (isDefi) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:currentDuel.imageJoueurDeux]];
            if ( data == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(50.0f, 50.0f)];
                [imageProfil setImage:newImage];
            });
        });
        [duelTitreLabel setText:@"Duel avec"];
        [nomDuelLabel setText:[NSString stringWithFormat:@"%@", currentDuel.joueurDeux]];
    } else {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[defaults stringForKey:picProfil]]];
            if ( data == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(50.0f, 50.0f)];
                [imageProfil setImage:newImage];
            });
        });
        [duelTitreLabel setText:@"Mode solo"];
        NSString *profileName = [defaults stringForKey:nameProfil];
        if (profileName && ![profileName isEqualToString:@""]) {
            [nomDuelLabel setText:[NSString stringWithFormat:@"%@", [defaults stringForKey:nameProfil]]];
        } else {
            [nomDuelLabel setText:[NSString stringWithFormat:@"Invité"]];
        }
    }
}

-(void)computeTempsBonus {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [tempsLabel setText:[NSString stringWithFormat:@"x%@", [utils getTempsRestant]]];
    } else {
        int temps = [[defaults stringForKey:tempsBonusCurrent] intValue] - 1;
        [defaults setValue:[NSString stringWithFormat:@"%i", temps] forKey:tempsBonusCurrent];
        [tempsLabel setText:[NSString stringWithFormat:@"x%i", temps]];
    }
    [self updateBonus];
}

-(void)computeCorrectionBonus {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [plusUnLabel setText:[NSString stringWithFormat:@"x%@", [utils getCorrectionRestant]]];
    } else {
        int temps = [[defaults stringForKey:checkBonusCurrent] intValue] - 1;
        [defaults setValue:[NSString stringWithFormat:@"%i", temps] forKey:checkBonusCurrent];
        [plusUnLabel setText:[NSString stringWithFormat:@"x%i", temps]];
    }
    [self updateBonus];
}

-(void)computeAntiSeche {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [suivantLabel setText:[NSString stringWithFormat:@"x%@", [utils getAntiSecheRestant]]];
    } else {
        int temps = [[defaults stringForKey:nextBonusCurrent] intValue] - 1;
        [defaults setValue:[NSString stringWithFormat:@"%i", temps] forKey:nextBonusCurrent];
        [suivantLabel setText:[NSString stringWithFormat:@"x%i", temps]];
    }
    [self updateBonus];
}

-(void)computeBonusRestant {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [plusUnLabel setText:[NSString stringWithFormat:@"x%@", [utils getCorrectionRestant]]];
        [tempsLabel setText:[NSString stringWithFormat:@"x%@", [utils getTempsRestant]]];
        [suivantLabel setText:[NSString stringWithFormat:@"x%@", [utils getAntiSecheRestant]]];
    } else {
        [suivantLabel setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:nextBonusCurrent]]]];
        [tempsLabel setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:tempsBonusCurrent]]]];
        [plusUnLabel setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:checkBonusCurrent]]]];
    }
    [self updateBonus];
}

-(void)updateBonus {
    int counterVal = [[[plusUnLabel text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (counterVal <= 0) {
        [plusUnImage setImage:[UIImage imageNamed:@"bonus_correction_plein.png"]];
    } else {
        [plusUnImage setImage:[UIImage imageNamed:@"bonus_correction_uo.png"]];
    }
    int antiVal = [[[suivantLabel text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (antiVal <= 0) {
        [suivantImage setImage:[UIImage imageNamed:@"mot.png"]];
    } else {
        [suivantImage setImage:[UIImage imageNamed:@"bonus_mot_up.png"]];
    }
    int tempsVal = [[[tempsLabel text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (tempsVal <= 0) {
        [tempsImage setImage:[UIImage imageNamed:@"pause.png"]];
    } else {
        [tempsImage setImage:[UIImage imageNamed:@"bonus_pause_up.png"]];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)playAudio {
    //Play the audio and set the button to represent the audio is playing
    [audioPlayer play];
}

- (void)startTimer {
    
    // Lance le minuteur
    BOOL started = [self beginTimer:secondes];
    if (!started ) {
        [self stopTimer];
    }
}

- (BOOL)beginTimer:(int)numberOfSeconds {
    BOOL started = NO; // Le chronomètre n'est pas lancé
    remainingSeconds = numberOfSeconds;
    
    if ( remainingSeconds > 0 ) {
        
        // Crée le chronomètre avec une méthode de construction
        // Chaque seconde le message updateTimer: sera reçu par l'objet
        bonusTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTimer:)
                                                    userInfo:nil
                                                     repeats:YES];
        started = YES; // Le chronomètre est lancé
    }
    if (started) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL isSonOn = [defaults boolForKey:isMusiqueActive];
        if (isSonOn) {
            [self playAudio];
        }
    }
    
    return started;
}

- (void)stopTimer {
    // Arrête le minuteur
    [self endTimer];
    [audioPlayer pause];
    
}

- (void) endTimer {
    if ( nil != bonusTimer ) {
        [bonusTimer invalidate];
        bonusTimer = nil;
    }
}

- (void) updateTimer: (NSTimer *) aTimer
{
    --remainingSeconds;
    [timerImage setCenter:CGPointMake(timerImage.center.x + sizeToMove, timerImage.center.y)];
    
    // Arrète le NSTimer lorsque le compte à rebour est à zéro
    if ( remainingSeconds <= 0 ) {
        // PLay SOUND
        tempEcoule = TRUE;
        [self stopTimer];
        if (isDefi) {
            [finiLabel setText:@"Défi terminé ! Bien joué."];
            [detailText setText:@"Vas vite voir les résultats."];
        } else {
            [finiLabel setText:@"L'épreuve est terminée ! Bon travail."];
            [detailText setText:@"Il est temps de voir tes notes."];
        }
        [self displayCorrectionAlert];
    }
}



- (void)startTimerBonus {
    
    // Lance le minuteur
    BOOL started = [self beginTimerBonus:secondesBonus];
    if (!started ) {
        [self stopTimerBonus];
    }
}

- (BOOL)beginTimerBonus:(int)numberOfSeconds {
    BOOL started = NO; // Le chronomètre n'est pas lancé
    remainingSecondsBonus = numberOfSeconds;
    
    if ( remainingSecondsBonus > 0 ) {
        
        // Crée le chronomètre avec une méthode de construction
        // Chaque seconde le message updateTimer: sera reçu par l'objet
        bonusTimerTemps = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                           target:self
                                                         selector:@selector(updateTimerBonus:)
                                                         userInfo:nil
                                                          repeats:YES];
        started = YES; // Le chronomètre est lancé
    }
    if (started) {
        [self stopTimer];
    }
    
    return started;
}

- (void)stopTimerBonus {
    // Arrête le minuteur
    [self endTimerBonus];
    [self beginTimer:remainingSeconds];
    
}

- (void) endTimerBonus {
    if ( nil != bonusTimerTemps ) {
        [bonusTimerTemps invalidate];
        bonusTimerTemps = nil;
    }
}

- (void) updateTimerBonus: (NSTimer *) aTimer
{
    --remainingSecondsBonus;
    
    // Arrète le NSTimer lorsque le compte à rebour est à zéro
    if ( remainingSecondsBonus <= 0 ) {
        // PLay SOUND
        tempEcouleBonus = TRUE;
        [self stopTimerBonus];
    }
}



- (IBAction)retourPressed:(id)sender {
    isAction = TRUE;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:isGameRunning];
    [reponseField resignFirstResponder];
    [self stopTimer];
    [delegate retourSoloPressed];
}

- (IBAction)tempsPressed:(id)sender {
    if (![[utils getTempsRestant] isEqualToString:@"0"]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL isSonOn = [defaults boolForKey:isEffetActive];
        if (isSonOn) {
            [audioController playSound:@"UtilisationBonusJeu" extension:@"mp3"];
        }
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils addTemps:@"-1"];
        }
        // TODO : stopper counter
        [self computeTempsBonus];
        [self startTimerBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Plus de bonus !" message:@"Rends toi sur le store pour convertir tes Littles en bonus." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

- (IBAction)plusUnPressed:(id)sender {
    if (![[utils getCorrectionRestant] isEqualToString:@"0"]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL isSonOn = [defaults boolForKey:isEffetActive];
        if (isSonOn) {
            [audioController playSound:@"UtilisationBonusJeu" extension:@"mp3"];
        }
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils addCorrection:@"-1"];
        }
        // TODO : stopper counter
        [self computeCorrectionBonus];
        [self validateMot];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Plus de bonus !" message:@"Rends toi sur le store pour convertir tes Littles en bonus." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

-(void)validateMot {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    categorie *cat = [_dataSource objectAtIndex:selIndex];
    
    resultat *res = [utils validateMot:reponseField.text idCategorie:cat.id];
    if ([res.message isEqualToString:@"OK"]) {
        categorie *cat = [_dataSource objectAtIndex:selIndex];
        for (int i=0;i<[categoriesReponse count];i++) {
            NSString *currentId = [categoriesReponse objectAtIndex:i];
            if (currentId == cat.id) {
                [motsReponse replaceObjectAtIndex:i withObject:reponseField.text];
                break;
            }
        }
        cat.nom = [NSString stringWithFormat:@"%@ ✔",cat.nom];
        [_dataSource replaceObjectAtIndex:selIndex withObject:cat];
        [categoriePicker reloadAllComponents];
        [cateView reloadData];
        [categoriePicker reloadAllComponents];
        [cateView reloadData];
        selIndex++;
        [reponseField setText:@""];
        [cateView setSelectedRow:selIndex];
        [self displayNextCat];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL isSonOn = [defaults boolForKey:isEffetActive];
        if (isSonOn) {
            [audioController playSound:@"Validation Mots pendant le jeu" extension:@"mp3"];
        }
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Mauvaise Réponse !" message:@"Change ta réponse pour marquer des points."
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
        [reponseField setText:@""];
    }
}

- (IBAction)suivantPressed:(id)sender {
    if (![[utils getAntiSecheRestant] isEqualToString:@"0"]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL isSonOn = [defaults boolForKey:isEffetActive];
        if (isSonOn) {
            [audioController playSound:@"UtilisationBonusJeu" extension:@"mp3"];
        }
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils addAntiSeche:@"-1"];
        }
        // TODO : stopper counter
        categorie *cat = [_dataSource objectAtIndex:selIndex];
        for (int i=0;i<[categoriesReponse count];i++) {
            NSString *currentId = [categoriesReponse objectAtIndex:i];
            if (currentId == cat.id) {
                [motsReponse replaceObjectAtIndex:i withObject:@"xxBonusAntiSechexx"];
                break;
            }
        }
        cat.nom = [NSString stringWithFormat:@"%@ ✔",cat.nom];
        [_dataSource replaceObjectAtIndex:selIndex withObject:cat];
        [categoriePicker reloadAllComponents];
        [cateView reloadData];
        [categoriePicker reloadAllComponents];
        [cateView reloadData];
        [self computeAntiSeche];
        selIndex++;
        [reponseField setText:@""];
        [cateView setSelectedRow:selIndex];
        
        reponsesTaped++;
        if (reponsesTaped == 10) {
            [self stopTimer];
            [self displayCorrectionAlert];
        }
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Plus de bonus !" message:@"Rends toi sur le store pour convertir tes Littles en bonus." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

- (IBAction)deconnexionPressedButton:(id)sender {
    isAction = TRUE;
    [self hideReglages];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [defaults setValue:@"0" forKey:tempsBonusCurrent];
    [defaults setValue:@"0" forKey:checkBonusCurrent];
    [defaults setValue:@"0" forKey:nextBonusCurrent];
    [self loadProfile];
    [self retourPressed:nil];
}

-(void)displayCorrectionAlert {
    isAction = TRUE;
    [self hideTextField:reponseField];
    [cateView setHidden:TRUE];
    if (!isDefi) {
        [correctionAlertSoloView setHidden:FALSE];
        [correctionAlertDefiView setHidden:TRUE];
    } else {
        resultat *res = [utils finirDuel:currentDuel.id];
        if (res && ![res.message isEqualToString:@"KO"]) {
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                resultat *result = [utils sendPushFinished:currentDuel.id];
                if ( result == nil ) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (![result.message isEqualToString:@"OK"]) {
                        alert = [[CustomAlert alloc] initWithTitle:@"Erreur " message:@"Une erreur est survenue lors de l'envoi de la notification à ton adversaire. Pas de panique, il a bien reçu le duel."
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil,nil];
                        [alert show];
                        alert = nil;
                    }
                });
            });
        }
        [correctionAlertSoloView setHidden:TRUE];
        [correctionAlertDefiView setHidden:FALSE];
        [correctionDefiAmi setText:currentDuel.joueurDeux];
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:currentDuel.imageJoueurDeux]];
            if ( data == nil ) {
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(67.0f, 67.0f)];
                [correctionDefiAmiImage setImage:newImage];
            });
        });
        if (isDefi) {
            resultat *res = [utils isFinished:currentDuel.id];
            if ([res.message isEqualToString:@"OK"]) {
                [correctionDefiDetail setText:@"C'est aussi fini pour"];
                [correctionDefiMessage setText:@"Fonce voir tes résultats dans le menu Duel"];
                [utils vuCorr:currentDuel.id];
            }
        }
    }
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [correctionAlertView setHidden:FALSE];
}

- (IBAction)correctionPressed:(id)sender {
    [self sendReponse];
}

- (IBAction)correctionDefiPressed:(id)sender {
    [self sendReponse];
}


-(void)loadDataFromApi {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/random", apiCategorieUrl]];
    if (![self isNetworkEnabled]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Cours de techno !" message:@"Tu ne captes pas ? Trouves du réseau et reviens au plus vite en salle de classe !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else if (![self isAPIReachable]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Prof absent !" message:@"Les cours ne sont plus assurés pour le moment ! Reviens nous voir après la récré !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
        
    } else {
        XMLToObjectParser *apiParser = [[XMLToObjectParser alloc]
                                        parseXMLAtURL:URL toObject:@"categorie" parseError:nil];
        _dataSource = [[NSMutableArray alloc] init];
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                categorie *currentMarker = [[apiParser items] objectAtIndex:i];
                [_dataSource addObject:currentMarker];
            }
        }
    }
}

-(void)loadDataFromApiDefi:(NSString *)ids {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    _dataSource = [utils getOrdered:ids];
}


-(void)revealReglages {
    isAction = TRUE;
    [reponseField resignFirstResponder];
    [cateView setHidden:TRUE];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    isAction = FALSE;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
    
    [reponseField endEditing:YES];
    [reponseField resignFirstResponder];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
    [reponseField endEditing:NO];
    [reponseField becomeFirstResponder];
    [cateView setHidden:FALSE];
}


-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    if (!isMusique) {
        [audioPlayer pause];
    } else {
        [audioPlayer play];
    }
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}

-(void)sendReponse {
    NSString *lettre = [NSString stringWithFormat:@"%c",[imageName characterAtIndex:0]];
    [self computeReponseForRequest];
    // Envoi de la réponse
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [correctionAlertView setHidden:TRUE];
	NSMutableArray *corrections = nil;
	if (!isDefi) {
        corrections =  [self parseCorrections:[utils getCorrectionSolo:mots categories:categories lettre:lettre]];
        // Affichage de la correction
        if (!correctionView) {
            correctionView = [[CorrectionViewController alloc]initWithNibName:@"CorrectionViewController" bundle:nil];
            [correctionView setDelegate:self];
        }
        [correctionView.view setFrame:CGRectMake(0, 0, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
        correctionView.imageName = imageName;
        correctionView.isDefi = isDefi;
        correctionView.currentDuel = currentDuel;
        correctionView.lettre = lettre;
        reponse *rep = [[reponse alloc] init];
        [rep setCategorie:@"vide"];
        [corrections addObject:rep];
        correctionView.correctionDataSource = corrections;
        isAction = TRUE;
        [self hideTextField:reponseField];
        [self.view addSubview:correctionView.view];
        
	} else {
        resultat *res = [utils isFinished:currentDuel.id];
        [utils addReponses:currentDuel.id reponses:mots];
        if ([res.message isEqualToString:@"OK"]) {
            [delegate retourDuelPressed:currentDuel];
        } else {
            [self retourPressed:nil];
        }
	}
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:isGameRunning];
    [self stopTimer];
}


-(NSMutableArray *)parseCorrections:(NSMutableArray*)input {
    NSMutableArray *output = [[NSMutableArray alloc] init];
    if (isDefi) {
        reponse *rep = [[reponse alloc] init];
        [rep setCategorie:@"vide"];
        [output addObject:rep];
    }
    for (int i=0;i<[input count];i++) {
        [output addObject:[input objectAtIndex:i]];
    }
    if (isDefi) {
        reponse *rep2 = [[reponse alloc] init];
        [rep2 setCategorie:@"vide"];
        [output addObject:rep2];
    }
    return output;
}


- (void)hideTextField:(UITextField *)textField {
    [textField resignFirstResponder];
}

#pragma mark TextFieldDelegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [reponseField endEditing:NO];
    [self parseMots];
    categorie *cat = [_dataSource objectAtIndex:selIndex];
    if ([cat.nom rangeOfString:@"✔"].location == NSNotFound) {
        cat.nom = [NSString stringWithFormat:@"%@ ✔",cat.nom];
        [_dataSource replaceObjectAtIndex:selIndex withObject:cat];
        [categoriePicker reloadAllComponents];
        [cateView reloadData];
        [categoriePicker reloadAllComponents];
        [cateView reloadData];
    }
    selIndex++;
    if (selIndex == [_dataSource count]) {
        selIndex = 0;
    }
    [cateView setSelectedRow:selIndex];
    reponsesTaped++;
    [reponseField setText:@""];
    cat = [_dataSource objectAtIndex:selIndex];
    [reponseField setPlaceholder:[NSString stringWithFormat:@"%@",cat.nom]];
    [reponseField becomeFirstResponder];
    if (reponsesTaped == 10) {
        [self stopTimer];
        [self displayCorrectionAlert];
        return YES;
    } else {
        [self displayNextCat];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (!isAction) {
        [reponseField endEditing:NO];
        [reponseField becomeFirstResponder];
    }
}

-(void)displayNextCat {
    BOOL found = FALSE;
    for (int i=selIndex; i < [_dataSource count]; i++) {
        categorie *cat = [_dataSource objectAtIndex:i];
        if ([cat.nom rangeOfString:@"✔"].location == NSNotFound) {
            selIndex = i;
            [cateView setSelectedRow:selIndex];
            [reponseField setPlaceholder:[NSString stringWithFormat:@"%@",cat.nom]];
            [reponseField becomeFirstResponder];
            found  = TRUE;
            return ;
        }
    }
    if (!found) {
        for (int i=0; i < selIndex; i++) {
            categorie *cat = [_dataSource objectAtIndex:i];
            if ([cat.nom rangeOfString:@"✔"].location == NSNotFound) {
                selIndex = i;
                [cateView setSelectedRow:selIndex];
                [reponseField setPlaceholder:[NSString stringWithFormat:@"%@",cat.nom]];
                [reponseField becomeFirstResponder];
                found  = TRUE;
                return ;
            }
        }
    }
}

-(void)populateTabs {
    categoriesReponse = [[NSMutableArray alloc] init];
    motsReponse = [[NSMutableArray alloc] init];
    for (int i=0;i<[_dataSource count];i++) {
        categorie *cat = [_dataSource objectAtIndex:i];
        [categoriesReponse addObject:cat.id];
        [motsReponse addObject:@""];
    }
}

-(void)computeReponseForRequest {
    for (int i=0;i<[categoriesReponse count];i++) {
        NSString *mot = [motsReponse objectAtIndex:i];
        NSData *temp = [mot dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *motConverted = [[NSString alloc] initWithData:temp encoding:NSASCIIStringEncoding];
        if ([mot isEqualToString:@""]) {
            motConverted = @" ";
        }
        if (i==0) {
            categories = [categoriesReponse objectAtIndex:i];
            mots = motConverted;
        } else {
            categories = [NSString stringWithFormat:@"%@;%@", categories, [categoriesReponse objectAtIndex:i]];
            mots = [NSString stringWithFormat:@"%@;%@", mots, motConverted];
        }
    }
}

-(void)parseMots {
    categorie *cat = [_dataSource objectAtIndex:selIndex];
    for (int i=0;i<[categoriesReponse count];i++) {
        NSString *currentId = [categoriesReponse objectAtIndex:i];
        if (currentId == cat.id) {
            NSString *currentMot = [motsReponse objectAtIndex:i];
            categorie *catCurrent = [_dataSource objectAtIndex:selIndex];
            if (![currentMot isEqualToString:@""] || ([currentMot isEqualToString:@""] && [catCurrent.nom rangeOfString:@"✔"].location != NSNotFound)) {
                reponsesTaped--;
            }
            [motsReponse replaceObjectAtIndex:i withObject:reponseField.text];
            break;
        }
    }
}


#pragma mark Table view methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count > 0 ? _dataSource.count : 2;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"categorieCell";
    CategorieCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CategorieCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[CategorieCell class]])
            {
                cell = (CategorieCell *)currentObject;
                break;
            }
        }
    }
    
    [cell.categorieLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:23]];
    if (_dataSource != nil && [_dataSource count] > 0) {
        categorie *current = [_dataSource objectAtIndex:indexPath.row];
        [cell.categorieLabel setText:current.nom];
    } else {
        [cell.categorieLabel setText:@"Non connecte"];
        [cell.tamponImage setHidden:TRUE];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark CorrectionViewControllerDelegate
-(void)retourCorrectionPressed {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:isGameRunning];
    [delegate retourSoloPressed];
}

-(void)rejouerPressed:(BOOL)defiEnable {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:isGameRunning];
    [delegate rejouerPressed:defiEnable];
}

-(void)nouvelleLettreCorrection:(NSString *)lettre isDefi:(BOOL)defiEnable {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:isGameRunning];
    [delegate nouvelleLettrePressed:lettre isDefi:defiEnable];
}

-(void)nouvelleLettreDuelCorrection:(duel *)duel {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
}

-(void)rejouerDuelCorrection:(duel *)duel {
    if (correctionView) {
        [correctionView.view removeFromSuperview];
        correctionView = nil;
        [correctionView.view setFrame:CGRectMake(0, -1500, correctionView.view.frame.size.width, correctionView.view.frame.size.height)];
    }
}

-(void)disconnectCorrectionReglagesPressed {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"avatar_defaut.png" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [defaults setValue:@"0" forKey:tempsBonusCurrent];
    [defaults setValue:@"0" forKey:checkBonusCurrent];
    [defaults setValue:@"0" forKey:nextBonusCurrent];
    [defaults setBool:TRUE forKey:isNotifActive];
    [self loadProfile];
    [self retourPressed:nil];
}

- (BOOL)isNetworkEnabled
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}


-(BOOL)isAPIReachable {
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(8080);
    address.sin_addr.s_addr = inet_addr(apiHost);
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    Reachability *reach2 = [Reachability reachabilityWithAddress:&address];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    NetworkStatus net2 = [reach2 currentReachabilityStatus];
    return (networkStatus != NotReachable && net2 != NotReachable);
    
}
@end
