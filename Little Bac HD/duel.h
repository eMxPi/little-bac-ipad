//
//  duel.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 17/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface duel : NSObject {
    NSString *id;
    NSString *joueurUn;
    NSString *joueurDeux;
    NSString *imageJoueurUn;
    NSString *imageJoueurDeux;
    NSString *StatutJoueurUn;
    NSString *StatutJoueurDeux;
    NSString *categories;
    NSString *lettre;
    NSString *vuDuelUn;
    NSString *vuDuelDeux;
    NSString *vuCorrUn;
    NSString *vuCorrDeux;
}
@property (nonatomic, retain) NSString *id;
@property (nonatomic, retain) NSString *joueurUn;
@property (nonatomic, retain) NSString *joueurDeux;

@property (nonatomic, retain) NSString *imageJoueurUn;
@property (nonatomic, retain) NSString *imageJoueurDeux;

@property (nonatomic, retain) NSString *StatutJoueurUn;
@property (nonatomic, retain) NSString *StatutJoueurDeux;
@property (nonatomic, retain) NSString *categories;
@property (nonatomic, retain) NSString *lettre;
@property (nonatomic, retain) NSString *vuDuelUn;
@property (nonatomic, retain) NSString *vuDuelDeux;
@property (nonatomic, retain) NSString *vuCorrUn;
@property (nonatomic, retain) NSString *vuCorrDeux;

@end
