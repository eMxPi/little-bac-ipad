//
//  ViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 03/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    imageOptions.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *down = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealOptions)];
    down.delegate = self;
    [down setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageOptions addGestureRecognizer:down];
    
    imageOptionsDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *up = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideOptions)];
    up.delegate = self;
    [up setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageOptionsDeplie addGestureRecognizer:up];
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [notifLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [creditLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [howPlayLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [otherAppLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [notifDuelLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    audioController = [[AudioController alloc] init];
    [soloButton setImage:[UIImage imageNamed:@"accueil_solo_down.png"] forState:UIControlStateHighlighted];
    [profilButton setImage:[UIImage imageNamed:@"accueil_profil_down.png"] forState:UIControlStateHighlighted];
    [storeButton setImage:[UIImage imageNamed:@"accueil_store_down.png"] forState:UIControlStateHighlighted];
    [duelButton setImage:[UIImage imageNamed:@"accueil_duel_down.png"] forState:UIControlStateHighlighted];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveDuel) name:@"refreshView" object:nil];
    
    [appTableView setDelegate:self];
    [appTableView setDataSource:self];
    
    
    [creditTableView setDelegate:self];
    [creditTableView setDataSource:self];
    
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *isSavedToken = [defaults stringForKey:savedToken];
    if (isSavedToken == nil || [isSavedToken isEqualToString:@""] || ![isSavedToken isEqualToString:@"TRUE"]) {
        NSString *profil = [defaults stringForKey:nameProfil];
        if (profil == nil || [profil isEqualToString:@""] || [profil isEqualToString:@"(null)"]) {
            [self saveToken];
        }
        [defaults setValue:@"TRUE" forKey:savedToken];
    }
}

-(void)saveToken {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [utils saveTokenNotConnected:appDelegate.dToken];
}

-(void)loading {
    [self loadProfile];
    [self getDuels];
}

-(void)viewWillAppear:(BOOL)animated {
    // Loading
    [self loading];
}
-(void)getDuels {
    
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [notifDuelLabel setText:[NSString stringWithFormat:@"%i", [[utils getDuelsInvite] count]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *profil = [defaults stringForKey:nameProfil];
    if (profil != nil && ![profil isEqualToString:@""]) {
        isEffet = [defaults boolForKey:isEffetActive];
        isMusique = [defaults boolForKey:isMusiqueActive];
        isShare = [defaults boolForKey:isShareActive];
        isNotifications = [defaults boolForKey:isNotifActive];
    } else {
        isEffet = TRUE;
        isMusique = TRUE;
        isShare = TRUE;
        isNotifications = TRUE;
    }
    [self changeButtons];
}

-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isNotifications) {
        [notifButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [notifButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    
}


-(void)revealOptions {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showAnimation)];
    [optionsView setFrame:CGRectMake(20, 40, optionsView.frame.size.width, optionsView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideOptions {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideAnimation)];
    [optionsView setFrame:CGRectMake(20, -379, optionsView.frame.size.width, optionsView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showAnimation {
    [imageOptions setHidden:TRUE];
    [imageOptionsDeplie setHidden:FALSE];
}

-(void)hideAnimation {
    [imageOptions setHidden:FALSE];
    [imageOptionsDeplie setHidden:TRUE];
}

-(void)revealReglages {
    
    [Flurry logEvent:@"Reglages Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Reglages Accueil Affiche"];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [Flurry logEvent:@"Reglages Accueil Masque"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Reglages Accueil Masque"];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}

- (IBAction)activeNotif:(id)sender {
    isNotifications = !isNotifications;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isNotifications forKey:isNotifActive];
    [self changeButtons];
}



-(void)loadApp {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    appDataSource = [utils appList];
    app *empty = [[app alloc] init];
    [empty setNom:@""];
    [appDataSource addObject:empty];
    [appTableView reloadData];
}

-(void)loadTuto {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    creditDataSource = [utils creditList];
    [creditTableView reloadData];
}

- (IBAction)creditsPressed:(id)sender {
    [Flurry logEvent:@"Credit Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Credit Accueil Affiche"];
    
    [creditView setHidden:FALSE];
    [creditText setHidden:FALSE];
    [appTableView setHidden:TRUE];
    [creditTableView setHidden:TRUE];
}

- (IBAction)howToPlayPressed:(id)sender {
    [Flurry logEvent:@"Tuto Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Tuto Accueil Affiche"];
    if (creditDataSource == nil) {
        [self loadTuto];
    }
    
    [creditView setHidden:FALSE];
    [creditText setHidden:TRUE];
    [appTableView setHidden:TRUE];
    [creditTableView setHidden:FALSE];
}

- (IBAction)otherAppPressed:(id)sender {
    [Flurry logEvent:@"App Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"App Accueil Affiche"];
    if (appDataSource == nil) {
        [self loadApp];
    }
    [creditView setHidden:FALSE];
    [creditText setHidden:TRUE];
    [creditTableView setHidden:TRUE];
    [appTableView setHidden:FALSE];
}

- (IBAction)hideCredit:(id)sender {
    [creditView setHidden:TRUE];
}

-(void)displayAccueil:(BOOL)visibility {
    [optionsView setHidden:!visibility];
    [reglagesView setHidden:!visibility];
    [accueilView setHidden:!visibility];
    [accueilLogo setHidden:!visibility];
}

- (IBAction)profilPressed:(id)sender {
    [Flurry logEvent:@"Profil Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Profil Accueil Affiche"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isSonOn = [defaults boolForKey:isEffetActive];
    if (isSonOn) {
        [audioController playSound:@"Boutonsacc" extension:@"m4a"];
    }
    // NSString *profileName = [defaults stringForKey:nameProfil];
    //if (profileName && ![profileName isEqualToString:@""]) {
    if (!profileView) {
        profileView = [[ProfilViewController alloc]initWithNibName:@"ProfilViewController" bundle:nil];
        [profileView setDelegate:self];
    }
    [profileView.view setFrame:CGRectMake(0, 0, profileView.view.frame.size.width, profileView.view.frame.size.height)];
    [self.view addSubview:profileView.view];
    /*} else {
     if (!connexionView) {
     connexionView = [[ConnexionViewController alloc]initWithNibName:@"ConnexionViewController" bundle:nil];
     [connexionView setDelegate:self];
     }
     [connexionView.view setFrame:CGRectMake(0, 0, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
     [self.view addSubview:connexionView.view];
     }*/
    [self displayAccueil:FALSE];
}


-(NSString *)generateImage {
    int random = [self getRandomNumber:1 to:25];
    NSString *lettres = @"ABCDEFGHIJKLMOPNQRSTUVWXYZ";
    NSString *lettre = [lettres substringWithRange:NSMakeRange(random, 1)];
    return lettre;
}

-(int)getRandomNumber:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

- (IBAction)soloPressed:(id)sender {
    [Flurry logEvent:@"Solo Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Solo Accueil Affiche"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isSonOn = [defaults boolForKey:isEffetActive];
    if (isSonOn) {
        [audioController playSound:@"Boutonsacc" extension:@"m4a"];
    }
    if (!tutoSoloView) {
        tutoSoloView = [[TutoSoloViewController alloc]initWithNibName:@"TutoSoloViewController" bundle:nil];
        [tutoSoloView setDelegate:self];
    }
    [tutoSoloView.view setFrame:CGRectMake(0, 0, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    tutoSoloView.isDefi = FALSE;
    lettreSelected = [self generateImage];
    [tutoSoloView.lettreAnimationUn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationDeux setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationTrois setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationQuatre setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationConq setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationSix setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationSept setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationHuit setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationNeuf setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationDix setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    tutoSoloView.lettre = lettreSelected;
    [self.view addSubview:tutoSoloView.view];
    [self displayAccueil:FALSE];
}

- (IBAction)storePressed:(id)sender {
    [Flurry logEvent:@"Store Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Store Accueil Affiche"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isSonOn = [defaults boolForKey:isEffetActive];
    if (isSonOn) {
        [audioController playSound:@"Boutonsacc" extension:@"m4a"];
    }
    if (!trousseView) {
        trousseView = [[TrousseViewController alloc]initWithNibName:@"TrousseViewController" bundle:nil];
        [trousseView setDelegate:self];
    }
    [trousseView.view setFrame:CGRectMake(0, 0, trousseView.view.frame.size.width, trousseView.view.frame.size.height)];
    [self.view addSubview:trousseView.view];
}


- (IBAction)deconnexionPressedButton:(id)sender {
    [Flurry logEvent:@"Deco Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Deco Accueil Affiche"];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [self loading];
    [self hideReglages];
}

-(void)didReceiveDuel {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self removeAllView];
    [Flurry logEvent:@"Duel recu Accueil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Duel recu Accueil Affiche"];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.isNotif = FALSE;
    BOOL isSonOn = [defaults boolForKey:isEffetActive];
    if (isSonOn) {
        [audioController playSound:@"Boutonsacc" extension:@"m4a"];
    }
    NSString *profileName = [defaults stringForKey:nameProfil];
    if (profileName && ![profileName isEqualToString:@""]) {
        if (!selectionView) {
            selectionView = [[SelectionDuelViewController alloc]initWithNibName:@"SelectionDuelViewController" bundle:nil];
            [selectionView setDelegate:self];
        }
        [selectionView.view setFrame:CGRectMake(0, 0, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
        [self.view addSubview:selectionView.view];
    } else {
        if (!connexionView) {
            connexionView = [[ConnexionViewController alloc]initWithNibName:@"ConnexionViewController" bundle:nil];
            [connexionView setDelegate:self];
        }
        [connexionView.view setFrame:CGRectMake(0, 0, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
        [self.view addSubview:connexionView.view];
    }
    [self displayAccueil:FALSE];
}

-(void)removeAllView {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    if (profileView) {
        [profileView.view removeFromSuperview];
        profileView = nil;
        [profileView.view setFrame:CGRectMake(0, -1500, profileView.view.frame.size.width, profileView.view.frame.size.height)];
    }
    if (soloView) {
        [soloView.view removeFromSuperview];
        soloView = nil;
        [soloView.view setFrame:CGRectMake(0, -1500, soloView.view.frame.size.width, soloView.view.frame.size.height)];
    }
    if (tutoSoloView) {
        [tutoSoloView.view removeFromSuperview];
        tutoSoloView = nil;
        [tutoSoloView.view setFrame:CGRectMake(0, -1500, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    }
    if (selectionView) {
        [selectionView.view removeFromSuperview];
        selectionView = nil;
        [selectionView.view setFrame:CGRectMake(0, -1500, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
    }
    if (trousseView) {
        [trousseView.view removeFromSuperview];
        trousseView = nil;
        [trousseView.view setFrame:CGRectMake(0, -1500, trousseView.view.frame.size.width, trousseView.view.frame.size.height)];
    }
}

#pragma mark Table view methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == appTableView) {
        return 253;
    } else if (tableView == creditTableView) {
        return 253;
    }  else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == appTableView) {
        return appDataSource.count > 0 ? appDataSource.count : 1;
    } else if (tableView == creditTableView) {
        return creditDataSource.count > 0 ? creditDataSource.count : 1;
    } else {
        return 0;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == appTableView) {
        if (appDataSource != nil && [appDataSource count] > 0) {
            if (indexPath.row < [appDataSource count] - 1) {
                NSString *cellIdentifier = @"appCell";
                ApplicationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil) {
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ApplicationCell" owner:nil options:nil];
                    
                    for(id currentObject in topLevelObjects)
                    {
                        if([currentObject isKindOfClass:[ApplicationCell class]])
                        {
                            cell = (ApplicationCell *)currentObject;
                            break;
                        }
                    }
                }
                app *current = [appDataSource objectAtIndex:indexPath.row];
                if (current) {
                    [cell.appName setText:current.nom];
                    [cell.appDetail setText:current.detail];
                    if ([current.url isEqualToString:@"soon"]) {
                        cell.appUrl = nil;
                        [cell.appStoreButton setImage:[UIImage imageNamed:@"nos_jeux_btn_appstore_soon.png"] forState:UIControlStateNormal];
                    } else {
                        cell.appUrl = [NSString stringWithFormat:@"%@",current.url];
                    }
                    dispatch_async(dispatch_get_global_queue(0,0), ^{
                        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.image]];
                        if ( data == nil ) {
                            return;
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(200.0f, 200.0f)];
                            [cell.appImage setImage:newImage];
                        });
                    });
                }
                [cell.appName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
                [cell.appDetail setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
                cell.appName.adjustsFontSizeToFitWidth = YES;
                cell.appDetail.adjustsFontSizeToFitWidth = YES;
                return cell;
            } else {
                NSString *cellIdentifier = @"ContactCell";
                ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil) {
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ContactCell" owner:nil options:nil];
                    
                    for(id currentObject in topLevelObjects)
                    {
                        if([currentObject isKindOfClass:[ContactCell class]])
                        {
                            cell = (ContactCell *)currentObject;
                            break;
                        }
                    }
                }
                [cell.appName setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
                [cell.appDetail setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
                cell.appName.adjustsFontSizeToFitWidth = YES;
                cell.appDetail.adjustsFontSizeToFitWidth = YES;
                return cell;
            }
        } else {
            NSString *cellIdentifier = @"emptyAccueilCell";
            EmptyCellAccueil *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyCellAccueil" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyCellAccueil class]])
                    {
                        cell = (EmptyCellAccueil *)currentObject;
                        break;
                    }
                }
            }
            [cell.titre setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            return cell;
        }
    } else if (tableView == creditTableView) {
        if (creditDataSource != nil && [creditDataSource count] > 0) {
            NSInteger num = indexPath.row;
            if (num % 2) {
                NSString *cellIdentifier = @"creditImpairCell";
                CreditImpairCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil) {
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CreditImpairCell" owner:nil options:nil];
                    
                    for(id currentObject in topLevelObjects)
                    {
                        if([currentObject isKindOfClass:[CreditImpairCell class]])
                        {
                            cell = (CreditImpairCell *)currentObject;
                            break;
                        }
                    }
                }
                credit *current = [creditDataSource objectAtIndex:indexPath.row];
                if (current) {
                    [cell.titre setText:current.nom];
                    [cell.detail setText:current.detail];
                    dispatch_async(dispatch_get_global_queue(0,0), ^{
                        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.image]];
                        if ( data == nil ) {
                            return;
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(237.0f, 237.0f)];
                            [cell.image setImage:newImage];
                        });
                    });
                }
                [cell.titre setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
                [cell.detail setFont:[UIFont fontWithName:@"Kameron" size:15]];
                cell.titre.adjustsFontSizeToFitWidth = YES;
                return cell;
            } else {
                NSString *cellIdentifier = @"creditCell";
                CreditCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                if (cell == nil) {
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CreditCell" owner:nil options:nil];
                    
                    for(id currentObject in topLevelObjects)
                    {
                        if([currentObject isKindOfClass:[CreditCell class]])
                        {
                            cell = (CreditCell *)currentObject;
                            break;
                        }
                    }
                }
                credit *current = [creditDataSource objectAtIndex:indexPath.row];
                if (current) {
                    [cell.titre setText:current.nom];
                    [cell.detail setText:current.detail];
                    dispatch_async(dispatch_get_global_queue(0,0), ^{
                        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:current.image]];
                        if ( data == nil ) {
                            return;
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(237.0f, 237.0f)];
                            [cell.image setImage:newImage];
                        });
                    });
                }
                [cell.titre setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
                [cell.detail setFont:[UIFont fontWithName:@"Kameron" size:15]];
                cell.titre.adjustsFontSizeToFitWidth = YES;
                return cell;
            }
        } else {
            NSString *cellIdentifier = @"emptyAccueilCell";
            EmptyCellAccueil *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"EmptyCellAccueil" owner:nil options:nil];
                
                for(id currentObject in topLevelObjects)
                {
                    if([currentObject isKindOfClass:[EmptyCellAccueil class]])
                    {
                        cell = (EmptyCellAccueil *)currentObject;
                        break;
                    }
                }
            }
            [cell.titre setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
            return cell;
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


- (IBAction)duelPressed:(id)sender {
    [Flurry logEvent:@"Duel Accueil Pressed"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Duel Accueil Pressed"];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.isNotif = FALSE;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isSonOn = [defaults boolForKey:isEffetActive];
    if (isSonOn) {
        [audioController playSound:@"Boutonsacc" extension:@"m4a"];
    }
    //NSString *profileName = [defaults stringForKey:nameProfil];
    //if (profileName && ![profileName isEqualToString:@""]) {
    if (!selectionView) {
        selectionView = [[SelectionDuelViewController alloc]initWithNibName:@"SelectionDuelViewController" bundle:nil];
        [selectionView setDelegate:self];
    }
    [selectionView.view setFrame:CGRectMake(0, 0, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
    [self.view addSubview:selectionView.view];
    /*} else {
     if (!connexionView) {
     connexionView = [[ConnexionViewController alloc]initWithNibName:@"ConnexionViewController" bundle:nil];
     [connexionView setDelegate:self];
     }
     [connexionView.view setFrame:CGRectMake(0, 0, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
     [self.view addSubview:connexionView.view];
     }*/
    [self displayAccueil:FALSE];
}

#pragma mark ConnexionViewControllerDelegate
-(void)validatePressedDetail:(BOOL)nouveau {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [self profilPressed:nil];
    [Flurry logEvent:@"Accueil Affiche Connexion validee"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche Connexion validee"];
}

-(void)retourConnexionPressed {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [self displayAccueil:TRUE];
    [self loadProfile];
    [self getDuels];
    [Flurry logEvent:@"Accueil Affiche Connexion retour"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche Connexion retour"];
}

-(void)disconnectConnexionReglagesPressed {
    [self retourConnexionPressed];
    [self disconnect];
    [Flurry logEvent:@"Accueil Affiche deconnexion "];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche deconnexion"];
}

-(void)disconnect {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [self loadProfile];
    [self retourPressed];
    [Flurry logEvent:@"Accueil Affiche deconnexion "];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche deconnexion"];
    
}

#pragma mark ProfilViewControllerDelegate
-(void)deconnexionPressed {
    [self retourPressed];
    [self getDuels];
    [self soloPressed:nil];
}

-(void)retourPressed {
    if (profileView) {
        [profileView.view removeFromSuperview];
        profileView = nil;
        [profileView.view setFrame:CGRectMake(0, -1500, profileView.view.frame.size.width, profileView.view.frame.size.height)];
    }
    [self displayAccueil:TRUE];
    [self loadProfile];
    [self getDuels];
}

-(void)disconnectReglagesPressed {
    [self disconnect];
}

#pragma mark TutoSoloViewControllerDelegate
-(void)validateTutoSoloPressed:(duel *) duel isDefi:(BOOL)isDefi imageName:(NSString *)imageName {
    if (tutoSoloView) {
        [tutoSoloView.view removeFromSuperview];
        tutoSoloView = nil;
        [tutoSoloView.view setFrame:CGRectMake(0, -1500, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    }
    if (!soloView) {
        soloView = [[SoloViewController alloc]initWithNibName:@"SoloViewController" bundle:nil];
        [soloView setDelegate:self];
    }
    [soloView.view setFrame:CGRectMake(0, 0, soloView.view.frame.size.width, soloView.view.frame.size.height)];
    soloView.isDefi = isDefi;
    soloView.imageName = imageName;
    [soloView.imageLettre setImage:[UIImage imageNamed:imageName]];
    soloView.currentDuel = duel;
    [self.view addSubview:soloView.view];
}

-(void)retourTutoSoloPressed {
    if (tutoSoloView) {
        [tutoSoloView.view removeFromSuperview];
        tutoSoloView = nil;
        [tutoSoloView.view setFrame:CGRectMake(0, -1500, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    }
    [self displayAccueil:TRUE];
    [self loadProfile];
    [self getDuels];
}

-(void)disconnectTutoReglagesPressed {
    if (tutoSoloView) {
        [tutoSoloView.view removeFromSuperview];
        tutoSoloView = nil;
        [tutoSoloView.view setFrame:CGRectMake(0, -1500, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    }
    [self disconnect];
}

-(void)replayAnimation {
    if (tutoSoloView) {
        [tutoSoloView.view removeFromSuperview];
        tutoSoloView = nil;
        [tutoSoloView.view setFrame:CGRectMake(0, -1500, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    }
    if (!tutoSoloView) {
        tutoSoloView = [[TutoSoloViewController alloc]initWithNibName:@"TutoSoloViewController" bundle:nil];
        [tutoSoloView setDelegate:self];
    }
    [tutoSoloView.view setFrame:CGRectMake(0, 0, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    tutoSoloView.isDefi = FALSE;
    lettreSelected = [self generateImage];
    [tutoSoloView.lettreAnimationUn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationDeux setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationTrois setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationQuatre setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationConq setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationSix setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationSept setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationHuit setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationNeuf setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationDix setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    tutoSoloView.lettre = lettreSelected;
    [self.view addSubview:tutoSoloView.view];
}

#pragma mark SoloViewControllerDelegate
-(void)retourSoloPressed {
    if (soloView) {
        [soloView.view removeFromSuperview];
        soloView = nil;
        [soloView.view setFrame:CGRectMake(0, -1500, soloView.view.frame.size.width, soloView.view.frame.size.height)];
    }
    [self displayAccueil:TRUE];
    [self loadProfile];
    [self getDuels];
}
-(void)retourDuelPressed:(duel *)duel {
    if (soloView) {
        [soloView.view removeFromSuperview];
        soloView = nil;
        [soloView.view setFrame:CGRectMake(0, -1500, soloView.view.frame.size.width, soloView.view.frame.size.height)];
    }
    if (!selectionView) {
        selectionView = [[SelectionDuelViewController alloc]initWithNibName:@"SelectionDuelViewController" bundle:nil];
        [selectionView setDelegate:self];
    }
    selectionView.duelCorrect = duel;
    [selectionView.view setFrame:CGRectMake(0, 0, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
    [self.view addSubview:selectionView.view];
}

-(void)rejouerPressed:(BOOL)defiEnable {
    if (!defiEnable) {
        if (soloView) {
            [soloView.view removeFromSuperview];
            soloView = nil;
            [soloView.view setFrame:CGRectMake(0, -1500, soloView.view.frame.size.width, soloView.view.frame.size.height)];
        }
    } else {
        if (selectionView) {
            [selectionView.view removeFromSuperview];
            selectionView = nil;
            [selectionView.view setFrame:CGRectMake(0, -1500, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
        }
    }
    if (!soloView) {
        soloView = [[SoloViewController alloc]initWithNibName:@"SoloViewController" bundle:nil];
        [soloView setDelegate:self];
    }
    [soloView.view setFrame:CGRectMake(0, 0, soloView.view.frame.size.width, soloView.view.frame.size.height)];
    soloView.isDefi = defiEnable;
    soloView.imageName = lettreSelected;
    [soloView.imageLettre setImage:[UIImage imageNamed:lettreSelected]];
    [self.view addSubview:soloView.view];
}

-(void)nouvelleLettrePressed:(NSString *)lettre isDefi:(BOOL)defiEnable {
    if (!defiEnable) {
        if (soloView) {
            [soloView.view removeFromSuperview];
            soloView = nil;
            [soloView.view setFrame:CGRectMake(0, -1500, soloView.view.frame.size.width, soloView.view.frame.size.height)];
        }
    } else {
        if (selectionView) {
            [selectionView.view removeFromSuperview];
            selectionView = nil;
            [selectionView.view setFrame:CGRectMake(0, -1500, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
        }    }
    if (!tutoSoloView) {
        tutoSoloView = [[TutoSoloViewController alloc]initWithNibName:@"TutoSoloViewController" bundle:nil];
        [tutoSoloView setDelegate:self];
    }
    [tutoSoloView.view setFrame:CGRectMake(0, 0, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    tutoSoloView.isDefi = defiEnable;
    lettreSelected = lettre;
    [tutoSoloView.lettreAnimationUn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationDeux setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationTrois setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationQuatre setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationConq setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationSix setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationSept setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationHuit setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationNeuf setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    [tutoSoloView.lettreAnimationDix setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self generateImage]]]];
    tutoSoloView.lettre = lettreSelected;
    [self.view addSubview:tutoSoloView.view];}

#pragma mark TrousseViewControllerDelegate
-(void)retourTroussePressed {
    if (trousseView) {
        [trousseView.view removeFromSuperview];
        trousseView = nil;
        [trousseView.view setFrame:CGRectMake(0, -1500, trousseView.view.frame.size.width, trousseView.view.frame.size.height)];
    }
    [self displayAccueil:TRUE];
    [self loadProfile];
    [self getDuels];
}

#pragma mark SelectionDuelViewControllerDelegate
-(void)duelSelected:(duel *)duel {
    if (selectionView) {
        [selectionView.view removeFromSuperview];
        selectionView = nil;
        [selectionView.view setFrame:CGRectMake(0, -1500, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
    }
    if (!tutoSoloView) {
        tutoSoloView = [[TutoSoloViewController alloc]initWithNibName:@"TutoSoloViewController" bundle:nil];
        [tutoSoloView setDelegate:self];
    }
    [tutoSoloView.view setFrame:CGRectMake(0, 0, tutoSoloView.view.frame.size.width, tutoSoloView.view.frame.size.height)];
    tutoSoloView.isDefi = TRUE;
    tutoSoloView.duel = duel;
    tutoSoloView.lettre = duel.lettre;
    [self.view addSubview:tutoSoloView.view];
}

-(void)retourSelectionDuelPressed {
    if (selectionView) {
        [selectionView.view removeFromSuperview];
        selectionView = nil;
        [selectionView.view setFrame:CGRectMake(0, -1500, selectionView.view.frame.size.width, selectionView.view.frame.size.height)];
    }
    [self displayAccueil:TRUE];
    [self loadProfile];
    [self getDuels];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}
@end
