//
//  ContactCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 02/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell
@synthesize appName, appDetail;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)fbPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/QuetzalGames"]];
}

- (IBAction)twPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/quetzalgames"]];
}
@end
