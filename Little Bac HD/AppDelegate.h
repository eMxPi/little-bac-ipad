//
//  AppDelegate.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 03/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import "FBSession.h"
#import "CustomAlert.h"
#import "listeAPI.h"
#import "UIImage+ProportionalFill.h"
#import "RageIAPHelper.h"
#include <arpa/inet.h>
#import "CustomAlert.h"
#import "ApiUtils.h"
#import "Flurry.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    CustomAlert *alert;
    NSString *dToken;
    ApiUtils *utils;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) NSString *dToken;
@property (strong, nonatomic) Facebook *facebook;
@property (nonatomic) BOOL *isNotif;

// FB stuff
extern NSString *const FBSessionStateChangedNotification;
// FBLogic
// The app delegate is responsible for maintaining the current FBSession. The application requires
// the user to be logged in to Facebook in order to do anything interesting -- if there is no valid
// FBSession, a login screen is displayed.
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void) closeSession;

@end
