//
//  ApplicationCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 28/02/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlert.h"

@interface ApplicationCell : UITableViewCell <UIAlertViewDelegate> {
    CustomAlert *alert;
}
@property (strong, nonatomic) IBOutlet UIImageView *appImage;
@property (strong, nonatomic) IBOutlet UILabel *appName;
@property (strong, nonatomic) IBOutlet UILabel *appDetail;
@property (strong, nonatomic) NSString *appUrl;
@property (strong, nonatomic) IBOutlet UIButton *appStoreButton;

- (IBAction)appstorePressed:(id)sender;


@end
