//
//  StoreViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 14/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RageIAPHelper.h"
#import "ReglagesView.h"
#import "AppDelegate.h"
#import "CustomAlert.h"
#import "Reachability.h"
#import "ApiUtils.h"
#import "FBSession.h"
#import <Accounts/Accounts.h>
#import "ConnexionViewController.h"
#import "StatistiquesRequest.h"

@protocol StoreViewControllerDelegate;
@interface StoreViewController : UIViewController <UIGestureRecognizerDelegate, UIAlertViewDelegate, ConnexionViewControllerDelegate> {
    id<StoreViewControllerDelegate>__unsafe_unretained delegate;
    IBOutlet UILabel *storeTitre;
    IBOutlet UILabel *storeSousTitre;
    IBOutlet UILabel *prixTitre;
    IBOutlet UILabel *prixTitreUn;
    IBOutlet UILabel *prixTitreTrois;
    IBOutlet UILabel *prixTitreQuatre;
    IBOutlet UILabel *prixStoreMille;
    IBOutlet UILabel *prixStoreDeuxMille;
    IBOutlet UILabel *prixStoreHuitMille;
    IBOutlet UILabel *prixStoreVingtMille;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    NSNumberFormatter * _priceFormatter;
    CustomAlert *alert;
    ApiUtils *utils;
    BOOL loaded;
    IBOutlet UILabel *deconnexionReglagesLabel;
    ConnexionViewController *connexionView;
    IBOutlet UIView *storeSubView;
    IBOutlet UIImageView *fondLittles;
    IBOutlet UIButton *restaureAchatButton;
    NSString *currentAchat;
}

@property (nonatomic, unsafe_unretained) id<StoreViewControllerDelegate>delegate;
@property (strong, nonatomic) NSMutableDictionary *postParams;
- (IBAction)buyPressed:(id)sender;

- (IBAction)restaureAchatPressed:(id)sender;

- (IBAction)retourPressed:(id)sender;

// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

@end

@protocol StoreViewControllerDelegate
-(void)retourStorePressed;
@required
@end