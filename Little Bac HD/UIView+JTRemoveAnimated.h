//
//  UIView+JTRemoveAnimated.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 05/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface UIView (JTRemoveAnimated)

- (void)removeFromSuperviewAnimated;

@end
