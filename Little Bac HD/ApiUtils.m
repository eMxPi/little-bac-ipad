//
//  ApiUtils.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 11/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "ApiUtils.h"

@implementation ApiUtils

-(resultat *)resetPassword:(NSString *)profil {
    NSString *xml = [self createPseudoRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/resetPassword", apiUtilisateurUrl]] pseudo:profil];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(NSMutableArray *)appList {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", apiAppUrl]];
    NSMutableArray *utilisateurs = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLToObjectParser *apiParser = [[XMLToObjectParser alloc]
                                        parseXMLAtURL:URL toObject:@"app" parseError:nil];
        if ([[apiParser items] count] != 0) {
            utilisateurs = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                app *currentMarker = [[apiParser items] objectAtIndex:i];
                [utilisateurs addObject:currentMarker];
            }
        }
    }
    return utilisateurs;
}

-(NSMutableArray *)creditList {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", apiCreditUrl]];
    NSMutableArray *utilisateurs = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLToObjectParser *apiParser = [[XMLToObjectParser alloc]
                                        parseXMLAtURL:URL toObject:@"credit" parseError:nil];
        if ([[apiParser items] count] != 0) {
            utilisateurs = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                credit *currentMarker = [[apiParser items] objectAtIndex:i];
                [utilisateurs addObject:currentMarker];
            }
        }
    }
    return utilisateurs;
}

-(NSMutableArray *)getUtilisateurs {
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", apiUtilisateurUrl]];
    NSMutableArray *utilisateurs = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLToObjectParser *apiParser = [[XMLToObjectParser alloc]
                                        parseXMLAtURL:URL toObject:@"utilisateur" parseError:nil];
        if ([[apiParser items] count] != 0) {
            utilisateurs = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                utilisateur *currentMarker = [[apiParser items] objectAtIndex:i];
                [utilisateurs addObject:currentMarker];
            }
        }
    }
    return utilisateurs;
}

-(resultat *)addLittles:(NSString *)littles {
    NSString *xml = [self createLittlesRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiLittlesUrl]] restant:littles];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)resetBadge {
    NSString *xml = [self createRequestWithId:[NSURL URLWithString:[NSString stringWithFormat:@"%@/resetBadge", apiUtilisateurUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(resultat *)vuDuel:(NSString *)duel {
    NSString *xml = [self createRequestWithDuelForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/vuDuel", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}

-(resultat *)vuCorr:(NSString *)duel {
    NSString *xml = [self createRequestWithDuelForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/vuDuelCorr", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}

-(resultat *)hideDuels:(NSString *)duels {
    NSString *xml = [self createRequesWithIdsForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/hideDuel", apiDuelUrl]] ids:duels];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}

-(resultat *)saveTokenNotConnected:(NSString *)token {
    NSString *xml = [self createRequestWithToken:[NSURL URLWithString:[NSString stringWithFormat:@"%@/addTokenNonConnecte", apiUtilisateurUrl]] token:token];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)addToken:(NSString *)token {
    NSString *xml = [self createRequestWithMailForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/addToken", apiUtilisateurUrl]] mail:token];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)submitWord:(NSString *)mot categorie:(NSString *)categorie {
    NSString *xml = [self createRequestForWord:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiSoumissionUrl]] mot:mot categorie:categorie];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
    
}


-(resultat *)removeToken {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/resetToken", apiUtilisateurUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(resultat *)addMail:(NSString *)mail {
    NSString *xml = [self createRequestWithMailForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/addMail", apiUtilisateurUrl]] mail:mail];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)modifyLittles:(NSString *)littles {
    NSString *xml = [self createLittlesRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/modify", apiLittlesUrl]] restant:littles];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)validateMot:(NSString *)mot idCategorie:(NSString *)idCategorie {
    NSString *xml = [self createRequestMot:[NSURL URLWithString:[NSString stringWithFormat:@"%@/isValide", apiValidationUrl]] mot:mot idCategorie:idCategorie];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
    
}

-(NSString *)getLittlesRestant {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get", apiLittlesUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSString *little = @"0";
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"littles" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker.restant == nil) {
                    little = @"0";
                } else {
                    little = [NSString stringWithFormat:@"%@", currentMarker.restant];
                }
            }
        }
    }
    return little;
}

-(resultat *)addCorrection:(NSString *)littles {
    NSString *xml = [self createLittlesRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiCorrectionUrl]] restant:littles];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)addAntiSeche:(NSString *)littles {
    NSString *xml = [self createLittlesRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiAntiSecheUrl]] restant:littles];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}

-(resultat *)addTemps:(NSString *)littles {
    NSString *xml = [self createLittlesRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiTempsUrl]] restant:littles];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(resultat *)addInvitation:(NSString *)joueurDeux lettre:(NSString *)lettre {
    NSString *xml = [self createDuelRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/addInvitation", apiDuelUrl]] joueurDeux:joueurDeux lettre:lettre];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(resultat *)sendPushFinished:(NSString *)duel {
    NSString *xml = [self createRequestPushForDuelUtil:[NSURL URLWithString:[NSString stringWithFormat:@"%@/sendPushFinished", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(resultat *)sendPush:(NSString *)joueurDeux {
    NSString *xml = [self createRequestPush:[NSURL URLWithString:[NSString stringWithFormat:@"%@/sendPush", apiDuelUrl]] joueurDeux:joueurDeux];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *res = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                res = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return res;
}


-(NSString *)getCorrectionRestant {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get", apiCorrectionUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSString *little = @"0";
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"correction" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                correction *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker.restant == nil) {
                    little = @"0";
                } else {
                    little = [NSString stringWithFormat:@"%@", currentMarker.restant];
                }
            }
        }
    }
    return little;
}

-(NSString *)getAntiSecheRestant {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get", apiAntiSecheUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSString *little = @"0";
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"antiSeche" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                antiSeche *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker.restant == nil) {
                    little = @"0";
                } else {
                    little = [NSString stringWithFormat:@"%@", currentMarker.restant];
                }
            }
        }
    }
    return little;
}

-(NSString *)getTempsRestant {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get", apiTempsUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSString *little = @"0";
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"temps" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                temps *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker.restant == nil) {
                    little = @"0";
                } else {
                    little = [NSString stringWithFormat:@"%@", currentMarker.restant];
                }
            }
        }
    }
    return little;
}


-(NSString *)getDuels {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getListFromUser", apiDuelUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSString *little = @"0";
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"duel" parseError:nil];
        
        int counter = 0;
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                duel *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker != nil && currentMarker.joueurDeux != nil && currentMarker.joueurUn != nil) {
                    counter ++;
                }
            }
        }
        little = [NSString stringWithFormat:@"%i", counter];
    }
    return little;
}

-(NSMutableArray *)getDuelsEnCours {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getFromUser", apiDuelUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"duel" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                duel *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker != nil && currentMarker.joueurDeux != nil && currentMarker.joueurUn != nil) {
                    [littles addObject:[self parseDuel:currentMarker]];
                }
            }
        }
    }
    return littles;
}

-(NSMutableArray *)getDuelFinished {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getFinished", apiDuelUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"duel" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                duel *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker != nil && currentMarker.joueurDeux != nil && currentMarker.joueurUn != nil) {
                    [littles addObject:[self parseDuel:currentMarker]];
                }
            }
        }
    }
    return littles;
}

-(NSMutableArray *)getUtilisateursFiltered {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getFiltered", apiUtilisateurUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"utilisateur" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                utilisateur *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker != nil && currentMarker.id != nil && currentMarker.nom != nil) {
                    [littles addObject:currentMarker];
                }
            }
        }
    }
    return littles;
}

-(BOOL)checkFacebookFromUtilisateur:(NSString *)utilisateur {
    NSString *xml = [self createRequestWithUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/checkFacebook", apiUtilisateurUrl]] utilisateur:utilisateur];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    BOOL littles = FALSE;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                resultat *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker && [currentMarker.message isEqualToString:@"OK"]) {
                    littles = TRUE;
                }
            }
        }
    }
    return littles;
}

-(resultat *)valideInvitation:(NSString *)duel {
    NSString *xml = [self createRequestWithDuel:[NSURL URLWithString:[NSString stringWithFormat:@"%@/valideInvitation", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}

-(resultat *)demarrerDuel:(NSString *)duel {
    NSString *xml = [self createRequestWithDuelForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/demarrerDuel", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}

-(NSMutableArray *)getOrdered:(NSString *)ids {
    NSString *xml = [self createRequesWithIds:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getOrdered", apiCategorieUrl]] ids:ids];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"categorie" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                categorie *currentMarker = [[apiParser items] objectAtIndex:i];
                [littles addObject:currentMarker];
            }
        }
    }
    return littles;
    
}

-(NSMutableArray *)validateDuel:(NSString *)duel {
    NSString *xml = [self createRequestWithDuelForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/valideDuelById", apiValidationUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"reponse" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                reponse *currentMarker = [[apiParser items] objectAtIndex:i];
                [littles addObject:currentMarker];
            }
        }
    }
    return littles;
}

-(resultat *)finirDuel:(NSString *)duel {
    NSString *xml = [self createRequestWithDuelForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/finirDuelWithoutPush", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}

-(resultat *)isFinished:(NSString *)duel {
    NSString *xml = [self createRequestWithDuel:[NSURL URLWithString:[NSString stringWithFormat:@"%@/isFinished", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}



-(resultat *)addReponses:(NSString *)duel reponses:(NSString *)reponses {
    NSString *xml = [self createReponseRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/addReponses", apiReponsesUrl]] duel:duel reponses:reponses];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}


-(resultat *)removeDuel:(NSString *)duel {
    NSString *xml = [self createRequestWithDuelForUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/remove", apiDuelUrl]] duel:duel];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    resultat *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                littles = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return littles;
}


-(NSMutableArray *)getDuelsInvite {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getInvitation", apiDuelUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"duel" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                duel *currentMarker = [[apiParser items] objectAtIndex:i];
                if (currentMarker != nil && currentMarker.joueurDeux != nil && currentMarker.joueurUn != nil) {
                    [littles addObject:[self parseDuelInvitation:currentMarker]];
                }
            }
        }
    }
    return littles;
}

-(progression *)getProgression {
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get", apiProgressionUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    progression *progression = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"progression" parseError:nil];
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                progression = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return progression;
}

-(progression *)getProgressionForUser:(NSString *)user {
    NSString *xml = [self createRequestWithUser:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get", apiProgressionUrl]] utilisateur:user];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    progression *progression = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"progression" parseError:nil];
        if ([[apiParser items] count] != 0) {
            for(int i = 0; i < [[apiParser items] count]; i++) {
                progression = [[apiParser items] objectAtIndex:i];
            }
        }
    }
    return progression;
}


-(resultat *)addProgression:(NSString *)scoreSolo scoreDuo:(NSString*)scoreDuo {
    resultat *res = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *profileName = [defaults stringForKey:nameProfil];
    if (profileName && ![profileName isEqualToString:@""]) {
        NSString *xml = [self createProgressionRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiProgressionUrl]] scoreSolo:scoreSolo scoreDuo:scoreDuo];
        NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
        
        if ([self isNetworkEnabled] && [self isAPIReachable]) {
            XMLStringParser *apiParser = [[XMLStringParser alloc]
                                          parseXMLString:postData toObject:@"resultat" parseError:nil];
            
            if ([[apiParser items] count] != 0) {
                for(int i = 0; i < [[apiParser items] count]; i++) {
                    res = [[apiParser items] objectAtIndex:i];
                }
            }
        }
    }
    return res;
    
}

-(NSMutableArray *)getDuelsTotal {
    NSMutableArray *littles = [self getDuelsEnCours];
    NSMutableArray *temps = [self getDuelsInvite];
    for(int i = 0; i < [temps count]; i++) {
        duel *currentMarker = [temps objectAtIndex:i];
        [littles addObject:currentMarker];
    }
    return littles;
}


-(NSString *)modifStatus:(NSString *)statut {
    NSString *statusOut = @"";
    if ([statut isEqualToString:@"A1"]) {
        statusOut = @"En attente";
    } else if ([statut isEqualToString:@"A2"]) {
        statusOut = @"En attente";
    } else if ([statut isEqualToString:@"D1"] || [statut isEqualToString:@"D2"]) {
        statusOut = @"A son tour !";
    } else if ([statut isEqualToString:@"C1"] || [statut isEqualToString:@"C2"]) {
        statusOut = @"Confirmé";
    } else if ([statut isEqualToString:@"F1"]) {
        statusOut = @"Correction disponible";
    } else if ([statut isEqualToString:@"F2"]) {
        statusOut = @"A toi de jouer !";
    } else if ([statut isEqualToString:@"I1"]) {
        statusOut = @"Invitation envoyée";
    } else if ([statut isEqualToString:@"I2"]) {
        statusOut = @"Invitation reçue";
    }
    return statusOut;
}

-(duel *)parseDuel:(duel*)currentMarker {
    duel *outMarker = nil;
    if (currentMarker != nil) {
        outMarker = [[duel alloc] init];
        outMarker.id = currentMarker.id;
        outMarker.categories = currentMarker.categories;
        outMarker.lettre = currentMarker.lettre;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *user = [defaults stringForKey:nameProfil];
        if ([user isEqualToString:currentMarker.joueurUn]) {
            if ([currentMarker.StatutJoueurDeux isEqualToString:@"F2"] && [currentMarker.StatutJoueurUn isEqualToString:@"F1"]) {
                outMarker.StatutJoueurUn = @"Correction disponible";
                outMarker.StatutJoueurDeux = @"Correction disponible";
            } else {
                currentMarker.StatutJoueurUn = [self modifStatus:currentMarker.StatutJoueurUn];
                currentMarker.StatutJoueurDeux = [self modifStatus:currentMarker.StatutJoueurDeux];
            }
            outMarker = currentMarker;
        } else {
            outMarker.joueurUn = currentMarker.joueurDeux;
            outMarker.joueurDeux = currentMarker.joueurUn;
            outMarker.imageJoueurUn = currentMarker.imageJoueurDeux;
            outMarker.imageJoueurDeux = currentMarker.imageJoueurUn;
            outMarker.StatutJoueurUn = currentMarker.StatutJoueurDeux;
            outMarker.StatutJoueurDeux = currentMarker.StatutJoueurUn;
            if ([currentMarker.StatutJoueurDeux isEqualToString:@"A2"]) {
                outMarker.StatutJoueurUn = @"Te défie";
            }
            if ([outMarker.StatutJoueurDeux isEqualToString:@"F1"]) {
                outMarker.StatutJoueurDeux = @"F2";
            }
            if ([currentMarker.StatutJoueurDeux isEqualToString:@"F2"] && [currentMarker.StatutJoueurUn isEqualToString:@"F1"]) {
                outMarker.StatutJoueurUn = @"Correction disponible";
                outMarker.StatutJoueurDeux = @"Correction disponible";
            } else if ([currentMarker.StatutJoueurDeux isEqualToString:@"F2"] && [currentMarker.StatutJoueurUn isEqualToString:@"F1"]) {
                outMarker.StatutJoueurUn = @"Correction disponible";
                outMarker.StatutJoueurDeux = @"Correction disponible";
            } else {
                outMarker.StatutJoueurUn = [self modifStatus:outMarker.StatutJoueurUn];
                outMarker.StatutJoueurDeux = [self modifStatus:outMarker.StatutJoueurDeux];
            }
            outMarker.vuDuelUn = currentMarker.vuDuelDeux;
            outMarker.vuDuelDeux = currentMarker.vuDuelUn;
            outMarker.vuCorrUn = currentMarker.vuCorrDeux;
            outMarker.vuCorrDeux = currentMarker.vuCorrUn;
        }
    }
    return outMarker;
}

-(duel *)parseDuelInvitation:(duel*)currentMarker {
    duel *outMarker = nil;
    if (currentMarker != nil) {
        outMarker = [[duel alloc] init];
        outMarker.id = currentMarker.id;
        outMarker.categories = currentMarker.categories;
        outMarker.lettre = currentMarker.lettre;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *user = [defaults stringForKey:nameProfil];
        if ([user isEqualToString:currentMarker.joueurDeux]) {
            outMarker.StatutJoueurUn = @"En attente";
            outMarker.StatutJoueurDeux = @"Te défie";
        }
        outMarker.joueurUn = currentMarker.joueurDeux;
        outMarker.joueurDeux = currentMarker.joueurUn;
        outMarker.imageJoueurUn = currentMarker.imageJoueurDeux;
        outMarker.imageJoueurDeux = currentMarker.imageJoueurUn;
        outMarker.vuDuelUn = currentMarker.vuDuelDeux;
        outMarker.vuDuelDeux = currentMarker.vuDuelUn;
        outMarker.vuCorrUn = currentMarker.vuCorrDeux;
        outMarker.vuCorrDeux = currentMarker.vuCorrUn;
    }
    return outMarker;
}

-(NSMutableArray *)getCorrectionSolo:(NSString *)mots categories:(NSString *)categories lettre:(NSString *)lettre {
    NSString *xml = [self createRequestSolo:[NSURL URLWithString:[NSString stringWithFormat:@"%@/valideSolo", apiValidationUrl]] mots:mots categories:categories lettre:lettre];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    NSMutableArray *littles = nil;
    if ([self isNetworkEnabled] && [self isAPIReachable]) {
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"reponse" parseError:nil];
        
        if ([[apiParser items] count] != 0) {
            littles = [[NSMutableArray alloc] init];
            for(int i = 0; i < [[apiParser items] count]; i++) {
                reponse *currentMarker = [[apiParser items] objectAtIndex:i];
                [littles addObject:currentMarker];
            }
        }
    }
    return littles;
}

-(NSMutableArray *)getCorrectionDuel:(NSString *)mots categories:(NSString *)categories {
    
    NSMutableArray *littles = nil;
    return littles;
}


-(NSString *)createProgressionRequest:(NSURL *)url scoreSolo:(NSString *)scoreSolo scoreDuo:(NSString *)scoreDuo{
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&meilleurScoreSolo=%@&meilleurScoreDuo=%@", chaine, scoreSolo, scoreDuo] dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"requete = %@",[NSString stringWithFormat:@"utilisateur=%@&jeu=%@", [defaults stringForKey:nameProfil], jeu] );
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createLittlesRequest:(NSURL *)url restant:(NSString *)restant{
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&restant=%@", chaine, restant] dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"requete = %@",[NSString stringWithFormat:@"utilisateur=%@&jeu=%@", [defaults stringForKey:nameProfil], jeu] );
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestMot:(NSURL *)url mot:(NSString *)mot idCategorie:(NSString *)idCategorie {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"mot=%@&id=%@", mot, idCategorie] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequesWithIds:(NSURL *)url ids:(NSString *)ids {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"ids=%@", ids] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequesWithIdsForUser:(NSURL *)url ids:(NSString *)ids {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&ids=%@",chaine, ids] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestSolo:(NSURL *)url mots:(NSString *)mots categories:(NSString *)categories lettre:(NSString *)lettre {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&mots=%@&ids=%@&lettre=%@", chaine, mots, categories, lettre] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestWithUser:(NSURL *)url utilisateur:(NSString *)utilisateur {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSString *chaine = [utilisateur stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@", chaine] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createReponseRequest:(NSURL *)url duel:(NSString *)duel reponses:(NSString *)responses {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&duel=%@&reponses=%@", chaine, duel, responses] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestWithDuel:(NSURL *)url duel:(NSString *)duel {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"duel=%@", duel] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestWithDuelForUser:(NSURL *)url duel:(NSString *)duel {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&duel=%@", chaine, duel] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}


-(NSString *)createRequestPush:(NSURL *)url joueurDeux:(NSString *)joueurDeux {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    NSString *chaine2 = [joueurDeux stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"joueurUn=%@&joueurDeux=%@", chaine, chaine2] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}


-(NSString *)createRequestPushForDuelUtil:(NSURL *)url duel:(NSString *)duel {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@&duel=%@", chaine, duel] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestPushForDuel:(NSURL *)url duel:(NSString *)duel {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"joueurUn=%@&duel=%@", chaine, duel] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestWithMailForUser:(NSURL *)url mail:(NSString *)mail {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"id=%@&mail=%@", chaine, mail] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}



-(NSString *)createRequestWithToken:(NSURL *)url token:(NSString *)token {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"token=%@", token] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}


-(NSString *)createRequestForWord:(NSURL *)url mot:(NSString *)mot categorie:(NSString *)categorie{
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"mot=%@&categorie=%@", mot, categorie] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequest:(NSURL *)url {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@", chaine] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createRequestWithId:(NSURL *)url {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"id=%@", chaine] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createDuelRequest:(NSURL *)url joueurDeux:(NSString*)joueurDeux lettre:(NSString*)lettre{
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chaine = [[defaults stringForKey:nameProfil] stringByReplacingOccurrencesOfString:@" "
                                                                                     withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"joueurUn=%@&joueurDeux=%@&lettre=%@", chaine, joueurDeux, lettre] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}

-(NSString *)createPseudoRequest:(NSURL *)url pseudo:(NSString*)pseudo {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSString *chaine = [pseudo stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"utilisateur=%@", chaine] dataUsingEncoding:NSUTF8StringEncoding]];
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}


- (BOOL)isNetworkEnabled
{
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(8080);
    address.sin_addr.s_addr = inet_addr(apiHost);
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    Reachability *reach2 = [Reachability reachabilityWithAddress:&address];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    NetworkStatus net2 = [reach2 currentReachabilityStatus];
    return (networkStatus != NotReachable && net2 != NotReachable);
}

-(BOOL)isAPIReachable {
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(8080);
    address.sin_addr.s_addr = inet_addr(apiHost);
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    Reachability *reach2 = [Reachability reachabilityWithAddress:&address];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    NetworkStatus net2 = [reach2 currentReachabilityStatus];
    return (networkStatus != NotReachable && net2 != NotReachable);
    
}
-(BOOL)canReach {
    return ([self isNetworkEnabled] && [self isAPIReachable]);
}

@end
