//
//  XMLToObjectParser.h
//  XMLparser
//
//  Created by M. Pontoire
//  Copyright 2012 naonedBikes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface XMLToObjectParser : NSObject <NSXMLParserDelegate> {
    NSString *className;
	NSMutableArray *items;
	NSObject *item; // stands for any class    
	NSString *currentNodeName;
	NSMutableString *currentNodeContent;
}

- (NSArray *)items;
- (id)parseXMLAtURL:(NSURL *)url 
		   toObject:(NSString *)aClassName 
		 parseError:(NSError **)error;
- (id)parseXMLAtFile:(NSString *)file 
		   toObject:(NSString *)aClassName 
		 parseError:(NSError **)error;

@end
