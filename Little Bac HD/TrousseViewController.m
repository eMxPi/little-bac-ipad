//
//  TrousseViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 14/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "TrousseViewController.h"

@interface TrousseViewController ()

@end

@implementation TrousseViewController
@synthesize delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [titleText setFont:[UIFont fontWithName:@"Kameron-Bold" size:17]];
    [littlesTotal setFont:[UIFont fontWithName:@"Sansita One" size:30]];
    [troussePrix setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [choisirUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [choisirDeux setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [choisirTrois setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [detailCorrection setFont:[UIFont fontWithName:@"Kameron-Bold" size:14]];
    [detailHorloge setFont:[UIFont fontWithName:@"Kameron-Bold" size:14]];
    [detailAntiSeche setFont:[UIFont fontWithName:@"Kameron-Bold" size:14]];
    [tempsCounter setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [suivantCounter setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [correctionCounter setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    tempsCounter.adjustsFontSizeToFitWidth = YES;
    suivantCounter.adjustsFontSizeToFitWidth = YES;
    correctionCounter.adjustsFontSizeToFitWidth = YES;
    littlesTotal.adjustsFontSizeToFitWidth = YES;
    
    
    [milleCorrectionLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [deuxMilleCorrectionLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [troisMilleCorrectionLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [milleAntiSecheLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [deuxMilleAntiSecheLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [troisMilleAntiSecheLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [milleTempsLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [deuxMilleTempsLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [troisMilleTempsLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    
    
    [packTenAntiseche setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTenCorrection setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTenTemps setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTwentyAntiseche setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTwentyCorrection setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTwentyTemps setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTwentyFiveAntiseche setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTwentyFiveCorrection setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [packTwentyFiveTemps setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Trousse Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Trousse Affiche"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    littlesRestant  = 0;
    [self loadProfile];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        littlesRestant = [[utils getLittlesRestant] intValue];
        [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
        [suivantCounter setText:[NSString stringWithFormat:@"x%@", [utils getAntiSecheRestant]]];
        [tempsCounter setText:[NSString stringWithFormat:@"x%@", [utils getTempsRestant]]];
        [correctionCounter setText:[NSString stringWithFormat:@"x%@", [utils getCorrectionRestant]]];
    } else {
        littlesRestant = [[defaults stringForKey:littlesCurrent] intValue];
        [littlesTotal setText:[NSString stringWithFormat:@"- %i -", [self checkValue:[defaults stringForKey:littlesCurrent]]]];
        [suivantCounter setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:nextBonusCurrent]]]];
        [tempsCounter setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:tempsBonusCurrent]]]];
        [correctionCounter setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:checkBonusCurrent]]]];
        isEffet = [defaults boolForKey:isEffetActive];
        isMusique = [defaults boolForKey:isMusiqueActive];
        isShare = [defaults boolForKey:isShareActive];
    }
    [self updateBonus];
}

-(int)checkValue:(NSString *)value {
    if (value && ![value isEqualToString:@""] && ![value isEqualToString:@"(null)"]) {
        return [value intValue];
    } else {
        return 0;
    }
}

-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}

-(BOOL)checkCanBuy:(int)value {
    return value <= littlesRestant;
}

- (IBAction)troussePressed:(id)sender {
    if ([self checkCanBuy:750]) {
        littlesRestant -= 750;
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
        [utils addCorrection:@"3"];
        [utils addTemps:@"3"];
        [utils addAntiSeche:@"3"];
        [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
        int counterVal = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 3;
        [tempsCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        counterVal = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 3;
        [suivantCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        counterVal = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 3;
        [correctionCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}


- (IBAction)deconnexionPressedButton:(id)sender {
    [self hideReglages];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [self loadProfile];
    [self retourPressed:nil];
}

- (IBAction)correctionPressed:(id)sender {
    [fondSombre setHidden:FALSE];
    [horlogeView setHidden:TRUE];
    [correctionView setHidden:FALSE];
    [antiSecheView setHidden:TRUE];
}

- (IBAction)antisechePressed:(id)sender {
    [fondSombre setHidden:FALSE];
    [horlogeView setHidden:TRUE];
    [antiSecheView setHidden:FALSE];
    [correctionView setHidden:TRUE];
}

- (IBAction)horlogePressed:(id)sender {
    [fondSombre setHidden:FALSE];
    [horlogeView setHidden:FALSE];
    [correctionView setHidden:TRUE];
    [antiSecheView setHidden:TRUE];
}

- (IBAction)storePressed:(id)sender {
    if (!storeView) {
        storeView = [[StoreViewController alloc]initWithNibName:@"StoreViewController" bundle:nil];
        [storeView setDelegate:self];
    }
    [storeView.view setFrame:CGRectMake(0, 0, storeView.view.frame.size.width, storeView.view.frame.size.height)];
    [self.view addSubview:storeView.view];
}


-(void)updateBonus {
    int counterVal = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (counterVal <= 0) {
        [correctionImage setImage:[UIImage imageNamed:@"bonus_correction_plein.png"]];
    } else {
        [correctionImage setImage:[UIImage imageNamed:@"bonus_correction_uo.png"]];
    }
    int antiVal = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (antiVal <= 0) {
        [suivantImage setImage:[UIImage imageNamed:@"mot.png"]];
    } else {
        [suivantImage setImage:[UIImage imageNamed:@"bonus_mot_up.png"]];
    }
    int tempsVal = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (tempsVal <= 0) {
        [tempsImage setImage:[UIImage imageNamed:@"pause.png"]];
    } else {
        [tempsImage setImage:[UIImage imageNamed:@"bonus_pause_up.png"]];
    }
    
}

- (IBAction)retourPressed:(id)sender {
    [delegate retourTroussePressed];
}


- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}


-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}


#pragma mark StoreViewControllerDelegate
-(void)retourStorePressed {
    if (storeView) {
        [storeView.view removeFromSuperview];
        storeView = nil;
        [storeView.view setFrame:CGRectMake(0, -1500, storeView.view.frame.size.width, storeView.view.frame.size.height)];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        littlesRestant = [[utils getLittlesRestant] intValue];
        [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
    } else {
        [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
        littlesRestant = [[defaults stringForKey:littlesCurrent] intValue];
        [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
    }
}
- (IBAction)tenCorrectionPressed:(id)sender {
    if ([self checkCanBuy:1000]) {
        littlesRestant -= 1000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addCorrection:@"10"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 10;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:checkBonusCurrent];
        }
        int counterVal = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 10;
        [correctionCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [correctionView setHidden:TRUE];
}

- (IBAction)twentyCorrectionPressed:(id)sender {
    if ([self checkCanBuy:2000]) {
        littlesRestant -= 2000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addCorrection:@"25"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 25;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:checkBonusCurrent];
        }
        int counterVal = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 25;
        [correctionCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [correctionView setHidden:TRUE];
}

- (IBAction)twentyFiveCorrectionPressed:(id)sender {
    if ([self checkCanBuy:3000]) {
        littlesRestant -= 3000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addCorrection:@"50"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 50;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:checkBonusCurrent];
        }
        int counterVal = [[[correctionCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 50;
        [correctionCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [antiSecheView setHidden:TRUE];
}

- (IBAction)outCorrectionPressed:(id)sender {
    [fondSombre setHidden:TRUE];
    [correctionView setHidden:TRUE];
}

- (IBAction)tenAntiSechePressed:(id)sender {
    if ([self checkCanBuy:1000]) {
        littlesRestant -= 1000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addAntiSeche:@"10"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 10;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:nextBonusCurrent];
        }
        int counterVal = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 10;
        [suivantCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
        [fondSombre setHidden:TRUE];
        [antiSecheView setHidden:TRUE];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

- (IBAction)twentyAntiSechePressed:(id)sender {
    if ([self checkCanBuy:2000]) {
        littlesRestant -= 2000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addAntiSeche:@"25"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 25;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:nextBonusCurrent];
        }
        int counterVal = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 25;
        [suivantCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [antiSecheView setHidden:TRUE];
}

- (IBAction)twentyFiveAntiSechePressed:(id)sender {
    if ([self checkCanBuy:3000]) {
        littlesRestant -= 3000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addAntiSeche:@"50"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 50;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:nextBonusCurrent];
        }
        int counterVal = [[[suivantCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 50;
        [suivantCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [horlogeView setHidden:TRUE];
}

- (IBAction)outAntiSechePressed:(id)sender {
    [fondSombre setHidden:TRUE];
    [antiSecheView setHidden:TRUE];
}

- (IBAction)twentyTempsPressed:(id)sender {
    if ([self checkCanBuy:2000]) {
        littlesRestant -= 2000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addTemps:@"10"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 25;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:tempsBonusCurrent];
        }
        int counterVal = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 25;
        [tempsCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [correctionView setHidden:TRUE];
}

- (IBAction)tenTempsPressed:(id)sender {
    if ([self checkCanBuy:1000]) {
        littlesRestant -= 1000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addTemps:@"25"];
        } else {
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 10;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:tempsBonusCurrent];
        }
        int counterVal = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 10;
        [tempsCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [horlogeView setHidden:TRUE];
}

- (IBAction)twentyFiveTempsPressed:(id)sender {
    if ([self checkCanBuy:3000]) {
        littlesRestant -= 3000;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils modifyLittles:[NSString stringWithFormat:@"%i", littlesRestant]];
            [littlesTotal setText:[NSString stringWithFormat:@"- %i -", littlesRestant]];
            [utils addTemps:@"50"];
        } else {
            [defaults setValue:[NSString stringWithFormat:@"%i", littlesRestant] forKey:littlesCurrent];
            int counter = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 50;
            [defaults setValue:[NSString stringWithFormat:@"%i", counter] forKey:tempsBonusCurrent];
        }
        int counterVal = [[[tempsCounter text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue] + 50;
        [tempsCounter setText:[NSString stringWithFormat:@"x%i", counterVal]];
        [self updateBonus];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Argent de poche !" message:@"Tu n’as pas assez de Littles ! Prépares ta liste de fournitures, et viens faire un tour à l’économat !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
    [fondSombre setHidden:TRUE];
    [horlogeView setHidden:TRUE];
}

- (IBAction)outTempsPressed:(id)sender {
    [fondSombre setHidden:TRUE];
    [horlogeView setHidden:TRUE];
}


#pragma mark UIalertFirstViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = alertView.title;
    if ([title isEqualToString:@"Argent de poche !"]) {
        [self storePressed:nil];
    }
}
@end
