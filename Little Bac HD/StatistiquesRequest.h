//
//  StatistiquesRequest.h
//  Captain Price
//
//  Created by Maxime Pontoire on 15/12/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "listeAPI.h"
#import "CustomAlert.h"
#import "resultat.h"
#import "XMLStringParser.h"
#import "Reachability.h"
#include <arpa/inet.h>
#import <SystemConfiguration/SystemConfiguration.h>


@interface StatistiquesRequest : NSObject


-(BOOL)writeMessage:(NSString *)fichier message:(NSString *)message;
@end
