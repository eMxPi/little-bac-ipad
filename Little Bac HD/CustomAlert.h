//
//  CustomAlert.h
//  Captain Price
//
//  Created by Maxime Pontoire on 20/09/12.
//  Copyright (c) 2012 Maxime Pontoire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlert : UIAlertView

@property (nonatomic) int counter;

@end
