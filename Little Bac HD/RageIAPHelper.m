//
//  RageIAPHelper.m
//  Captain Price
//
//  Created by Maxime Pontoire on 29/11/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import "RageIAPHelper.h"

@implementation RageIAPHelper

+ (RageIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static RageIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.quetzalgames.little.bac.ipad.mille",
                                      @"com.quetzalgames.little.bac.ipad.deux",
                                      @"com.quetzalgames.little.bac.ipad.huit",
                                      @"com.quetzalgames.little.bac.ipad.vingt",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end