//
//  ApiUtils.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 11/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "listeAPI.h"
#import "XMLStringParser.h"
#import "XMLToObjectParser.h"
#import "Reachability.h"
#import "CustomAlert.h"
#import "littles.h"
#include <arpa/inet.h>
#import "resultat.h"
#import "correction.h"
#import "antiSeche.h"
#import "temps.h"
#import "duel.h"
#import "reponse.h"
#import "progression.h"
#import "utilisateur.h"
#import "categorie.h"
#import "app.h"
#import "credit.h"

#define tempsRatio 10
#define plusRatio 30
#define suivantRatio 50

@interface ApiUtils : NSObject {
    CustomAlert *alert;
}


-(NSMutableArray *)appList;
-(NSMutableArray *)creditList;

-(resultat *)resetPassword:(NSString *)profil;

-(resultat *)saveTokenNotConnected:(NSString *)token;

-(resultat *)addToken:(NSString *)token;
-(resultat *)removeToken;
-(resultat *)addMail:(NSString *)mail;
-(resultat *)resetBadge;
-(resultat *)vuDuel:(NSString *)duel;
-(resultat *)vuCorr:(NSString *)duel;
-(resultat *)hideDuels:(NSString *)duels;

//soumission
-(resultat *)submitWord:(NSString *)mot categorie:(NSString *)categorie;

// Duels
-(NSString *)getDuels;
-(NSMutableArray *)getDuelsEnCours;
-(NSMutableArray *)getDuelsInvite;
-(NSMutableArray *)getDuelsTotal;
-(resultat *)addInvitation:(NSString *)joueurDeux lettre:(NSString*)lettre;
-(NSMutableArray *)getDuelFinished;
-(resultat *)valideInvitation:(NSString *)duel;
-(resultat *)demarrerDuel:(NSString *)duel;
-(resultat *)finirDuel:(NSString *)duel;
-(resultat *)removeDuel:(NSString *)duel;
-(resultat *)isFinished:(NSString *)duel;
-(resultat *)sendPush:(NSString *)joueurDeux;
-(resultat *)sendPushFinished:(NSString *)duel;

-(resultat *)addReponses:(NSString *)duel reponses:(NSString*)reponses;
-(NSMutableArray *)validateDuel:(NSString *)duel;


-(NSMutableArray *)getOrdered:(NSString *)ids;

// Utilisateur
-(NSMutableArray *)getUtilisateurs;
-(NSMutableArray *)getUtilisateursFiltered;
-(BOOL)checkFacebookFromUtilisateur:(NSString *)utilisateur;


//Profil
-(progression *)getProgression;
-(progression *)getProgressionForUser:(NSString *)user;
-(resultat *)addProgression:(NSString *)scoreSolo scoreDuo:(NSString*)scoreDuo;

-(resultat *)addLittles:(NSString *)littles;
-(resultat *)modifyLittles:(NSString *)littles;
-(NSString *)getLittlesRestant;

-(resultat *)addCorrection:(NSString *)littles;
-(resultat *)addAntiSeche:(NSString *)littles;
-(resultat *)addTemps:(NSString *)littles;
-(NSString *)getCorrectionRestant;
-(NSString *)getAntiSecheRestant;
-(NSString *)getTempsRestant;

// Correction
-(NSMutableArray *)getCorrectionSolo:(NSString *)mots categories:(NSString *)categories lettre:(NSString *)lettre;
-(NSMutableArray *)getCorrectionDuel:(NSString *)mots categories:(NSString *)categories;
-(resultat *)validateMot:(NSString *)mot idCategorie:(NSString *)idCategorie;

//Connexion
-(BOOL)isNetworkEnabled;
-(BOOL)isAPIReachable;
-(BOOL)canReach;
@end
