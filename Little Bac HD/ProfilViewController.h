//
//  ProfilViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 04/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>
#import "UIImage+ProportionalFill.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "AppDelegate.h"
#import "DEFacebookComposeViewController.h"
#import "ReglagesView.h"
#import "ApiUtils.h"
#import "TrousseViewController.h"
#import "StatistiquesRequest.h"
#import "ConnexionViewController.h"
#import "CustomAlert.h"

@protocol ProfilViewControllerDelegate;
@interface ProfilViewController : UIViewController <MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, TrousseViewControllerDelegate, ConnexionViewControllerDelegate, UIAlertViewDelegate> {
    id<ProfilViewControllerDelegate>__unsafe_unretained delegate;
    IBOutlet UIImageView *imageProfile;
    IBOutlet UILabel *bonjourLabel;
    IBOutlet UILabel *deconnexionLabel;
    IBOutlet UILabel *loginLabel;
    IBOutlet UILabel *littleLabel;
    IBOutlet UILabel *tempsLabel;
    IBOutlet UILabel *plusUnLabel;
    IBOutlet UILabel *motLabel;
    IBOutlet UILabel *storeLabel;
    IBOutlet UILabel *invitationLabel;
    IBOutlet UILabel *totalLabel;
    IBOutlet UILabel *totalScoreLabel;
    IBOutlet UILabel *meilleurScoreLabel;
    IBOutlet UILabel *meilleurScoreScoreLabel;
    IBOutlet UILabel *meilleurSoloLabel;
    IBOutlet UILabel *meilleurSoloScoreLabel;
    IBOutlet UILabel *meilleurDuoLabel;
    IBOutlet UILabel *meilleurDuoScoreLabel;
    IBOutlet UILabel *meilleurSerieLabel;
    IBOutlet UILabel *meilleurSerieScoreLabel;
    IBOutlet UILabel *scoreMoyenLabel;
    IBOutlet UILabel *scoreMoyenScoreLabel;
    IBOutlet UIActivityIndicatorView *loadingIndicator;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    IBOutlet UILabel *deconnexionReglagesLabel;
    ApiUtils *utils;
    IBOutlet UIButton *tempsButton;
    IBOutlet UIButton *plusUnButton;
    IBOutlet UIButton *suivantButton;
    TrousseViewController *trousseView;
    ConnexionViewController *connexionView;
    CustomAlert *alert;
}
@property (nonatomic, unsafe_unretained) id<ProfilViewControllerDelegate>delegate;
@property (nonatomic, retain) UIImage *currentImage;

- (IBAction)deconnexionPressed:(id)sender;
- (IBAction)retourPressed:(id)sender;
- (IBAction)storePressed:(id)sender;
- (IBAction)postFB:(id)sender;
- (IBAction)postMail:(id)sender;


// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

@end

@protocol ProfilViewControllerDelegate
-(void)deconnexionPressed;
-(void)disconnectReglagesPressed;
-(void)retourPressed;
@required
@end