//
//  XMLStringParser.m
//  Captain Price
//
//  Created by Maxime Pontoire on 17/10/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import "XMLStringParser.h"

@implementation XMLStringParser

- (NSArray *)items
{
	return items;
}

- (id)parseXMLString:(NSData *)data
		   toObject:(NSString *)aClassName
		 parseError:(NSError **)error
{
	//[items release];
	items = [[NSMutableArray alloc] init];
	className = aClassName;
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
	[parser setDelegate:self];
	[parser parse];
    
	if([parser parserError] && error) {
		*error = [parser parserError];
	}
	//[parser release];
	return self;
}

- (id)parseXMLAtFile:(NSString *)file
            toObject:(NSString *)aClassName
          parseError:(NSError **)error
{
	//[items release];
	items = [[NSMutableArray alloc] init];
	className = aClassName;
    NSArray *fileTab = [file componentsSeparatedByString: @"."];
    NSString *fileName = [fileTab objectAtIndex:0];
    NSString *extension = [fileTab objectAtIndex:1];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:extension];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:myData];
	[parser setDelegate:self];
	[parser parse];
    
	if([parser parserError] && error) {
		*error = [parser parserError];
	}
	//[parser release];
	return self;
}

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
	attributes:(NSDictionary *)attributeDict
{
    //NSLog(@"Open tag: %@", elementName);
	if([elementName isEqualToString:className]) {
		// create an instance of a class on run-time
		item = [[NSClassFromString(className) alloc] init];
    } else {
		currentNodeName = [elementName copy];
		currentNodeContent = [[NSMutableString alloc] init];
	}
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    //NSLog(@"Close tag: %@", elementName);
	if([elementName isEqualToString:className]) {
		[items addObject:item];
		//[item release];
		item = nil;
	}
	else if([elementName isEqualToString:currentNodeName]) {
		// use key-value coding
        //NSLog(@"Valeur: %@", currentNodeContent);
		[item setValue:currentNodeContent forKey:elementName];
        
		//[currentNodeContent release];
		currentNodeContent = nil;
        
		//[currentNodeName release];
		currentNodeName = nil;
	}
}

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{
	[currentNodeContent appendString:string];
}

- (void)dealloc
{
	//[items release];
	//[super dealloc];
}
@end
