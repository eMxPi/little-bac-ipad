//
//  AudioController.m
//  Captain Price
//
//  Created by Maxime Pontoire on 18/09/12.
//  Copyright (c) 2012 Maxime Pontoire. All rights reserved.
//

#import "AudioController.h"

@implementation AudioController

-(void)playSound:(NSString *)filename extension:(NSString *)extension {
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain([NSURL fileURLWithPath: soundPath]), &soundID);
    AudioServicesPlaySystemSound (soundID);
}

@end
