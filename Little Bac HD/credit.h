//
//  credit.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 02/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface credit : NSObject {
    NSString *nom;
    NSString *detail;
    NSString *image;
}
@property (nonatomic, retain) NSString *nom;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *image;

@end
