//
//  categorie.h
//  Captain Price
//
//  Created by Maxime Pontoire on 20/10/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface categorie : NSObject {
    NSString *id;
    NSString *nom;
    NSString *table;
}
@property (nonatomic, retain) NSString *id;
@property (nonatomic, retain) NSString *nom;
@property (nonatomic, retain) NSString *table;

@end
