//
//  TrousseViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 14/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReglagesView.h"
#import "StoreViewController.h"
#import "ApiUtils.h"
#import "CustomAlert.h"
#import "StatistiquesRequest.h"

@protocol TrousseViewControllerDelegate;
@interface TrousseViewController : UIViewController <UIGestureRecognizerDelegate, StoreViewControllerDelegate, UIAlertViewDelegate> {
    id<TrousseViewControllerDelegate>__unsafe_unretained delegate;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    StoreViewController *storeView;
    IBOutlet UILabel *titleText;
    IBOutlet UILabel *troussePrix;
    IBOutlet UILabel *choisirUn;
    IBOutlet UILabel *choisirDeux;
    IBOutlet UILabel *choisirTrois;
    IBOutlet UILabel *detailCorrection;
    IBOutlet UILabel *detailAntiSeche;
    IBOutlet UILabel *detailHorloge;
    IBOutlet UILabel *littlesTotal;
    IBOutlet UILabel *correctionCounter;
    IBOutlet UIImageView *correctionImage;
    IBOutlet UILabel *suivantCounter;
    IBOutlet UIImageView *suivantImage;
    IBOutlet UILabel *tempsCounter;
    IBOutlet UIImageView *tempsImage;
    int littlesRestant;
    ApiUtils *utils;
    CustomAlert *alert;
    IBOutlet UIImageView *fondSombre;
    IBOutlet UIView *correctionView;
    IBOutlet UIView *antiSecheView;
    IBOutlet UIView *horlogeView;
    IBOutlet UILabel *milleCorrectionLabel;
    IBOutlet UILabel *deuxMilleCorrectionLabel;
    IBOutlet UILabel *troisMilleCorrectionLabel;
    IBOutlet UILabel *milleAntiSecheLabel;
    IBOutlet UILabel *deuxMilleAntiSecheLabel;
    IBOutlet UILabel *troisMilleAntiSecheLabel;
    IBOutlet UILabel *milleTempsLabel;
    IBOutlet UILabel *deuxMilleTempsLabel;
    IBOutlet UILabel *troisMilleTempsLabel;
    IBOutlet UILabel *packTenCorrection;
    IBOutlet UILabel *packTwentyCorrection;
    IBOutlet UILabel *packTwentyFiveCorrection;
    IBOutlet UILabel *packTenAntiseche;
    IBOutlet UILabel *packTwentyAntiseche;
    IBOutlet UILabel *packTwentyFiveAntiseche;
    IBOutlet UILabel *packTenTemps;
    IBOutlet UILabel *packTwentyTemps;
    IBOutlet UILabel *packTwentyFiveTemps;
    IBOutlet UILabel *deconnexionReglagesLabel;
}

@property (nonatomic, unsafe_unretained) id<TrousseViewControllerDelegate>delegate;
- (IBAction)troussePressed:(id)sender;
- (IBAction)correctionPressed:(id)sender;
- (IBAction)antisechePressed:(id)sender;
- (IBAction)horlogePressed:(id)sender;

- (IBAction)storePressed:(id)sender;

- (IBAction)retourPressed:(id)sender;

//CorrectionView
- (IBAction)tenCorrectionPressed:(id)sender;
- (IBAction)twentyCorrectionPressed:(id)sender;
- (IBAction)twentyFiveCorrectionPressed:(id)sender;
- (IBAction)outCorrectionPressed:(id)sender;

//AntiSeche
- (IBAction)tenAntiSechePressed:(id)sender;
- (IBAction)twentyAntiSechePressed:(id)sender;
- (IBAction)twentyFiveAntiSechePressed:(id)sender;
- (IBAction)outAntiSechePressed:(id)sender;

// Temps
- (IBAction)tenTempsPressed:(id)sender;
- (IBAction)twentyTempsPressed:(id)sender;
- (IBAction)twentyFiveTempsPressed:(id)sender;
- (IBAction)outTempsPressed:(id)sender;


// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

@end

@protocol TrousseViewControllerDelegate
-(void)retourTroussePressed;
@required
@end