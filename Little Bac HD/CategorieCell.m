//
//  CategorieCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 05/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "CategorieCell.h"

@implementation CategorieCell
@synthesize categorieLabel, tamponImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
