//
//  ProfilViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 04/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "ProfilViewController.h"

@interface ProfilViewController ()

@end

@implementation ProfilViewController
@synthesize delegate, currentImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [bonjourLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [deconnexionLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [loginLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [littleLabel setFont:[UIFont fontWithName:@"Sansita One" size:30]];
    [tempsLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [plusUnLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [motLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [storeLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [invitationLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [totalLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [totalScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [meilleurScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [meilleurScoreScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [meilleurSoloLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [meilleurSoloScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [meilleurDuoLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [meilleurDuoScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [meilleurSerieLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [meilleurSerieScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [scoreMoyenLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [scoreMoyenScoreLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    littleLabel.adjustsFontSizeToFitWidth = YES;
    tempsLabel.adjustsFontSizeToFitWidth = YES;
    plusUnLabel.adjustsFontSizeToFitWidth = YES;
    motLabel.adjustsFontSizeToFitWidth = YES;
    totalScoreLabel.adjustsFontSizeToFitWidth = YES;
    meilleurScoreScoreLabel.adjustsFontSizeToFitWidth = YES;
    meilleurDuoScoreLabel.adjustsFontSizeToFitWidth = YES;
    meilleurSoloScoreLabel.adjustsFontSizeToFitWidth = YES;
    meilleurSerieScoreLabel.adjustsFontSizeToFitWidth = YES;
    scoreMoyenScoreLabel.adjustsFontSizeToFitWidth = YES;
    
    [imageProfile.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [imageProfile.layer setBorderWidth: 2.0f];
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Profil Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Profil Affiche"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    // Load Profile
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *profileName = [defaults stringForKey:nameProfil];
    if (profileName == nil || [profileName isEqualToString:@""]) {
        [totalScoreLabel setText:@"0"];
        [meilleurScoreScoreLabel setText:@"0"];
        [meilleurSoloScoreLabel setText:@"0"];
        [meilleurDuoScoreLabel setText:@"0"];
        [meilleurSerieScoreLabel setText:@"0"];
        [scoreMoyenScoreLabel setText:@"0"];
        [loginLabel setText:@""];
        [littleLabel setText:@"-0-"];
        [littleLabel setText:[NSString stringWithFormat:@"- %i -", [self checkValue:[defaults stringForKey:littlesCurrent]]]];
        [motLabel setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:nextBonusCurrent]]]];
        [tempsLabel setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:tempsBonusCurrent]]]];
        [plusUnLabel setText:[NSString stringWithFormat:@"x%i", [self checkValue:[defaults stringForKey:checkBonusCurrent]]]];
        alert = [[CustomAlert alloc] initWithTitle:@"Rejoins-moi !" message:@"Il faut te connecter pour voir tes informations !"
                                          delegate:self
                                 cancelButtonTitle:@"Plus tard"
                                 otherButtonTitles:@"Se connecter",nil];
        [alert show];
        alert = nil;
    } else {
        [loginLabel setText:[defaults stringForKey:nameProfil]];
        if (![[defaults stringForKey:picProfil] isEqualToString:@""] && [defaults stringForKey:nameProfil] != nil) {
            [loadingIndicator startAnimating];
            [self.view addSubview:loadingIndicator];
            if (currentImage == nil) {
                dispatch_async(dispatch_get_global_queue(0,0), ^{
                    NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[defaults stringForKey:picProfil]]];
                    if ( data == nil ) {
                        return;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(127.0f, 127.0f)];
                        [imageProfile setImage:newImage];
                        currentImage = newImage;
                        [loadingIndicator removeFromSuperview];
                    });
                });
            } else {
                [imageProfile setImage:currentImage];
                [loadingIndicator removeFromSuperview];
            }
        }
        [self loadProfile];
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        [littleLabel setText:[NSString stringWithFormat:@"- %@ -",[utils getLittlesRestant]]];
        [plusUnLabel setText:[NSString stringWithFormat:@"x%@", [utils getCorrectionRestant]]];
        [tempsLabel setText:[NSString stringWithFormat:@"x%@", [utils getTempsRestant]]];
        [motLabel setText:[NSString stringWithFormat:@"x%@", [utils getAntiSecheRestant]]];
        progression *progression = [utils getProgression];
        if (progression == nil || progression.utilisateur == nil) {
            [totalScoreLabel setText:@"0"];
            [meilleurScoreScoreLabel setText:@"0"];
            [meilleurSoloScoreLabel setText:@"0"];
            [meilleurDuoScoreLabel setText:@"0"];
            [meilleurSerieScoreLabel setText:@"0"];
            [scoreMoyenScoreLabel setText:@"0"];
        } else {
            [totalScoreLabel setText:[NSString stringWithFormat:@"%@", progression.totalPartiesJoues]];
            [meilleurScoreScoreLabel setText:[NSString stringWithFormat:@"%@", progression.meilleurScore]];
            [meilleurSoloScoreLabel setText:[NSString stringWithFormat:@"%@", progression.meilleurScoreSolo]];
            [meilleurDuoScoreLabel setText:[NSString stringWithFormat:@"%@", progression.meilleurScoreDuo]];
            [meilleurSerieScoreLabel setText:[NSString stringWithFormat:@"%@", progression.meilleureSerie]];
            [scoreMoyenScoreLabel setText:[NSString stringWithFormat:@"%@", progression.scoreMoyen]];
        }
    }
    [self computeBonusRestant];
}

-(int)checkValue:(NSString *)value {
    if (value && ![value isEqualToString:@""] && ![value isEqualToString:@"(null)"]) {
        return [value intValue];
    } else {
        return 0;
    }
}

-(void)computeBonusRestant {
    int counterVal = [[[plusUnLabel text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (counterVal <= 0) {
        [plusUnButton setImage:[UIImage imageNamed:@"bonus_correction_plein.png"] forState:UIControlStateNormal];
    } else {
        [plusUnButton setImage:[UIImage imageNamed:@"bonus_correction_uo.png"] forState:UIControlStateNormal];
    }
    int antiVal = [[[motLabel text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (antiVal <= 0) {
        [suivantButton setImage:[UIImage imageNamed:@"mot.png"] forState:UIControlStateNormal];
    } else {
        [suivantButton setImage:[UIImage imageNamed:@"bonus_mot_up.png"] forState:UIControlStateNormal];
    }
    int tempsVal = [[[tempsLabel text] stringByReplacingOccurrencesOfString:@"x" withString:@""] intValue];
    if (tempsVal <= 0) {
        [tempsButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    } else {
        [tempsButton setImage:[UIImage imageNamed:@"bonus_pause_up.png"] forState:UIControlStateNormal];
    }
    
}

-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}

- (IBAction)deconnexionPressed:(id)sender {
    [self loadProfile];
    [delegate deconnexionPressed];
}

- (IBAction)retourPressed:(id)sender {
    [delegate retourPressed];
}

- (IBAction)storePressed:(id)sender {
    if (!trousseView) {
        trousseView = [[TrousseViewController alloc]initWithNibName:@"TrousseViewController" bundle:nil];
        [trousseView setDelegate:self];
    }
    [trousseView.view setFrame:CGRectMake(0, 0, trousseView.view.frame.size.width, trousseView.view.frame.size.height)];
    [self.view addSubview:trousseView.view];
}

- (IBAction)postFB:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self sendFBWithText:[NSString stringWithFormat:@"Viens me défier sur Little Bac. %@ t'attend à ses côtés pour réviser avec lui : http://bit.ly/WQBorI", [defaults stringForKey:nameProfil]] andImage:[UIImage imageNamed:@"icone.png"] showSheet:YES];
}


-(void)sendFBWithText:(NSString *)text andImage:(UIImage *)image showSheet:(BOOL)sheet
{
    if (FBSession.activeSession.isOpen) {
        DEFacebookComposeViewControllerCompletionHandler completionHandler = ^(DEFacebookComposeViewControllerResult result) {
            switch (result) {
                case DEFacebookComposeViewControllerResultCancelled:
                    NSLog(@"Facebook Result: Cancelled");
                    break;
                case DEFacebookComposeViewControllerResultDone:
                    NSLog(@"Facebook Result: Sent");
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
        DEFacebookComposeViewController *facebookViewComposer = [[DEFacebookComposeViewController alloc] init];
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [facebookViewComposer setInitialText:text];
        [facebookViewComposer addImage:image];
        facebookViewComposer.completionHandler = completionHandler;
        [self presentViewController:facebookViewComposer animated:YES completion:^{
            [self addLittles];
        }];
    } else {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        if ([appDelegate openSessionWithAllowLoginUI:YES]) {
            [self sendFBWithText:text andImage:image showSheet:YES];
        }
    }
}


- (IBAction)postMail:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self sendMailWithText:[NSString stringWithFormat:@"Viens me défier sur Little Bac. %@ t'attend à ses côtés pour réviser avec lui : http://bit.ly/WQBorI", [defaults stringForKey:nameProfil]]];
}


-(void)sendMailWithText:(NSString *)text {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"Rejoins moi sur Little Bac"];
        NSArray *toRecipients = [NSArray arrayWithObjects:nil];
        [mailer setToRecipients:toRecipients];
        NSString *emailBody = text;
        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentViewController:mailer animated:YES completion:nil];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Votre téléphone ne supporte pas l'envoi de mail"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            [self addLittles];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)addLittles {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    resultat *res2 = [utils addLittles:@"10"];
    if (res2 == nil){
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Le service semble indisponible. Êtes-vous sûr d'être bien connecté à internet ?"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else if ([res2.message isEqualToString:@"KO"]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Enregistrement indisponible"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else {
        [littleLabel setText:[NSString stringWithFormat:@"- %@ -",[utils getLittlesRestant]]];
    }
}

-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}

- (IBAction)deconnexionPressedButton:(id)sender {
    [self hideReglages];
    [delegate disconnectReglagesPressed];
}

#pragma mark TrousseViewControllerDelegate
-(void)retourTroussePressed {
    if (trousseView) {
        [trousseView.view removeFromSuperview];
        trousseView = nil;
        [trousseView.view setFrame:CGRectMake(0, -1500, trousseView.view.frame.size.width, trousseView.view.frame.size.height)];
    }
}
-(void)disconnectStoreReglagesPressed {
    [self retourPressed:nil];
}

#pragma mark UIalertFirstViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = alertView.title;
    if ([title isEqualToString:@"Rejoins-moi !"] && buttonIndex == 1) {
        if (!connexionView) {
            connexionView = [[ConnexionViewController alloc]initWithNibName:@"ConnexionViewController" bundle:nil];
            [connexionView setDelegate:self];
        }
        [connexionView.view setFrame:CGRectMake(0, 0, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
        [self.view addSubview:connexionView.view];
    }
}


#pragma mark ConnexionViewControllerDelegate
-(void)validatePressedDetail:(BOOL)nouveau {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [self viewWillAppear:NO];
    [Flurry logEvent:@"Accueil Affiche Connexion validee"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche Connexion validee"];
}

-(void)retourConnexionPressed {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [self viewWillAppear:NO];
    [Flurry logEvent:@"Accueil Affiche Connexion retour"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche Connexion retour"];
}

-(void)disconnectConnexionReglagesPressed {
    [self retourConnexionPressed];
    [self disconnectStoreReglagesPressed];
    [Flurry logEvent:@"Accueil Affiche deconnexion "];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Accueil Affiche deconnexion"];
}
@end
