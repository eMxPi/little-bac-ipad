//
//  antiSeche.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 14/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface antiSeche : NSObject {
    NSString *restant;
    NSString *utilisateur;
}

@property (nonatomic, retain) NSString *restant;
@property (nonatomic, retain) NSString *utilisateur;

@end
