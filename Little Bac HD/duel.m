//
//  duel.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 17/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "duel.h"

@implementation duel
@synthesize id, joueurUn, joueurDeux, imageJoueurUn, imageJoueurDeux, StatutJoueurUn, StatutJoueurDeux, categories, lettre;
@synthesize vuDuelUn, vuDuelDeux, vuCorrUn, vuCorrDeux;

@end
