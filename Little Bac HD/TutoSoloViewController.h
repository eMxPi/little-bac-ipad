//
//  TutoSoloViewController.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 08/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReglagesView.h"
#import "UIView+JTRemoveAnimated.h"
#import "AppDelegate.h"
#import "duel.h"
#import "ApiUtils.h"
#import "StatistiquesRequest.h"
#import "CustomAlert.h"

@protocol TutoSoloViewControllerDelegate;
@interface TutoSoloViewController : UIViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate> {
    id<TutoSoloViewControllerDelegate>__unsafe_unretained delegate;
    BOOL isEffet;
    BOOL isMusique;
    BOOL isShare;
    IBOutlet ReglagesView *reglagesView;
    IBOutlet UIImageView *imageReglages;
    IBOutlet UIImageView *imageReglagesDeplie;
    IBOutlet UIButton *effetButton;
    IBOutlet UILabel *effetLabel;
    IBOutlet UIButton *musiqueButton;
    IBOutlet UILabel *musiqueLabel;
    IBOutlet UIButton *shareButton;
    IBOutlet UILabel *shareLabel;
    IBOutlet UIImageView *lettreImage;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIView *lettreView;
    IBOutlet UILabel *deconnexionReglagesLabel;
    int secondes;
    int remainingSeconds;
    BOOL tempEcoule;
    NSTimer *bonusTimer;
    IBOutlet UIButton *validateButton;
    NSString *imageToPlay;
    IBOutlet UIScrollView *animationScroll;
    int offset;
    NSString *lettreUrl;
    IBOutlet UIButton *shuffleButton;
    ApiUtils *utils;
    CustomAlert *alert;
    IBOutlet UIButton *retourButton;
}
@property (nonatomic, unsafe_unretained) id<TutoSoloViewControllerDelegate>delegate;
@property (nonatomic) BOOL isDefi;
@property (nonatomic, retain) duel *duel;
@property (nonatomic, retain) IBOutlet UIImageView *lettreImage;
@property (nonatomic, retain) NSString *lettre;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationUn;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationDeux;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationTrois;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationQuatre;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationConq;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationSix;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationSept;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationHuit;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationNeuf;
@property (strong, nonatomic) IBOutlet UIImageView *lettreAnimationDix;

- (IBAction)validatePressed:(id)sender;
- (IBAction)retourPressed:(id)sender;
- (IBAction)shufflePressed:(id)sender;

// Reglages
- (IBAction)activeEffet:(id)sender;
- (IBAction)activeMusique:(id)sender;
- (IBAction)activeShare:(id)sender;
- (IBAction)deconnexionPressedButton:(id)sender;

@end


@protocol TutoSoloViewControllerDelegate
-(void)validateTutoSoloPressed:(duel *)duel isDefi:(BOOL)isDefi imageName:(NSString*)imageName;
-(void)retourTutoSoloPressed;
-(void)replayAnimation;
@required
@end