//
//  utilisateur.h
//  Captain Price
//
//  Created by Maxime Pontoire on 26/11/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface utilisateur : NSObject{
    NSString *id;
    NSString *nom;
    NSString *prenom;
    NSString *twitter;
    NSString *facebook;
    NSString *motDePasse;
    NSString *mail;
    NSString *urlImage;
}

@property (nonatomic, retain) NSString *id;
@property (nonatomic, retain) NSString *nom;
@property (nonatomic, retain) NSString *prenom;
@property (nonatomic, retain) NSString *twitter;
@property (nonatomic, retain) NSString *facebook;
@property (nonatomic, retain) NSString *motDePasse;
@property (nonatomic, retain) NSString *mail;
@property (nonatomic, retain) NSString *urlImage;
@end
