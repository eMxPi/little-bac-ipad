//
//  XMLStringParser.h
//  Captain Price
//
//  Created by Maxime Pontoire on 17/10/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface XMLStringParser : NSObject <NSXMLParserDelegate> {
    NSString *className;
	NSMutableArray *items;
	NSObject *item; // stands for any class
	NSString *currentNodeName;
	NSMutableString *currentNodeContent;
}

- (NSArray *)items;
- (id)parseXMLString:(NSData *)data
		   toObject:(NSString *)aClassName
		 parseError:(NSError **)error;
- (id)parseXMLAtFile:(NSString *)file
            toObject:(NSString *)aClassName
          parseError:(NSError **)error;
@end
