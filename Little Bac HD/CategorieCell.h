//
//  CategorieCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 05/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorieCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *categorieLabel;
@property (strong, nonatomic) IBOutlet UIImageView *tamponImage;

@end
