//
//  reponse.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 22/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface reponse : NSObject {
    
}
@property (nonatomic, retain) NSString *reponseJoueurUn;
@property (nonatomic, retain) NSString *reponseJoueurDeux;
@property (nonatomic, retain) NSString *scoreJoueurUn;
@property (nonatomic, retain) NSString *scoreJoueurDeux;
@property (nonatomic, retain) NSString *categorie;
@property (nonatomic, retain) NSString *message;

@end
