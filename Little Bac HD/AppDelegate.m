//
//  AppDelegate.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 03/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize dToken;
@synthesize facebook = _facebook;
@synthesize isNotif;
NSString *const FBSessionStateChangedNotification = @"com.quetzal.Login:FBSessionStateChangedNotification";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // IN APP
    [RageIAPHelper sharedInstance];
    
    // Register for push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    [Flurry startSession:@"5VDKK4FXHJ7GF5FX5QMS"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSession.activeSession handleDidBecomeActive];
    // Sound
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    //FaceBook
    [FBSession.activeSession close];
}



#pragma mark FaceBook Stuff
/*
 * Callback for session changes.
 */
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // We have a valid session
                
                // Initiate a Facebook instance
                self.facebook = [[Facebook alloc]
                                 initWithAppId:FBSession.activeSession.appID
                                 andDelegate:nil];
                
                // Store the Facebook session information
                self.facebook.accessToken = FBSession.activeSession.accessToken;
                self.facebook.expirationDate = FBSession.activeSession.expirationDate;
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            // Clear out the Facebook instance
            self.facebook = nil;
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:FBSessionStateChangedNotification
     object:session];
    
    if (error) {
        alert = [[CustomAlert alloc] initWithTitle:@"Alerte" message:@"Erreur lors de la connexion à Facebook"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

/*
 * Opens a Facebook session and optionally shows the login UX.
 */
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    
    // Get the email
    NSArray *permissions = [NSArray arrayWithObjects:@"email",@"read_friendlists", nil];
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
}

/*
 * If we have a valid session at the time of openURL call, we handle
 * Facebook transitions by passing the url argument to handleOpenURL
 */
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}

- (void) closeSession {
    [FBSession.activeSession closeAndClearTokenInformation];
}

#pragma mark notifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *deviceTokenParsed = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceTokenParsed = [deviceTokenParsed stringByReplacingOccurrencesOfString:@" " withString:@""];
    [userDefault setValue:deviceTokenParsed forKey:deviceTokenKey];
    dToken = deviceTokenParsed;
    
    NSLog(@"Sortie register avec token : %@", dToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
	NSString* alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    if ([alertValue rangeOfString:@"vient de "].location != NSNotFound) {
        isNotif = TRUE;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL isGame = [defaults boolForKey:isGameRunning];
        if (!isGame) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil];
            int counter = [UIApplication sharedApplication].applicationIconBadgeNumber;
            if (counter > 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshAll" object:nil];
            } else {
                if ([alertValue rangeOfString:@"défier"].location != NSNotFound) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshInvitations" object:nil];
                } else if ([alertValue rangeOfString:@"terminer"].location != NSNotFound) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshDuels" object:nil];
                } else if ([alertValue rangeOfString:@"correction"].location != NSNotFound) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshDernier" object:nil];
                } else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshAll" object:nil];
                }
            }
        } else {
            alert = [[CustomAlert alloc] initWithTitle:@"Alerte" message:alertValue
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        }
    } else {
        isNotif = FALSE;
        
        // Parse
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *profileName = [defaults stringForKey:nameProfil];
        if (profileName && ![profileName isEqualToString:@""]&& ![profileName isEqualToString:@"(null)"]) {
            if (utils == nil) {
                utils = [[ApiUtils alloc] init];
            }
            [utils resetBadge];
        }
        alert = [[CustomAlert alloc] initWithTitle:@"Alerte" message:alertValue
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    return UIInterfaceOrientationMaskAll;
}
@end
