//
//  CreditCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 02/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "CreditCell.h"

@implementation CreditCell
@synthesize titre, detail, image;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
