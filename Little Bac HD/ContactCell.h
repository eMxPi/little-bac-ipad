//
//  ContactCell.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 02/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *appName;
@property (strong, nonatomic) IBOutlet UILabel *appDetail;

- (IBAction)fbPressed:(id)sender;
- (IBAction)twPressed:(id)sender;

@end
