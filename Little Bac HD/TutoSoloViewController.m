//
//  TutoSoloViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 08/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "TutoSoloViewController.h"

@interface TutoSoloViewController ()

@end

@implementation TutoSoloViewController
@synthesize delegate, isDefi, duel, lettreImage, lettre;
@synthesize lettreAnimationUn, lettreAnimationDeux, lettreAnimationTrois, lettreAnimationQuatre, lettreAnimationConq, lettreAnimationSix, lettreAnimationSept, lettreAnimationHuit, lettreAnimationNeuf, lettreAnimationDix;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [titleLabel setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Tuto Jeu Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Tuto Jeu Affiche"];
}


-(void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:TRUE forKey:isGameRunning];
    [self loadProfile];
    secondes = 10;
    [self startTimer];
    offset = 0;
    NSString *name = [NSString stringWithFormat:@"%@.png", lettre];
    lettreUrl = name;
    [lettreImage setImage:[UIImage imageNamed:name]];
    imageToPlay = name;
    [shuffleButton setHidden:TRUE];
    if (isDefi) {
        [retourButton setHidden:TRUE];
    } else {
        [retourButton setHidden:FALSE];
    }
}

-(void)loadAnimation {
    [delegate replayAnimation];
}


- (void)startTimer {
    
    // Lance le minuteur
    BOOL started = [self beginTimer:secondes];
    if (!started ) {
        [self stopTimer];
    }
}

- (BOOL)beginTimer:(int)numberOfSeconds {
    BOOL started = NO; // Le chronomètre n'est pas lancé
    remainingSeconds = numberOfSeconds;
    
    if ( remainingSeconds > 0 ) {
        
        // Crée le chronomètre avec une méthode de construction
        // Chaque seconde le message updateTimer: sera reçu par l'objet
        bonusTimer = [NSTimer scheduledTimerWithTimeInterval:0.3
                                                      target:self
                                                    selector:@selector(updateTimer:)
                                                    userInfo:nil
                                                     repeats:YES];
        started = YES; // Le chronomètre est lancé
    }
    
    return started;
}

- (void)stopTimer {
    // Arrête le minuteur
    [self endTimer];
    
}

- (void) endTimer {
    if ( nil != bonusTimer ) {
        [bonusTimer invalidate];
        bonusTimer = nil;
    }
}

- (void) updateTimer: (NSTimer *) aTimer
{
    --remainingSeconds;
    //[self addSubviewWithFadeAnimation:lettreView duration:0.3 option:curveValues[1]];
    float contentScroll = offset++ * 175;
    [animationScroll setContentOffset:CGPointMake(contentScroll, 0.0f) animated:YES];
    
    // Arrète le NSTimer lorsque le compte à rebour est à zéro
    if ( remainingSeconds <= 0 ) {
        // PLay SOUND
        tempEcoule = TRUE;
        [self stopTimer];
        [animationScroll setHidden:TRUE];
        [lettreImage setHidden:FALSE];
        [validateButton setHidden:FALSE];
        [shuffleButton setHidden:isDefi];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}


- (IBAction)deconnexionPressedButton:(id)sender {
    [self hideReglages];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [self loadProfile];
    [self retourPressed:nil];
}

-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}

- (void)addSubviewWithFadeAnimation:(UIView*)view duration:(float)secs option:(UIViewAnimationOptions)option
{
	view.alpha = 0.0;
	[view setHidden:FALSE];// make the view transparent
	//[self.view addSubview:view];
    [UIView setAnimationDidStopSelector:@selector(displayBack)];// add it
	[UIView animateWithDuration:secs delay:0.0 options:option
                     animations:^{view.alpha = 1.0;}
                     completion:nil];
}

- (IBAction)validatePressed:(id)sender {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    if ([utils canReach]) {
        [delegate validateTutoSoloPressed:duel isDefi:isDefi imageName:lettreUrl];
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Cours de techno !" message:@"Tu ne captes pas ? Trouves du réseau et reviens au plus vite en salle de classe !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}
- (IBAction)retourPressed:(id)sender {
    [delegate retourTutoSoloPressed];
}

- (IBAction)shufflePressed:(id)sender {
    [self loadAnimation];
}

-(NSString *)generateImage {
    int random = [self getRandomNumber:1 to:25];
    NSString *lettres = @"ABCDEFGHIJKLMOPNQRSTUVWXYZ";
    NSString *lettreGen = [lettres substringWithRange:NSMakeRange(random, 1)];
    return lettreGen;
}

-(int)getRandomNumber:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

@end
