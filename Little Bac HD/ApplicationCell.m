//
//  ApplicationCell.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 28/02/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "ApplicationCell.h"

@implementation ApplicationCell
@synthesize appImage, appDetail, appName, appUrl, appStoreButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)appstorePressed:(id)sender {
    if (appUrl) {
        alert = [[CustomAlert alloc] initWithTitle:@"Tu nous quittes ?" message:@"Tu vas quitter Little Bac pour te rendre sur l'AppStore"
                                          delegate:self
                                 cancelButtonTitle:@"Annuler"
                                 otherButtonTitles:@"AppStore",nil];
        [alert show];
        alert = nil;
    }
}


#pragma mark UIalertFirstViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = alertView.title;
    if ([title isEqualToString:@"Tu nous quittes ?"] && buttonIndex == 1) {
        if (appUrl) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appUrl]];
        }
    }
}
@end
