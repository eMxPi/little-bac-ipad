//
//  StoreViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 14/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "StoreViewController.h"

@interface StoreViewController () {
    NSArray *_products;
}

@end

@implementation StoreViewController
@synthesize delegate;
@synthesize postParams = _postParams;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [storeTitre setFont:[UIFont fontWithName:@"Kameron-Bold" size:25]];
    [storeSousTitre setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [prixTitre setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixTitreUn setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixTitreTrois setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixTitreQuatre setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixStoreMille setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixStoreDeuxMille setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixStoreHuitMille setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    [prixStoreVingtMille setFont:[UIFont fontWithName:@"Kameron-Bold" size:16]];
    // Do any additional setup after loading the view from its nib.
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    
    [self reload];
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Store Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Store Affiche"];
}


- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            NSLog(@"Produit acheté : %@", product.productIdentifier);
            currentAchat = product.productIdentifier;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            int littles = [[defaults stringForKey:littlesCurrent] intValue];
            if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.mille"]) {
                littles += 1000;
            } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.deux"]) {
                littles += 2000;
            } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.huit"]) {
                littles += 8000;
            } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.vingt"]) {
                littles += 20000;
            }
            [defaults setValue:[NSString stringWithFormat:@"%i", littles] forKey:littlesCurrent];
            if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
                if (utils == nil) {
                    utils = [[ApiUtils alloc] init];
                }
                if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.mille"]) {
                    [utils addLittles:@"1000"];
                } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.deux"]) {
                    [utils addLittles:@"2000"];
                } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.huit"]) {
                    [utils addLittles:@"8000"];
                } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.vingt"]) {
                    [utils addLittles:@"20000"];
                }
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                BOOL isShareEnable = [defaults boolForKey:isShareActive];
                BOOL isFacebook = [defaults boolForKey:isFacebookActive];
                if (isShareEnable) {
                    if (isFacebook) {
                        [self sendFBWithText:@"Je viens d'acquérir un des bonus de Little Bac, attention, me voilà ! http://bit.ly/WQBorI" andImage:nil showSheet:NO];
                    }
                }
                alert = [[CustomAlert alloc] initWithTitle:@"Félicitations !" message:@"Le jury regarde ailleurs quand tu utilises un bonus ! Profites-en !"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
                [alert show];
                alert = nil;
                *stop = YES;
            } else {
                alert = [[CustomAlert alloc] initWithTitle:@"Rejoins-moi !" message:@"Tu peux dès à présent utiliser tes bonus. Je te conseille de te connecter si tu veux les conserver !"
                                                  delegate:self
                                         cancelButtonTitle:@"Plus tard"
                                         otherButtonTitles:@"Se connecter",nil];
                [alert show];
                alert = nil;
            }
        }
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    if (![self isNetworkEnabled]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Cours de techno !" message:@"Tu ne captes pas ? Trouves du réseau et reviens au plus vite en salle de classe !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else if (![self isAPIReachable]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Prof absent !" message:@"Les cours ne sont plus assurés pour le moment ! Reviens nous voir après la récré !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
        
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    }
    [self loadProfile];
    loaded = FALSE;
    currentAchat = @"";
}


-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)deconnexionPressedButton:(id)sender {
    [self hideReglages];
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    [utils removeToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"" forKey:nameProfil];
    [defaults setValue:@"" forKey:emailProfil];
    [defaults setValue:@"" forKey:motDePasseProfil];
    [defaults setValue:@"" forKey:picProfil];
    [defaults setBool:TRUE forKey:isEffetActive];
    [defaults setBool:TRUE forKey:isMusiqueActive];
    [defaults setBool:TRUE forKey:isShareActive];
    [defaults setBool:TRUE forKey:isNotifActive];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [self loadProfile];
    [self retourPressed:nil];
}

-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}

- (IBAction)retourPressed:(id)sender {
    [delegate retourStorePressed];
}


- (void)reload {
    _products = nil;
    if ([self isNetworkEnabled] || ![self isAPIReachable]) {
        [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                _products = products;
                [self loadPrices];
            }
        }];
    }
}


-(void)loadPrices {
    for (int i=0; i < [_products count]; i++) {
        SKProduct *product = _products[i];
        [_priceFormatter setLocale:product.priceLocale];
        if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.mille"]) {
            [prixStoreMille setText:[_priceFormatter stringFromNumber:product.price]];
        } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.deux"]) {
            [prixStoreDeuxMille setText:[_priceFormatter stringFromNumber:product.price]];
        } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.huit"]) {
            [prixStoreHuitMille setText:[_priceFormatter stringFromNumber:product.price]];
        } else if ([product.productIdentifier isEqualToString:@"com.quetzalgames.little.bac.ipad.vingt"]) {
            [prixStoreVingtMille setText:[_priceFormatter stringFromNumber:product.price]];
        }
    }
    loaded = TRUE;
}

- (IBAction)buyPressed:(id)sender {
    if (loaded) {
        if (![self isNetworkEnabled]) {
            alert = [[CustomAlert alloc] initWithTitle:@"Cours de techno !" message:@"Tu ne captes pas ? Trouves du réseau et reviens au plus vite en salle de classe !"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        } else if (![self isAPIReachable]) {
            alert = [[CustomAlert alloc] initWithTitle:@"Prof absent !" message:@"Les cours ne sont plus assurés pour le moment ! Reviens nous voir après la récré !"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
            
        } else {
            UIButton *buyButton = (UIButton *)sender;
            SKProduct *product = _products[buyButton.tag];
            
            NSLog(@"Buying %@...", product.productIdentifier);
            [[RageIAPHelper sharedInstance] buyProduct:product];
        }
    }
}

- (IBAction)restaureAchatPressed:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults stringForKey:nameProfil] isEqualToString:@""] && ![[defaults stringForKey:nameProfil] isEqualToString:@"(null)"] && [defaults stringForKey:nameProfil] != nil) {
        alert = [[CustomAlert alloc] initWithTitle:@"Te revoilà !" message:@"Tes achats ont été restaurés. Bon retour dans l'aventure !"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else {
        currentAchat = @"";
        alert = [[CustomAlert alloc] initWithTitle:@"Rejoins-moi !" message:@"Il faut te connecter pour restaurer des bonus !"
                                          delegate:self
                                 cancelButtonTitle:@"Plus tard"
                                 otherButtonTitles:@"Se connecter",nil];
        [alert show];
        alert = nil;
    }
}

-(void)savePurchase{
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    if ([currentAchat isEqualToString:@"com.quetzalgames.little.bac.ipad.mille"]) {
        [utils addLittles:@"1000"];
    } else if ([currentAchat isEqualToString:@"com.quetzalgames.little.bac.ipad.deux"]) {
        [utils addLittles:@"2000"];
    } else if ([currentAchat isEqualToString:@"com.quetzalgames.little.bac.ipad.huit"]) {
        [utils addLittles:@"8000"];
    } else if ([currentAchat isEqualToString:@"com.quetzalgames.little.bac.ipad.vingt"]) {
        [utils addLittles:@"20000"];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isShareEnable = [defaults boolForKey:isShareActive];
    BOOL isFacebook = [defaults boolForKey:isFacebookActive];
    if (isShareEnable) {
        if (isFacebook) {
            [self sendFBWithText:@"Je viens d'acquérir un des bonus de Little Bac, attention, me voilà ! http://bit.ly/WQBorI" andImage:nil showSheet:NO];
        }
    }
    alert = [[CustomAlert alloc] initWithTitle:@"Félicitations !" message:@"Le jury regarde ailleurs quand tu utilises un bonus ! Profites-en !"
                                      delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil,nil];
    [alert show];
    alert = nil;
    
}

#pragma mark UIalertFirstViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = alertView.title;
    if ([title isEqualToString:@"Rejoins-moi !"] && buttonIndex == 1) {
        [self displayConnexion];
    }
}

-(void)displayConnexion {
    if (!connexionView) {
        connexionView = [[ConnexionViewController alloc]initWithNibName:@"ConnexionViewController" bundle:nil];
        [connexionView setDelegate:self];
    }
    [connexionView.view setFrame:CGRectMake(0, 0, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    [self.view addSubview:connexionView.view];
    [storeSubView setHidden:TRUE];
    [fondLittles setHidden:TRUE];
}


#pragma mark ConnexionViewControllerDelegate
-(void)validatePressedDetail:(BOOL)nouveau {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [storeSubView setHidden:FALSE];
    [fondLittles setHidden:FALSE];
    if (![currentAchat isEqualToString:@""]) {
        [self savePurchase];
    }
}

-(void)retourConnexionPressed {
    if (connexionView) {
        [connexionView.view removeFromSuperview];
        connexionView = nil;
        [connexionView.view setFrame:CGRectMake(0, -1500, connexionView.view.frame.size.width, connexionView.view.frame.size.height)];
    }
    [storeSubView setHidden:FALSE];
    [fondLittles setHidden:FALSE];
}
-(void)disconnectConnexionReglagesPressed {
    [self retourConnexionPressed];
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}

-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}


- (BOOL)isNetworkEnabled
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return (networkStatus != NotReachable);
}


-(BOOL)isAPIReachable {
    struct sockaddr_in address;
    address.sin_len = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_port = htons(8080);
    address.sin_addr.s_addr = inet_addr(apiHost);
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    Reachability *reach2 = [Reachability reachabilityWithAddress:&address];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    NetworkStatus net2 = [reach2 currentReachabilityStatus];
    return (networkStatus != NotReachable && net2 != NotReachable);
    
}

-(void)sendFBWithText:(NSString *)text andImage:(UIImage *)image showSheet:(BOOL)sheet
{
    if (FBSession.activeSession.isOpen) {
        self.postParams =
        [[NSMutableDictionary alloc] initWithObjectsAndKeys:
         @"http://bit.ly/WQBorI", @"link",
         @"http://quetzalgames.com/LB/icone@2x.png", @"picture",
         @"Little Bac HD", @"name",
         @"Little Bac HD ", @"caption",
         text, @"description",
         nil];
        if ([FBSession.activeSession.permissions
             indexOfObject:@"publish_actions"] == NSNotFound) {
            // No permissions found in session, ask for it
            [FBSession.activeSession
             reauthorizeWithPublishPermissions:
             [NSArray arrayWithObject:@"publish_actions"]
             defaultAudience:FBSessionDefaultAudienceFriends
             completionHandler:^(FBSession *session, NSError *error) {
                 if (!error) {
                     // If permissions granted, publish the story
                     [FBRequestConnection
                      startWithGraphPath:@"me/feed"
                      parameters:self.postParams
                      HTTPMethod:@"POST"
                      completionHandler:^(FBRequestConnection *connection,
                                          id result,
                                          NSError *error) {
                          if (error) {
                              alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Erreur lors de la publication"                                                                  delegate:self
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil,nil];
                              [alert show];alert = nil;
                          }
                      }];
                     
                 }
             }];
        }
        else {
            [FBRequestConnection
             startWithGraphPath:@"me/feed"
             parameters:self.postParams
             HTTPMethod:@"POST"
             completionHandler:^(FBRequestConnection *connection,
                                 id result,
                                 NSError *error) {
                 if (error) {
                     alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Erreur lors de la publication"                                                                  delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil,nil];
                     [alert show];alert = nil;
                 }
             }];
        }
    } else {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate openSessionWithAllowLoginUI:YES];
        [self sendFBWithText:text andImage:image showSheet:sheet];
    }
}

@end
