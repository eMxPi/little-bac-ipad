//
//  EmptyCellAccueil.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 25/03/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmptyCellAccueil : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *titre;

@end
