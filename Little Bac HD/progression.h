//
//  progression.h
//  Little Bac HD
//
//  Created by Maxime Pontoire on 28/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface progression : NSObject {
    NSString *utilisateur;
	NSString *totalPartiesJoues;
	NSString *meilleurScore;
	NSString *meilleurScoreSolo;
	NSString *meilleurScoreDuo;
	NSString *meilleureSerie;
	NSString *scoreMoyen;
}
@property (nonatomic, retain) NSString *utilisateur;
@property (nonatomic, retain) NSString *totalPartiesJoues;
@property (nonatomic, retain) NSString *meilleurScore;
@property (nonatomic, retain) NSString *meilleurScoreSolo;
@property (nonatomic, retain) NSString *meilleurScoreDuo;
@property (nonatomic, retain) NSString *meilleureSerie;
@property (nonatomic, retain) NSString *scoreMoyen;

@end
