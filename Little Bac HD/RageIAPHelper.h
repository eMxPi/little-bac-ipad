//
//  RageIAPHelper.h
//  Captain Price
//
//  Created by Maxime Pontoire on 29/11/12.
//  Copyright (c) 2012 Quetzal. All rights reserved.
//

#import "IAPHelper.h"

@interface RageIAPHelper : IAPHelper

+ (RageIAPHelper *)sharedInstance;

@end
