//
//  ConnexionViewController.m
//  Little Bac HD
//
//  Created by Maxime Pontoire on 04/01/13.
//  Copyright (c) 2013 QuetzalGames. All rights reserved.
//

#import "ConnexionViewController.h"

@interface ConnexionViewController ()

@end

@implementation ConnexionViewController
@synthesize delegate, keyboardControls = _keyboardControls, urlImage;

static int curveValues[] = {
    UIViewAnimationOptionCurveEaseInOut,
    UIViewAnimationOptionCurveEaseIn,
    UIViewAnimationOptionCurveEaseOut,
    UIViewAnimationOptionCurveLinear };

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [motDePasseField setDelegate:self];
    [emailField setDelegate:self];
    [pseudoField setDelegate:self];
    [titleLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [motDePasseField setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [emailField setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [pseudoField setFont:[UIFont fontWithName:@"Kameron-Bold" size:20]];
    [pseudoLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [emailLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [emailDetailLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:13]];
    [motDePasseLabel setFont:[UIFont fontWithName:@"Kameron-Bold" size:15]];
    [infoFBTextLabel setFont:[UIFont fontWithName:@"Sansita One" size:15]];
    [littleFBLabel setFont:[UIFont fontWithName:@"Sansita One" size:20]];
    // Do any additional setup after loading the view from its nib.
    
    // Initialize the keyboard controls
    self.keyboardControls = [[BSKeyboardControls alloc] init];
    
    // Set the delegate of the keyboard controls
    self.keyboardControls.delegate = self;
    
    // Add all text fields you want to be able to skip between to the keyboard controls
    // The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
    if (isFacebook) {
        self.keyboardControls.textFields = [NSArray arrayWithObjects:pseudoField,
                                            motDePasseField, nil];
    } else {
        self.keyboardControls.textFields = [NSArray arrayWithObjects:pseudoField,
                                            motDePasseField, emailField, nil];
    }
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    [self.keyboardControls reloadTextFields];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide) name:UIKeyboardDidHideNotification object:nil];
    [imageProfile.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [imageProfile.layer setBorderWidth: 2.0f];
    
    
    // Reglages
    imageReglages.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *downReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(revealReglages)];
    downReglages.delegate = self;
    [downReglages setDirection:UISwipeGestureRecognizerDirectionDown];
    [imageReglages addGestureRecognizer:downReglages];
    
    imageReglagesDeplie.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *upReglages = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideReglages)];
    upReglages.delegate = self;
    [upReglages setDirection:UISwipeGestureRecognizerDirectionUp];
    [imageReglagesDeplie addGestureRecognizer:upReglages];
    
    
    [effetLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [musiqueLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [shareLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [deconnexionReglagesLabel setFont:[UIFont fontWithName:@"Sansita One" size:25]];
    [Flurry logAllPageViews:self];
    [Flurry logEvent:@"Connexion Affiche"];
    StatistiquesRequest *stats = [[StatistiquesRequest alloc] init];
    [stats writeMessage:@"fichier.csv" message:@"Connexion Affiche"];
}

-(void)viewWillAppear:(BOOL)animated {
    isFacebook = TRUE;
    isMail = FALSE;
    [self loadProfile];
    facebookName = @"";
    [Flurry logEvent:@"Connexion Affiche"];
    facebookConnected = FALSE;
    secondes = 1;
    secondesLoad = 1;
    appels = 0;
}


-(void)reloadFacebook {
    secondes++;
    if (secondes == 15+appels*5) {
        if (appels < 3) {
            if (!facebookConnected) {
                alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"La connexion à Facebook a échoué. L'application va réessayer"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
                [alert show];
                alert = nil;
                [self tryFaceBook];
                secondes = 1;
            }
            appels++;
        } else {
            alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"La connexion à Facebook a échoué."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
            [chargementIndicator setHidden:TRUE];
        }
    }
}

-(void)reloadFacebookFirst {
    secondesLoad++;
    if (secondesLoad == 2) {
        [self tryFaceBook];
        [timerLoad invalidate];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [timer invalidate];
    [timerLoad invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resetFields {
    [pseudoField setText:@""];
    [emailField setText:@""];
    [motDePasseField setText:@""];
    [imageProfile setImage:[UIImage imageNamed:@"avatar_defaut.png"]];
}

- (IBAction)deconnexionPressedButton:(id)sender {
    [self hideReglages];
    [delegate disconnectConnexionReglagesPressed];
}


-(void)revealReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(showReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, 40, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)hideReglages {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector: @selector(hideReglagesAnimation)];
    [reglagesView setFrame:CGRectMake(644, -496, reglagesView.frame.size.width, reglagesView.frame.size.height)];
    [UIView commitAnimations];
}

-(void)showReglagesAnimation {
    [imageReglages setHidden:TRUE];
    [imageReglagesDeplie setHidden:FALSE];
}

-(void)hideReglagesAnimation {
    [imageReglages setHidden:FALSE];
    [imageReglagesDeplie setHidden:TRUE];
}


-(void)loadProfile {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isEffet = [defaults boolForKey:isEffetActive];
    isMusique = [defaults boolForKey:isMusiqueActive];
    isShare = [defaults boolForKey:isShareActive];
    [self changeButtons];
}

-(void)changeButtons {
    if (isEffet) {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [effetButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isMusique) {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [musiqueButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
    if (isShare) {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_coche.png"] forState:UIControlStateNormal];
    } else {
        [shareButton setImage:[UIImage imageNamed:@"reglages_btn_decoche.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)activeEffet:(id)sender {
    isEffet = !isEffet;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isEffet forKey:isEffetActive];
    [self changeButtons];
}

- (IBAction)activeMusique:(id)sender {
    isMusique = !isMusique;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isMusique forKey:isMusiqueActive];
    [self changeButtons];
}

- (IBAction)activeShare:(id)sender {
    isShare = !isShare;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isShare forKey:isShareActive];
    [self changeButtons];
}

- (IBAction)retourPressed:(id)sender {
    [delegate retourConnexionPressed];
}

- (IBAction)flipFbPressed:(id)sender {
	/*[UIView beginAnimations:nil context:nil];
     [UIView setAnimationDelegate:self];
     [UIView setAnimationDidStopSelector:@selector(displayBack)];
     [UIView setAnimationDuration:1.0];
     [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
     forView:frontView
     cache:YES];
     [UIView commitAnimations];*/
    [self hideReglages];
	[self addSubviewWithFadeAnimation:backView duration:1.0 option:curveValues[1]];
    [self fbPressed:nil];
}

- (IBAction)flipMailPressed:(id)sender {
	/*[UIView beginAnimations:nil context:nil];
     [UIView setAnimationDelegate:self];
     [UIView setAnimationDidStopSelector:@selector(displayBack)];
     [UIView setAnimationDuration:1.0];
     [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
     forView:frontView
     cache:YES];
     [UIView commitAnimations];*/
    [self hideReglages];
	[self addSubviewWithFadeAnimation:backView duration:1.0 option:curveValues[1]];
    [self mailPressed:nil];
}

- (IBAction)resetPassword:(id)sender {
    if ([pseudoField.text length] > 0) {
        if (utils == nil) {
            utils = [[ApiUtils alloc] init];
        }
        resultat *res = [utils resetPassword:pseudoField.text];
        if ([res.message isEqualToString:@"OK"]) {
            alert = [[CustomAlert alloc] initWithTitle:@"C'est fait !" message:@"Veuillez saisir une nouveau mot de passe."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        } else if ([res.message isEqualToString:@"ME"]) {
            alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Cet utilisateur n'existe pas"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        } else {
            alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Veuillez réessayer ultérieurement."
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        }
    } else {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Veuillez saisir un pseudo"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    }
}
- (void) addSubviewWithFadeAnimation:(UIView*)view duration:(float)secs option:(UIViewAnimationOptions)option
{
	view.alpha = 0.0;
	[view setHidden:FALSE];// make the view transparent
	//[self.view addSubview:view];
    [UIView setAnimationDidStopSelector:@selector(displayBack)];// add it
	[UIView animateWithDuration:secs delay:0.0 options:option
                     animations:^{view.alpha = 1.0;}
                     completion:nil];
	frontView.alpha = 1.0;
	[UIView animateWithDuration:secs/2 delay:0.0 options:option
                     animations:^{frontView.alpha = 0.0;}
                     completion:nil];	// animate the return to visible
}

-(void)displayBack {
    //[backView setHidden:FALSE];
    [frontView setHidden:TRUE];
    [self.view bringSubviewToFront:reglagesView];
}

- (IBAction)fbPressed:(id)sender {
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reloadFacebook) userInfo:nil repeats:YES];
    timerLoad = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reloadFacebookFirst) userInfo:nil repeats:YES];
    isFacebook = TRUE;
    isMail = FALSE;
    [infoFBTextLabel setHidden:FALSE];
    [chargementIndicator setHidden:FALSE];
    // suppression du champ FB
    _keyboardControls.textFields = [NSArray arrayWithObjects:pseudoField,
                                    motDePasseField, nil];
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    [_keyboardControls reloadTextFields];
    [self resetFields];
    [fbSeparateur setHidden:FALSE];
    [motDePasseField setHidden:FALSE];
    [motDePasseLabel setHidden:FALSE];
    [emailField setHidden:TRUE];
    [emailLabel setHidden:TRUE];
    [emailDetailLabel setHidden:TRUE];
    [imageMotDePasse setHidden:TRUE];
    [mailSeparateur setHidden:TRUE];
    [self tryFaceBook];
}


-(void)tryFaceBook {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    // If the user is authenticated, log out when the button is clicked.
    // If the user is not authenticated, log in when the button is clicked.
    if (!FBSession.activeSession.isOpen) {
        // The user has initiated a login, so call the openSession method
        // and show the login UX if necessary.
        if ([appDelegate openSessionWithAllowLoginUI:YES]) {
            [self loadFaceBookDetails];
        };
    } else {
        [self loadFaceBookDetails];
    }
}

- (IBAction)mailPressed:(id)sender {
    [timer invalidate];
    [timerLoad invalidate];
    isFacebook = FALSE;
    isMail = TRUE;
    [infoFBTextLabel setHidden:TRUE];
    // Ajout du champ mot de passe
    _keyboardControls.textFields = [NSArray arrayWithObjects:pseudoField,
                                    motDePasseField, emailField, nil];
    
    // Add the keyboard control as accessory view for all of the text fields
    // Also set the delegate of all the text fields to self
    [_keyboardControls reloadTextFields];
    [self resetFields];
    [motDePasseField setHidden:FALSE];
    [motDePasseLabel setHidden:FALSE];
    [emailField setHidden:FALSE];
    [emailLabel setHidden:FALSE];
    [emailDetailLabel setHidden:FALSE];
    [imageMotDePasse setHidden:FALSE];
    [self resetFields];
    [fbSeparateur setHidden:TRUE];
    [mailSeparateur setHidden:FALSE];
}

-(void)addOjectsFromPreviouslyNotConnected {
    if (utils == nil) {
        utils = [[ApiUtils alloc] init];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [utils addLittles:[defaults stringForKey:littlesCurrent]];
    [utils addAntiSeche:[defaults stringForKey:nextBonusCurrent]];
    [utils addCorrection:[defaults stringForKey:checkBonusCurrent]];
    [utils addTemps:[defaults stringForKey:tempsBonusCurrent]];
    [defaults setValue:@"0" forKey:littlesCurrent];
    [defaults setValue:@"0" forKey:nextBonusCurrent];
    [defaults setValue:@"0" forKey:checkBonusCurrent];
    [defaults setValue:@"0" forKey:tempsBonusCurrent];
}


-(void)loadFaceBookDetails {
    [self parseFacebookAccount];
}


-(NSDictionary *)parseFacebookAccount
{
    NSMutableDictionary *propsDic = [[NSMutableDictionary alloc]init];
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             [propsDic setObject:user.name forKey:@"username"];
             [propsDic setObject:[user objectForKey:@"email"] forKey:@"email"];
             [propsDic setObject:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectForKey:@"id"]] forKey:@"pic"];
             [pseudoField setText:[propsDic objectForKey:@"username"]];
             [emailField setText:[propsDic objectForKey:@"email"]];
             facebookName = [propsDic objectForKey:@"username"];
             
             dispatch_async(dispatch_get_global_queue(0,0), ^{
                 NSData * data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[propsDic objectForKey:@"pic"]]];
                 if ( data == nil )
                     return;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     //[imageProfile.layer setMasksToBounds:YES];
                     //imageProfile.layer.cornerRadius = 65.0f;
                     [imageProfile setImage:[[UIImage imageWithData:data] imageCroppedToFitSize:CGSizeMake(127.0f, 127.0f)]];
                     [chargementIndicator setHidden:TRUE];
                     if ([facebookName isEqualToString:@""]) {
                         [self tryFaceBook];
                     } else {
                         facebookConnected = TRUE;
                         [timer invalidate];
                         [timerLoad invalidate];
                     }
                 });
             });
         } else {
             alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"La connexion à Facebook a échoué. Réappuie sur le bouton pour te connecter."
                                               delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil,nil];
             [alert show];
             alert = nil;
         }
     }];
    return propsDic;
}

- (IBAction)validatePressed:(id)sender {
    [emailField endEditing:YES];
    [pseudoField endEditing:YES];
    [motDePasseField endEditing:YES];
    if ([motDePasseField.text isEqualToString:@""] || [pseudoField.text isEqualToString:@""]) {
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Veuillez saisir tous les champs"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else {
        NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/add", apiUtilisateurUrl]]];
        NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
        
        XMLStringParser *apiParser = [[XMLStringParser alloc]
                                      parseXMLString:postData toObject:@"resultat" parseError:nil];
        if ([[apiParser items] count] == 0) {
            
            alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Enregistrement indisponible"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil,nil];
            [alert show];
            alert = nil;
        } else {
            for(int i = 0; i < 1; i++) {
                resultat *res = [[apiParser items] objectAtIndex:i];
                if ([res.message isEqualToString:@"MP"]) {
                    
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Veuillez vérifier tous les champs"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                } else if ([res.message isEqualToString:@"E"]) {
                    
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Ce pseudo existe déjà, ou un mauvais mot de passe a été saisi."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                } else if ([res.message isEqualToString:@"IA"]) {
                    alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Votre adresse mail ne semble pas valide"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
                    [alert show];
                    alert = nil;
                    
                }  else if ([res.message isEqualToString:@"OK"] || [res.message isEqualToString:@"CF"]) {
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setValue: motDePasseField.text forKey:motDePasseProfil];
                    [userDefault setValue: pseudoField.text forKey:nameProfil];
                    [userDefault setValue: emailField.text forKey:emailProfil];
                    [userDefault setBool:isFacebook forKey:isFacebookActive];
                    [userDefault setBool:isMail forKey:isMailActive];
                    
                    if (utils == nil) {
                        utils = [[ApiUtils alloc] init];
                    }
                    [utils addToken:[userDefault stringForKey:deviceTokenKey]];
                    if (isFacebook) {
                        if (utils == nil) {
                            utils = [[ApiUtils alloc] init];
                        }
                        resultat *res2 = [utils addLittles:@"100"];
                        if (res2 == nil){
                            alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Le service semble indisponible. Êtes-vous sûr d'être bien connecté à internet ?"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil,nil];
                            [alert show];
                            alert = nil;
                        } else if ([res2.message isEqualToString:@"KO"]) {
                            alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Enregistrement indisponible"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil,nil];
                            [alert show];
                            alert = nil;
                        }
                    }
                    [self addOjectsFromPreviouslyNotConnected];
                    [self pushUpload];
                } else if ([res.message isEqualToString:@"DE"]) {
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setValue: motDePasseField.text forKey:motDePasseProfil];
                    [userDefault setValue: pseudoField.text forKey:nameProfil];
                    [userDefault setValue: emailField.text forKey:emailProfil];
                    [self loadUtilisateurImage];
                    [userDefault setValue:urlImage forKey:picProfil];
                    [userDefault setBool:isFacebook forKey:isFacebookActive];
                    [userDefault setBool:isMail forKey:isMailActive];
                    
                    if (utils == nil) {
                        utils = [[ApiUtils alloc] init];
                    }
                    NSString *device = [userDefault stringForKey:deviceTokenKey];
                    if ([device isEqualToString:@""] || [device isEqualToString:@"(null)"]) {
                        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                        device = appDelegate.dToken;
                        [userDefault setValue:device forKey:deviceTokenKey];
                    }
                    [utils addToken:device];
                    [self addOjectsFromPreviouslyNotConnected];
                    [delegate validatePressedDetail:FALSE];
                }
            }
        }
    }
}

-(void)loadUtilisateurImage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults stringForKey:nameProfil];
    
    str = [str stringByReplacingOccurrencesOfString:@" "
                                         withString:@"%20"];
    NSString *chaine = [NSString stringWithFormat:@"%@", apiUtilisateurUrl];
    chaine = [NSString stringWithFormat:@"%@/%@", chaine, str];
    NSURL *URL = [NSURL URLWithString:chaine];
    NSLog(@"URL %@", URL);
    XMLToObjectParser *apiParser = [[XMLToObjectParser alloc]
                                    parseXMLAtURL:URL toObject:@"utilisateur" parseError:nil];
    
    for(int i = 0; i < [[apiParser items] count]; i++) {
        utilisateur *currentMarker = [[apiParser items] objectAtIndex:0];
        urlImage = currentMarker.urlImage;
    }
}

- (IBAction)selectImage:(id)sender {
    [emailField resignFirstResponder];
    [pseudoField resignFirstResponder];
    [motDePasseField resignFirstResponder];
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)) {
        
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Vous devez autoriser l'accès à vos photos"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
        popoverController.delegate = self;
        [popoverController presentPopoverFromRect:CGRectMake(302,263,10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)pickerSelect didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
	//[picker.parentViewController dismissModalViewControllerAnimated:YES];
    [popoverController dismissPopoverAnimated:YES];
    [imageProfile setImage:[image imageCroppedToFitSize:CGSizeMake(127.0f, 127.0f)]];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)pickerSelect {
	//[picker.parentViewController dismissModalViewControllerAnimated:YES];
    [popoverController dismissPopoverAnimated:YES];
}

-(void)pushUpload {
    NSData *data = UIImageJPEGRepresentation(imageProfile.image, 90);
    UIImage* newImage = [[UIImage imageWithData:data] imageCroppedToFitSize:imageProfile
                         .frame.size];
    NSData *imageData = UIImageJPEGRepresentation(newImage, 90);
	NSString *urlString = @"http://quetzalgames.com/uploader/upload.php";
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
	
	NSString *boundary = [NSString stringWithFormat:@"---------------------------14737809831466499882746641449"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
	[request addValue:contentType forHTTPHeaderField:@"Content-Type"];
	
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\".jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[NSData dataWithData:imageData]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[request setHTTPBody:body];
	
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	urlImage = returnString;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:pseudoField.text forKey:nameProfil];
    [userDefault setValue:urlImage forKey:picProfil];
    
    NSString *xml = [self createRequest:[NSURL URLWithString:[NSString stringWithFormat:@"%@/modify", apiUtilisateurUrl]]];
    NSData *postData = [NSData dataWithBytes:[xml UTF8String] length:[xml length]];
    
    XMLStringParser *apiParser = [[XMLStringParser alloc]
                                  parseXMLString:postData toObject:@"resultat" parseError:nil];
    if ([[apiParser items] count] == 0) {
        
        alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Enregistrement indisponible"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil,nil];
        [alert show];
        alert = nil;
    } else {
        for(int i = 0; i < 1; i++) {
            resultat *res = [[apiParser items] objectAtIndex:i];
            if ([res.message isEqualToString:@"OK"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setObject:urlImage forKey:picProfil];
                [delegate validatePressedDetail:TRUE];
            } else {
                alert = [[CustomAlert alloc] initWithTitle:@"Erreur" message:@"Envoi de l'image de profil impossible"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil,nil];
                [alert show];
                alert = nil;
            }
        }
    }
}

-(NSString *)createRequest:(NSURL *)url {
    NSMutableURLRequest *requete = [NSMutableURLRequest requestWithURL:url];
    [requete setHTTPMethod:@"POST"];
    [requete setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requete setHTTPBody:[[NSString stringWithFormat:@"id=%@&nom=%@&prenom=%@&twitter=%@&facebook=%@&mail=%@&pwd=%@&urlImage=%@", pseudoField.text,@"", @"", @"", facebookName ,emailField.text, motDePasseField.text, urlImage] dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"requete = %@",requete);
    //In case of error
    NSError *error;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:requete returningResponse:&response error:&error];
    NSLog(@"error = %@",error);
    NSLog(@"response = %@",response);
    NSLog(@"result = %@",result);
    
    NSString *stringXML = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"stringXML = %@",stringXML);
    return stringXML;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == motDePasseField) {
        [motDePasseField resignFirstResponder];
    } else if (textField == pseudoField) {
        [pseudoField resignFirstResponder];
    } else if (textField == emailField) {
        [emailField resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == emailField) {
        [emailField endEditing:YES];
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } else if (textField == pseudoField) {
        [pseudoField endEditing:YES];
    } else if (textField == motDePasseField) {
        [motDePasseField endEditing:YES];
    }
}


/* Scroll the view to the active text field */
- (void)scrollViewToTextField:(id)textField
{
    
    if (textField == emailField) {
        [self.view setFrame:CGRectMake(0, -100, self.view.frame.size.width, self.view.frame.size.height)];
    } else if (textField == motDePasseField) {
        [self.view setFrame:CGRectMake(0, -50, self.view.frame.size.width, self.view.frame.size.height)];
    } else {
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
}

#pragma mark -
#pragma mark BSKeyboardControls Delegate

/*
 * The "Done" button was pressed
 * We want to close the keyboard
 */
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)controls
{
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [controls.activeTextField resignFirstResponder];
}

/* Either "Previous" or "Next" was pressed
 * Here we usually want to scroll the view to the active text field
 * If we want to know which of the two was pressed, we can use the "direction" which will have one of the following values:
 * KeyboardControlsDirectionPrevious        "Previous" was pressed
 * KeyboardControlsDirectionNext            "Next" was pressed
 */
- (void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self scrollViewToTextField:textField];
}

#pragma mark -
#pragma mark Text Field Delegate

/* Editing began */
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.keyboardControls.textFields containsObject:textField])
        self.keyboardControls.activeTextField = textField;
    [self scrollViewToTextField:textField];
}

#pragma mark -
#pragma mark Text View Delegate

/* Editing began */
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([self.keyboardControls.textFields containsObject:textView])
        self.keyboardControls.activeTextField = textView;
    [self scrollViewToTextField:textView];
}

/* Keyboard hide */
-(void)keyboardDidHide {
    [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

@end
